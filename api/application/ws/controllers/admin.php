<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require(APPPATH . '/libraries/REST_Controller.php');

class Admin extends REST_Controller {

    var $client;
    protected $methods = array(
        'index_get' => array('level' => 10, 'limit' => 10),
    );

    public function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
    }

    
    public function categoryList_post() {
        $postData = $_POST;
        extract($postData);

        $categoryList = $this->admin_model->getCategoryList();
        if(isset($vDeviceID) && $vDeviceID!=''){
            $data = $this->admin_model->checkDevicetoken($postData);
            if(!empty($data)){
                $count = $this->admin_model->updateDevicetoken($postData);
            }else{
                $count = $this->admin_model->addDevicetoken($postData);
            }
        }

        if (!empty($categoryList)) {
                $this->send_success($categoryList);
        }else{
            $this->send_fail('No Data Found');
        }
    }


    public function categoryImage_post() {
        $postData = $_POST;
        extract($postData);

        $categoryImageList = $this->admin_model->getCategoryImageList($iCategoryMasterID);

        if (!empty($categoryImageList)) {
                $this->send_success($categoryImageList);
        }else{
            $this->send_fail('No Data Found');
        }
    }

    public function subCategoryList_post() {
        $postData = $_POST;
        extract($postData);

        $subCategoryList = $this->admin_model->getSubCategoryList($iCategoryMasterID);
        
        if (!empty($subCategoryList)) {
                $this->send_success($subCategoryList);
        }else{
            $this->send_fail('No Data Found');
        }
    }

    public function subCategoryImageList_post() {
        $postData = $_POST;
        extract($postData);

        $subCategoryList = $this->admin_model->getSubCategoryImageList($iSubCategoryID);

        if (!empty($subCategoryList)) {
                $this->send_success($subCategoryList);
        }else{
            $this->send_fail('No Data Found');
        }
    }

    public function serviceProviderList_post() {
        $postData = $_POST;
        extract($postData);

        $subCategoryList = $this->admin_model->getServiceProviderList($iSubCategoryID);

        if (!empty($subCategoryList)) {
                $this->send_success($subCategoryList);
        }else{
            $this->send_fail('No Data Found');
        }
    }

    public function addViewCountToServiceProvider_post() {
        $postData = $_POST;
        extract($postData);

        $count = $this->admin_model->addViewCountToServiceProvider($iServiceProviderID);

        if (!empty($count)) {
                $data = array(
                            'iCount'=>$count
                                );
                $this->send_success($data);
        }else{
            $this->send_fail('Please try again');
        }
    }

    public function pollList_post() {
        $postData = $_POST;
        extract($postData);

        $pollList = $this->admin_model->pollList($vDeviceID);

        if (!empty($pollList)) {
                $this->send_success($pollList);
        }else{
            $this->send_fail('No Data Found');
        }
    }

    public function productOfTheDay_post() {
        $postData = $_POST;
        extract($postData);

        $pollList = $this->admin_model->productOfTheDay($vDeviceID);

        if (!empty($pollList)) {
                $this->send_success($pollList);
        }else{
            $this->send_fail('No Data Found');
        }
    }

    public function bookProduct_post() {
        $postData = $_POST;
        extract($postData);

        $count = $this->admin_model->bookProduct($postData);

        if ($count == '1') {
            $this->send_success('Booked successfully');
        }else{
            $this->send_fail('Already Booked, Please try other');
        }
    }

    public function addPoll_post() {
        $postData = $_POST;
        extract($postData);

        $count = $this->admin_model->addPoll($postData);

        if ($count == '1') {
            $this->send_success('Poll submited successfully');
        }else{
            $this->send_fail('Already submited, Please try other');
        }
    }

    public function updateDevicetoken_post() {
        $postData = $_POST;
        extract($postData);

        $data = $this->admin_model->checkDevicetoken($postData);
        if(!empty($data)){
            $count = $this->admin_model->updateDevicetoken($postData);
        }else{
            $count = $this->admin_model->addDevicetoken($postData);
        }

        if ($count == '1') {
            $this->send_success('Device token update successfully');
        }else{
            $this->send_fail('Please try again');
        }
    }

    public function addContactUs_post() {
        $postData = $_POST;
        extract($postData);

        $count = $this->admin_model->addContactUs($postData);

        $this->send_success('Inquery submited successfully');
        
    }

    public function aboutUs_post() {
        $postData = $_POST;
        extract($postData);

        $aboutUs = $this->admin_model->aboutUs();

        if (!empty($aboutUs)) {
                $this->send_success($aboutUs);
        }else{
            $this->send_fail('No Data Found');
        }
    }


    public function fullpageAdvertise_post() {
        $postData = $_POST;
        extract($postData);

        $aboutUs = $this->admin_model->fullpageAdvertise();

        if (!empty($aboutUs)) {
                $this->send_success($aboutUs);
        }else{
            $this->send_fail('No Data Found');
        }
    }

    public function sliderAdvertise_post() {
        $postData = $_POST;
        extract($postData);

        $aboutUs = $this->admin_model->sliderAdvertise();

        if (!empty($aboutUs)) {
                $this->send_success($aboutUs);
        }else{
            $this->send_fail('No Data Found');
        }
    }

    public function serviceProviderAdvertise_post() {
        $postData = $_POST;
        extract($postData);
        
        $aboutUs = $this->admin_model->serviceProviderAdvertise($iSubCategoryID);

        if (!empty($aboutUs)) {
                $this->send_success($aboutUs);
        }else{
            $this->send_fail('No Data Found');
        }
    }


    function updateProfile_post() {
        $postData = $this->post();
        extract($postData);
        
        $iUserID = $this->admin_model->updateUserProfile($postData);
        $this->save($iUserID);
        if ($iUserID) {
            
            $getUserDetails = $this->admin_model->getUserDetailByID($iUserID);
            $data = array(
                            'iUserID'           => $getUserDetails['iUserID'],
                            'vName'             => $getUserDetails['vUserName'],
                            'vEmail'            => $getUserDetails['vUserEmail'],
                            'vPhone'            => $getUserDetails['vUserPhoneNumber'],
                            'isAlreadyRegister' => $getUserDetails['isAlreadyRegister'],
                            'vProfilePic'       => isset($getUserDetails['vUserProfilePic'])?PROFILE_PIC_URL.'thumb/'.$getUserDetails['vUserProfilePic']:'',
                            'vAddress'          => $getUserDetails['vUserAddress'],
                            'eUserType'         => 'privateUser'
                            
                        );
            $this->send_success($data);
          
        } else {
            $this->send_fail('User Profile Not Updated successfully');
        }    
    }

    public function historyList_post() {
        header( 'Content-Type: text/html; charset=utf-8' ); 
        $postData = $_POST;
        extract($postData);
        
        $historyList = $this->admin_model->historyList();
        if (!empty($historyList)) {
                $this->send_success($historyList);
        }else{
            $this->send_fail('No Data Found');
        }
    }

    //function send_android($device,$msg,$type){
    function send_android(){
        $apiKey = "AAAA0syjWbQ:APA91bE3ZzMxysoeJXBChdGemTZvm_Ovo3dGAxYKoeXW2h8Utmx-pAsKp60klnmrwJ6r6funqpp8J4oXj72cmhD1R-DP57YiXmUdskarroTTkdmHCxAu3l1EIbVIQbDV2iEM4UGLAsvi" ;
        $sound = 'default';    

        $device = 'cLdf-tq7aIQ:APA91bFdEM_yKdM8tW5CzMzEcOL5wQVgnsuUR2fOLjE3Q4xdYNbqOUSFO5zyNOyQpgpZVyySEnnfxcMRxcRJ5dPcZGoqEAFvXkcUyIxFR8Nd3SLcjmH3kv1y3z2KcmOFaHdvkknnoZ_h';
        $registrationIDs = array($device);
        $message = array(
                            //'msg'   => $msg,
                            'msg'   => 'jhjk sdfjksld ldsflsdj',
                            'image' => 'https://wallpaperbrowse.com/media/images/soap-bubble-1958650_960_720.jpg',
                            'sound' => $sound,
                            //'type'  =>$type
                            'type'  =>'test'
                        );

        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array(
            //'registration_ids' => $device,
            'registration_ids' => $registrationIDs,
            'data' => $message
            
        );

        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );

        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        $data = json_decode($result);
        mprd($data);
        if(!empty($data)){
            if ($data->success)
                return 'success';
            else
                return 'fail';    
        }else{

        }
        
        curl_close($ch);
    }


    function save($iUserID) {
        
        if (isset($_FILES['vUserProfilePic']) && $_FILES['vUserProfilePic']['name'] != '') {
            $upload_name = $_FILES['vUserProfilePic']['name'];
            $file_name = time() . "_" . random_string('alnum', 5);
            $targetpath = PROFILE_PIC_ROOT;

            if (!is_dir($targetpath)) {
                if (!mkdir($targetpath, 0777, TRUE)) {
                    exit('dir not created.');
                }
            }
            $config['upload_path'] = $targetpath;
            $config['file_name'] = $file_name;
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 6;
            $config['overwrite'] = false;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('vUserProfilePic')) {
                extract($this->upload->data());
                $mypath = $targetpath . '/' . $file_name;
                $this->make_thumb($mypath, $targetpath);
                //$this->make_main($mypath, $targetpath);
                $this->db->update('tbl_user', array('vUserProfilePic' => $file_name), array('iUserID' => $iUserID));
                return $file_name;
            } else {
                $this->send_fail($this->upload->display_errors());
            }
        }
    }

    public function make_thumb($mypath, $img_root_folder) {
        $source_path = $mypath;
        $list = list($width, $height) = getimagesize($mypath);
        $ratio = 200.00 / min($width, $height);
        $w = $width * $ratio;
        $h = $height * $ratio;
        $target_path = $img_root_folder . '/thumb/';
        if (!is_dir($target_path))
            mkdir($target_path, 0777, TRUE);
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'create_thumb' => FALSE,
            //'thumb_marker' => '_thumb',
            'width' => $w,
            'height' => $h
        );
        $this->load->library('image_lib', $config_manip);
        $this->image_lib->clear();
        $this->image_lib->initialize($config_manip);
        if (!$this->image_lib->resize()) {
            
        }
    }

    function send_mail($vEmail) {
        //mprd($vEmail);
        $query = $this->db->get_where('tbl_staff', array('vEmail' => $vEmail));
        $row = $query->row();
        $name = $row->vName;
        $id = $row->iStaffID;
        $email = $row->vEmail;
        $pass = $row->vPassword;


        $subject = 'Forgot Password';
        $this->load->library('email');
        $this->load->library('encrypt');

        $data1 = $this->db->query("SELECT vCompanymail FROM tbl_setting")->row_array();
        $COMPANY_EMAIL = $data1['vCompanymail'];

        $encrypted_id = base64_encode($id);
        $encrypted_pass = base64_encode($pass);

        $data = array(
            'link' => BASEURL . "forgot_pass/activate/" . $encrypted_id . "/" . $encrypted_pass,
            'email' => $email,
            'from' => $COMPANY_EMAIL,
            'name' => $name,
            'subject' => $subject
        );

        $this->load->view('email/forgotpass_view', $data);

        return true;
    }

    function send_fail($msg) {
        $row = array("MESSAGE" => "$msg", "SUCCESS" => 0);
        $this->response($row, 200);
    }

    function send_success($data, $additional = array()) {
        $row = array("DATA" => $data, "SUCCESS" => 1);
        if (!empty($additional)) {
            foreach ($additional as $key => $value) {
                $row[$key] = $value;
            }
        }
        $this->response($row, 200);
    }

    function send_success_with_message($msg) {
        $row = array("MESSAGE" => $msg, "SUCCESS" => 1);
        
        $this->response($row, 200);
    }
}