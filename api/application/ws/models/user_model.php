<?php

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->helper('string');
        $this->load->library('email');
        //$this->load->library('encrypt');
        $this->table = 'tbl_user';
    }

    function addPatient($data) {
        $this->db->insert('tbl_patients', $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return 0;
        }
    }

    function checkNurseEmailAvailable($vNurseEmail,$iNurseID='') {
        if($iNurseID!=''){
            $query = $this->db->query("SELECT * FROM tbl_nurse WHERE vNurseEmail = '$vNurseEmail' AND iNurseID != $iNurseID");
            if ($this->db->affected_rows() > 0) {
                return 1;
            } else {
                return 0;
            }
        }else{
            $query = $this->db->get_where('tbl_nurse', array('vNurseEmail' => $vNurseEmail));
            if ($query->num_rows() > 0)
                return 1;
            else
                return 0;    
        }
        
    }

    function getPineWoodConsignmentDataByuser() {
        
        $query = $this->db->query("SELECT 
                                        concat('http://kavitacareclinic.com/shyamimpex/jsondata/',file_name) as file_name
                                    FROM json_data as j
                                    WHERE j.consignment_id = 1
                                     ");
                                    
        
        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return array();
    }


    function getVesselList() {
        $query = $this->db->query("SELECT 
                                    iVesselID,
                                    vVesselName 
                                    FROM vessel_master as vm
                                    ");
        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return array();
    }

    function getLoadingList() {
        $query = $this->db->query("SELECT 
                                    iLoadingMasterID,
                                    vLoadingName 
                                    FROM loading_master as vm
                                    ");
        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return array();
    }




    function getUnLoadingList() {
        $query = $this->db->query("SELECT 
                                    iUnloadingMasterID,
                                    vUnloadingName 
                                    FROM unloading_master as vm
                                    ");
        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return array();
    }

    function getTransporterList() {
        $query = $this->db->query("SELECT 
                                    iTransporterID,
                                    vTransporterName 
                                    FROM transporter_master as vm
                                    ");
        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return array();
    }

    function getOtherDetail() {
        $nonTag = array();
        $query1 = $this->db->query("SELECT FORMAT(AVG(cbm),3) as avg FROM `pine_wood_logs` WHERE `length` BETWEEN 2.5 AND 3.9");
        $nonTag[0]['key'] = '3';
        $nonTag[0]['value'] = $query1->row_array()['avg'];

        $query2 = $this->db->query("SELECT FORMAT(AVG(cbm),3) as avg FROM `pine_wood_logs` WHERE `length` BETWEEN 4 AND 6");
        $nonTag[1]['key'] = '6';
        $nonTag[1]['value'] = $query2->row_array()['avg'];

        $query3 = $this->db->query("SELECT FORMAT(AVG(cbm),3) as avg FROM `pine_wood_logs` WHERE `length` BETWEEN 7 AND 9");
        
        $nonTag[2]['key'] = '9';
        $nonTag[2]['value'] = $query3->row_array()['avg'];

        $query4 = $this->db->query("SELECT FORMAT(AVG(cbm),3) as avg FROM `pine_wood_logs` WHERE `length` BETWEEN 10 AND 12");

        $nonTag[3]['key'] = '12';
        $nonTag[3]['value'] = $query4->row_array()['avg'];


        return $nonTag;
        
    }

    function addAllPineTransaction($data) {
        //mprd($data);
        foreach ($data as $key => $value) {
            
            $vTokenNumber = $value['vTokenNumber'];
            $query3 = $this->db->query("SELECT pine_transaction_id FROM pine_transaction WHERE vTokenNumber = '".$vTokenNumber."'");
            $pine_data = $query3->row_array();
            
            if(!empty($pine_data)){
                $pine_transaction_id = $pine_data['pine_transaction_id'];
            }else{
                $insertData = array(
                                'truckno'               =>$value['truckno'],
                                'vTokenNumber'          =>$value['vTokenNumber'],
                                'iTransporterID'        =>$value['iTransporterID'],
                                'iVesselID'             =>$value['iVesselID'],
                                'iLoadingMasterID'      =>$value['iLoadingMasterID'],
                                'iUnloadingMasterID'    =>$value['iUnloadingMasterID'],
                                'shift'                 =>$value['shift'],
                                'drivername'            =>$value['drivername'],
                                'driverMobileNumber'    =>$value['driverMobileNumber'],
                                'stack_1_nontally_count'=>isset($value['stack_1']['nontallyCount'])?$value['stack_1']['nontallyCount']:0,
                                'stack_2_nontally_count'=>isset($value['stack_2']['nontallyCount'])?$value['stack_2']['nontallyCount']:0,
                                'total_count'           =>$value['total_count'],
                                'remark'                =>$value['remark'],
                                'delivery_status'       =>'Pending',
                                'created'               =>date('Y-m-d H:i:s')
                                );
                $this->db->insert('pine_transaction', $insertData);
                $pine_transaction_id = $this->db->insert_id();
            }
            
            if ($pine_transaction_id != 0) {
                
                if(isset($value['stack_1']) && !empty($value['stack_1']['tallyData'])){
                    foreach ($value['stack_1']['tallyData'] as $key => $value_tally_data) {
                        $updateData = array(
                                                'status'            =>'tally',
                                                'stack_no'          =>'1',
                                                'pine_transaction_id'=>$pine_transaction_id
                                            );
                        $this->db->update('pine_wood_logs', $updateData, array('consignment_id' => $value['consignment_id'],'log_no'=>$value_tally_data));
                    }
                }
            
                if(isset($value['stack_1']['nontallyCount']) && $value['stack_1']['nontallyCount'] != 0){
                    $stack_1_lot_no = isset($value['stack_1']['lot_no'])?$value['stack_1']['lot_no']:0;
                    $stack_1_length = isset($value['stack_1']['length'])?$value['stack_1']['length']:0;
                    if($stack_1_lot_no !=0){
                        $cbm = 0.0;
                        $query = $this->db->query("SELECT FORMAT(AVG(cbm),3) as cbm FROM pine_wood_logs WHERE lot_no = $stack_1_lot_no AND length = $stack_1_length");
                        $result = $query->row_array();
                        if($result['cbm']!=''){
                            $cbm = $result['cbm'];
                        }
                    }
                    for ($i=0; $i < $value['stack_1']['nontallyCount'] ; $i++) { 
                        $insertData = array(
                                        'consignment_id'        =>$value['consignment_id'],
                                        'lot_no'                =>$value['stack_1']['lot_no'],
                                        'length'                =>$value['stack_1']['length'],
                                        'cbm'                   =>$cbm,
                                        'pine_transaction_id'   =>$pine_transaction_id,
                                        'stack_no'              => 1,
                                        'created'               =>date('Y-m-d'),
                                        'modified'              =>date('Y-m-d')
                                        );
                        $this->db->insert('non_tally_pine_wood_logs', $insertData);  
                    }
                }
                
                if(isset($value['stack_2']) && !empty($value['stack_2']['tallyData'])){
                    foreach ($value['stack_2']['tallyData'] as $key => $value_tally_data) {
                        $updateData = array(
                                            'status'             =>'tally',
                                            'stack_no'           =>'2',
                                            'pine_transaction_id'=>$pine_transaction_id
                                        );
                        $this->db->update('pine_wood_logs', $updateData, array('consignment_id' => $value['consignment_id'],'log_no'=>$value_tally_data));
                    }
                }

                if(isset($value['stack_2']['nontallyCount']) && $value['stack_2']['nontallyCount'] != 0){
                    $stack_2_lot_no = isset($value['stack_2']['lot_no'])?$value['stack_2']['lot_no']:0;
                    $stack_2_length = isset($value['stack_2']['length'])?$value['stack_2']['length']:0;
                    if($stack_2_lot_no!=0){
                        $cbm2 = 0.0;
                        $query2 = $this->db->query("SELECT FORMAT(AVG(cbm),3) as cbm FROM pine_wood_logs WHERE lot_no = $stack_2_lot_no AND length = $stack_2_length");
                        $result2 = $query2->row_array();
                        if($result2['cbm']!=''){
                            $cbm2 = $result['cbm'];
                        }
                    }
                    for ($j=0; $j < $value['stack_2']['nontallyCount'] ; $j++) { 
                        $insertData = array(
                                            'consignment_id'        =>$value['consignment_id'],
                                            'lot_no'                =>$value['stack_1']['lot_no'],
                                            'length'                =>$value['stack_1']['length'],
                                            'pine_transaction_id'   =>$pine_transaction_id,
                                            'cbm'                   =>$cbm2,
                                            'stack_no'              => 2,
                                            'created'               =>date('Y-m-d'),
                                            'modified'              =>date('Y-m-d')
                                        );
                        $this->db->insert('non_tally_pine_wood_logs', $insertData);  
                    }
                }
            }
        }
        return true;
    }

    function updateStatus($postData) {
        extract($postData);
        $query3 = $this->db->query("SELECT * FROM pine_transaction WHERE vTokenNumber = '".$vTokenNumber."'");
        $pine_data = $query3->row_array();
        if(!empty($pine_data)){
            $updateData = array(
                                'delivery_status'=>'Delivered',
                                'delivery_date'  =>date('Y-m-d H:i:s')
                            );
            $this->db->update('pine_transaction', $updateData, array('pine_transaction_id' => $pine_data['pine_transaction_id']));    
            $pine_transaction_id = $pine_data['pine_transaction_id'];
        }else{
            $insertData = array(
                                'truckno'               =>$truckno,
                                'vTokenNumber'          =>$vTokenNumber,
                                'iUnloadingMasterID'    =>$iUnloadingMasterID,
                                'drivername'            =>$drivername,
                                'driverMobileNumber'    =>$driverMobileNumber,
                                'stack_1_nontally_count'=>$stack_1_nontally_count,
                                'stack_2_nontally_count'=>$stack_2_nontally_count,
                                'total_count'           =>$total_count,
                                'delivery_status'       =>'Delivered',
                                'delivery_date'         =>date('Y-m-d H:i:s'),
                                'created'               =>date('Y-m-d')
                                );
                $this->db->insert('pine_transaction', $insertData);
                $pine_transaction_id = $this->db->insert_id();   
        }
        
        return $pine_transaction_id;
    }

    function removePatient($iPatientID) {
        $this->db->where('iPatientID', $iPatientID);
        $this->db->delete('tbl_patients');


        if ($this->db->affected_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

  

}

?>