<?php
class Page_model extends CI_Model 
{
	
	function __construct()
	{
		parent::__construct();
		
		$this->table = 'tbl_pagecontent';
	}
	
	function getPage($iPageID)
		{
			$result = $this->db->get_where($this->table, array('iPageID' => $iPageID));
			if($result->num_rows() > 0)
				
				return $result->row_array();
			
			else
				return '';  
		}
	
}
?>