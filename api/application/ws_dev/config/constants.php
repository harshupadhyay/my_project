<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



/*

  |--------------------------------------------------------------------------

  | File and Directory Modes

  |--------------------------------------------------------------------------

  |

  | These prefs are used when checking and setting modes when working

  | with the file system.  The defaults are fine on servers with proper

  | security, but you may wish (or even need) to change the values in

  | certain environments (Apache running a separate process for each

  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should

  | always be used to set the mode correctly.

  |

 */

define('FILE_READ_MODE', 0644);

define('FILE_WRITE_MODE', 0666);

define('DIR_READ_MODE', 0755);

define('DIR_WRITE_MODE', 0777);



/*

  |--------------------------------------------------------------------------

  | File Stream Modes

  |--------------------------------------------------------------------------

  |

  | These modes are used when working with fopen()/popen()

  |

 */



define('FOPEN_READ', 'rb');

define('FOPEN_READ_WRITE', 'r+b');

define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care

define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care

define('FOPEN_WRITE_CREATE', 'ab');

define('FOPEN_READ_WRITE_CREATE', 'a+b');

define('FOPEN_WRITE_CREATE_STRICT', 'xb');

define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');





/* End of file constants.php */

/* Location: ./application/config/constants.php */





/* Site Path and Urls */

define('_PATH', substr(dirname(__FILE__), 0, -25));



define('_URL', substr($_SERVER['PHP_SELF'], 0, - (strlen($_SERVER['SCRIPT_FILENAME']) - strlen(_PATH))));



define('SITE_PATH', _PATH . "/");

define('SITE_URL', _URL . "/");



define('WEBSITE_URL', SITE_URL . '/');


/*
define('DOMAIN_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/stol');

define('DOC_ROOT', $_SERVER['DOCUMENT_ROOT'] . '/stol/');

*/

define('DOMAIN_URL', 'http://'.$_SERVER['SERVER_NAME'].'/project/vignyapan');

define('DOC_ROOT', $_SERVER['DOCUMENT_ROOT'] . '/project/vignyapan/');




define('BASEURL', DOMAIN_URL . '/ws/');

define('gmapkey', '');

define('MAINTITLE', 'Vignyapan');

/* Site Path and Urls */

 
/* ############################################################################# */

/* URLS FOR FILE UPLOADED */
define('PROFILE_PIC_ROOT',DOC_ROOT.'images/profile_pic/');
define('PROFILE_PIC_URL',DOMAIN_URL.'/admin/images/profile_pic/');

define('CATEGORY_ROOT',DOC_ROOT.'/admin/images/category/');
define('CATEGORY_URL',DOMAIN_URL.'/admin/images/category/');

define('CATEGORY_IMAGE_ROOT',DOC_ROOT.'/admin/images/category_image/');
define('CATEGORY_IMAGE_URL',DOMAIN_URL.'/admin/images/category_image/');

define('SUBCATEGORY_COVER_IMAGE_ROOT',DOC_ROOT.'/admin/images/subcategory/cover_image/');
define('SUBCATEGORY_COVER_IMAGE_URL',DOMAIN_URL.'/admin/images/subcategory/cover_image/');

define('SUBCATEGORY_IMAGE_ROOT',DOC_ROOT.'/admin/images/subcategory/');
define('SUBCATEGORY_IMAGE_URL',DOMAIN_URL.'/admin/images/subcategory/');

define('HISTORY_IMAGE_ROOT',DOC_ROOT.'/admin/images/history/');
define('HISTORY_IMAGE_URL',DOMAIN_URL.'/admin/images/history/');

define('INSTA_IMAGE_ROOT',DOC_ROOT.'/admin/images/aboutus/Insta/');
define('INSTA_IMAGE_URL',DOMAIN_URL.'/admin/images/aboutus/Insta/');

define('TWITTER_IMAGE_ROOT',DOC_ROOT.'/admin/images/aboutus/Twitter/');
define('TWITTER_IMAGE_URL',DOMAIN_URL.'/admin/images/aboutus/Twitter/');

define('WHATSAPP_IMAGE_ROOT',DOC_ROOT.'/admin/images/aboutus/Whatsapp/');
define('WHATSAPP_IMAGE_URL',DOMAIN_URL.'/admin/images/aboutus/Whatsapp/');

define('FACEBOOK_IMAGE_ROOT',DOC_ROOT.'/admin/images/aboutus/Facebook/');
define('FACEBOOK_IMAGE_URL',DOMAIN_URL.'/admin/images/aboutus/Facebook/');

define('YOUTUBE_IMAGE_ROOT',DOC_ROOT.'/admin/images/aboutus/Youtube/');
define('YOUTUBE_IMAGE_URL',DOMAIN_URL.'/admin/images/aboutus/Youtube/');

define('WEB_IMAGE_ROOT',DOC_ROOT.'/admin/images/aboutus/Web/');
define('WEB_IMAGE_URL',DOMAIN_URL.'/admin/images/aboutus/Web/');

define('TELEGRAM_IMAGE_ROOT',DOC_ROOT.'/admin/images/aboutus/Telegram/');
define('TELEGRAM_IMAGE_URL',DOMAIN_URL.'/admin/images/aboutus/Telegram/');

define('FULL_PAGE_IMAGE_URL',DOMAIN_URL.'/admin/images/advertise/full/cover_images/');
define('SLIDER_COVER_AD_IMAGE_URL',DOMAIN_URL.'/admin/images/advertise/slider/cover_images/');
define('SLIDER_DETAIL_AD_IMAGE_URL',DOMAIN_URL.'/admin/images/advertise/slider/details_images/');
define('SERVICE_PROVIDER_COVER_AD_IMAGE_URL',DOMAIN_URL.'/admin/images/advertise/sp/details_images/');
define('SERVICE_PROVIDER_DETAIL_AD_IMAGE_URL',DOMAIN_URL.'/admin/images/advertise/sp/details_images/');

define('PRODUCT_IMAGE_ROOT',DOC_ROOT.'/admin/images/product/');
define('PRODUCT_IMAGE_URL',DOMAIN_URL.'/admin/images/product/');

define('NOTIFICATION_KEY','AAAAp00IfxY:APA91bG5GoW7-Lr4ks23cxYdvauzeShTfVvfjRr2B45z58oAX-aXQujg_lvOBwWRrxPXpADQBlkehrINvDe9TIZCWir_t-gzB2Tbm0JJnWtwSOfwBiGn9mICAHuuVQwzeGTyBCUX4CsT');


/*

  |--------------------------------------------------------------------------

  | CONSTANT MEASSAGES

  |--------------------------------------------------------------------------

  |

 */





/* * **************************************************

 * *                INFO MEASSAGE

 * ************************************************** */

define('EMAIL_INFO', 'Email Sent Successfully. Please check your indbox.');









