<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_model extends CI_Model {

    var $tbl_user;

    public function __construct() {
        parent::__construct();
        //Do your magic here
        $this->table = "tbl_user";
    }

    /**
     * [Insert_tbl_user_all description]
     * @param string $vFirstname [First name]
     */
    

    function getCategoryList(){
        $imagePath = CATEGORY_URL;
        $query = $this->db->query("SELECT 
                                        iCategoryMasterID,
                                        vCategoryName,
                                        isSubCategory,
                                        IF(vImageName <>'',CONCAT('$imagePath',vImageName),'') AS vImageName
                                    FROM category_master
                                    WHERE isDeleted = 'No' AND isStatus = 'Active'
                                     ");
        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return array();
    }

    function getCategoryImageList($iCategoryMasterID){
        $imagePath = CATEGORY_IMAGE_URL;

        $query = $this->db->query("SELECT 
                                        IF(vImageName <>'',CONCAT('$imagePath',vImageName),'') AS vImageName
                                    FROM category_image 
                                    WHERE iCategoryMasterID = $iCategoryMasterID
                                    ORDER BY iOrder ASC
                                ");
        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return array();
    }

    function getSubCategoryList($iCategoryMasterID){

        $imagePath = SUBCATEGORY_COVER_IMAGE_URL;
        if($iCategoryMasterID!=4){
            $query = $this->db->query("SELECT 
                                        iSubCategoryID,
                                        vText,
                                        isSubCategoryShow,
										created,
                                        IF(vCoverImage <>'',CONCAT('$imagePath',vCoverImage),'') AS vCoverImage
                                    FROM sub_category_master
                                    WHERE isDeleted = 'No'AND isStatus = 'active' AND iCategoryMasterID = $iCategoryMasterID
                                    ORDER BY created DESC
                                     ");
									 
									 //ORDER BY iSubCategoryID DESC
        }else{
            $query = $this->db->query("SELECT 
                                        iSubCategoryID,
                                        vText,
                                        isSubCategoryShow,
                                        IF(vCoverImage <>'',CONCAT('$imagePath',vCoverImage),'') AS vCoverImage
                                    FROM sub_category_master
                                    WHERE isDeleted = 'No'AND isStatus = 'active' AND iCategoryMasterID = $iCategoryMasterID
                                    ORDER BY vText ASC
                                     ");
									 
									 // ORDER BY vText ASC
									 // ORDER BY created DESC
									 
        }
        
        if ($query->num_rows() > 0){
            $data = $query->result_array();
            $imagePath = SUBCATEGORY_IMAGE_URL;
            foreach ($data as $key => $value) {
                $iSubCategoryID = $value['iSubCategoryID'];
                $query1 = $this->db->query("SELECT 
                                                IF(vImageName <>'',CONCAT('$imagePath',vImageName),'') AS vImageName
                                            FROM sub_category_image
                                            WHERE iSubCategoryID = $iSubCategoryID
                                            ORDER BY iOrder ASC
                                        ");
                $data[$key]['imageData'] = $query1->result_array();
            }
            return $data;
        }else
            return array();
    }

    function getSubCategoryImageList($iSubCategoryID){
        $imagePath = SUBCATEGORY_IMAGE_URL;
        $query = $this->db->query("SELECT 
                                                IF(vImageName <>'',CONCAT('$imagePath',vImageName),'') AS vImageName
                                            FROM sub_category_image
                                            WHERE iSubCategoryID = $iSubCategoryID
                                            ORDER BY iOrder ASC
                                        ");
        
        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return array();
    }

    function getServiceProviderList($iSubCategoryID){
		
		$imagePath = CATEGORY_URL;
        $query = $this->db->query("SELECT 
                                            *,
                                        IF(vProfilePic!='null',CONCAT('$imagePath',vProfilePic),'') AS vProfilePic
                                        FROM service_provider
                                        WHERE iSubCategoryID = $iSubCategoryID
                                        ORDER BY vName ASC
                                    ");
        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return array();
    }

    function addViewCountToServiceProvider($iServiceProviderID){

        $query = $this->db->get_where('service_provider', array('iServiceProviderID' => $iServiceProviderID));
        $data = $query->row_array();

        $newCount  = $data['iCount']+ 1;
        $updateData = array(
                              'iCount' => $newCount,
                            );
        $this->db->update('service_provider', $updateData, array('iServiceProviderID'=>$iServiceProviderID));
        return $newCount;
    }

    function pollList($vDeviceID){
        date_default_timezone_set("Asia/Calcutta");
        $query = $this->db->query("SELECT 
                                        *
                                    FROM poll_question
                                    ORDER BY iPollQuestionID DESC
                                ");

        $result = $query->result_array();
        
        foreach ($result as $key => $value) {
            $expiry_date =  strtotime($value['expiry_date']);
            $current_time = time();
            $iPollQuestionID = $value['iPollQuestionID'];

            $query = $this->db->get_where('poll_option', array('iPollQuestionID' => $value['iPollQuestionID']));
            $optionData = $query->result_array();

            $query2 = $this->db->query("SELECT 
                                                count(iPollAnswerID) as count
                                            FROM poll_answer_by_user
                                            WHERE iPollQuestionID = $iPollQuestionID
                                        ");
            $total_answer = $query2->row_array()['count'];

            foreach ($optionData as $key_option => $value_option) {
                $iPollOptionID = $value_option['iPollOptionID'];
                $result[$key]['option'][$key_option]['iPollOptionID'] = $iPollOptionID;
                $result[$key]['option'][$key_option]['vPollOption'] = $value_option['vPollOption'];

                $query2 = $this->db->query("SELECT 
                                                count(iPollAnswerID) as count
                                            FROM poll_answer_by_user
                                            WHERE iPollOptionID = $iPollOptionID AND iPollQuestionID = $iPollQuestionID
                                        ");
                $anwer = $query2->row_array()['count'];
                if($total_answer>0){
                    $per =  ($anwer * 100)/$total_answer;
                }else{
                    $per = 0;
                }
                $result[$key]['option'][$key_option]['per_ans'] = $per;

                $query3 = $this->db->get_where('poll_answer_by_user', array('vDeviceID' => $vDeviceID,'iPollQuestionID'=>$iPollQuestionID,'iPollOptionID'=>$value_option['iPollOptionID'])); 
                $isSelect = $query3->row_array();
                
                if(!empty($isSelect)){
                    $result[$key]['option'][$key_option]['is_selected'] = '1';
                }else{
                    $result[$key]['option'][$key_option]['is_selected'] = '0';
                }
            }
         
            if($expiry_date > $current_time){
                $result[$key]['is_expire'] = '0';
            }else{
                $result[$key]['is_expire'] = '1';
            }    

            $query1 = $this->db->get_where('poll_answer_by_user', array('vDeviceID' => $vDeviceID,'iPollQuestionID'=>$iPollQuestionID)); 
            $answerData = $query1->row_array();

            if(!empty($answerData)){
                $result[$key]['is_submited'] = '1';
            }else{
                $result[$key]['is_submited'] = '0';
            }   
        }
        return $result;
    }

    function addPoll($postData){
        extract($postData);
        $query = $this->db->get_where('poll_answer_by_user', array('vDeviceID' => $vDeviceID,'iPollQuestionID'=>$iPollQuestionID,'iPollOptionID'=>$iPollOptionID));
        $data = $query->row_array();
        if(empty($data)){
            $insertData = array(
                                'iPollQuestionID'   =>$iPollQuestionID,
                                'iPollOptionID'     =>$iPollOptionID,
                                'vDeviceID'         =>$vDeviceID,
                                'created'           =>gmdate('Y-m-d H:i:s'),
                                'modified'          =>gmdate('Y-m-d H:i:s')
                            );
            $this->db->insert('poll_answer_by_user', $insertData);
            return 1;
        }else{
            return 0;
        }
    }

    function productOfTheDay($vDeviceID){
        $imagePath = PRODUCT_IMAGE_URL;
        date_default_timezone_set("Asia/Calcutta");
        $current_time = time();
        $current_date = date('Y-m-d');

        $query = $this->db->query("SELECT 
                                    iProductID,
                                    vProductName,
                                    iCount,
                                    vImageName,
                                    IF(vImageName <>'',CONCAT('$imagePath',vImageName),'') AS vImageName,
                                    product_date,
                                    product_start_time,
                                    product_end_time,
                                    product_expiry_time,
                                    (SELECT vCode FROM product_purchase pp WHERE pp.iProductID = pod.iProductID AND vDeviceID = '$vDeviceID') as vCode,
                                    (SELECT count(iProductPurchaseID) FROM product_purchase pp WHERE pp.iProductID = pod.iProductID ) as used_count
                                    FROM product_ofthe_day pod
                                    WHERE product_date = '$current_date'
                                ");

        $result = $query->row_array();
        
        if(!empty($result) && strtotime($result['product_expiry_time']) > $current_time && strtotime($result['product_start_time']) < $current_time){
            if($result['vCode']!=''){
                $result['isBookingOpen'] = '0';
            }else{
                if(strtotime($result['product_start_time']) < $current_time AND strtotime($result['product_end_time']) > $current_time){
                    $result['isBookingOpen'] = '1';
                }else{
                    $result['isBookingOpen'] = '0';
                }
            }
            $result['type'] = 'Product';
        }else{
            $query = $this->db->query("SELECT 
                                    iProductImageID,
                                    product_date,
                                    vProductName,
                                    iCount,
                                    IF(vImage <>'',CONCAT('$imagePath',vImage),'') AS vImage
                                    FROM product_image
                                ");

            $result = $query->row_array();
            $result['type'] = 'image';
        }
        
        return $result;
    }

    function bookProduct($postData){
        extract($postData);
        $query = $this->db->get_where('product_purchase', array('vDeviceID' => $vDeviceID,'iProductID'=>$iProductID));
        $data = $query->row_array();
        if(empty($data)){
            $vCode = $this->generateRandomString(10);
            $insertData = array(
                                'iProductID'    =>$iProductID,
                                'vDeviceID'     =>$vDeviceID,
                                'vCode'         =>$vCode
                            );
            $this->db->insert('product_purchase', $insertData);
            return 1;
        }else{
            return 0;
        }
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function addContactUs($postData){
        extract($postData);
            $insertData = array(
                                'vName'     =>$vName,
                                'vMobile'   =>$vMobile,
                                'tText'     =>$tText,
                                'created'   =>gmdate('Y-m-d'),
                                'modified'  =>gmdate('Y-m-d')
                            );
        $this->db->insert('contacts_master', $insertData);
        return 1;
        
    }

    function aboutUs(){
        $insta_imagePath = INSTA_IMAGE_URL;
        $fb_imagePath = FACEBOOK_IMAGE_URL;
        $tw_imagePath = TWITTER_IMAGE_URL;
        $whatsapp_imagePath = WHATSAPP_IMAGE_URL;
        $youtube_imagePath = YOUTUBE_IMAGE_URL;
        $web_imagePath = WEB_IMAGE_URL;
		$telegram_imagePath = TELEGRAM_IMAGE_URL;
        
        $query= $this->db->query("SELECT
                                        tText,
                                        IF(vInstaCover!='null',CONCAT('$insta_imagePath',vInstaCover),'') AS vInstaCover,
                                        vInstaName,
                                        vInstaUrl,
                                        IF(vTwitterCover!='null',CONCAT('$tw_imagePath',vTwitterCover),'') AS vTwitterCover,
                                        vTwitterName,
                                        vTwitterUrl,
                                        IF(vFacebookCover!='null',CONCAT('$fb_imagePath',vFacebookCover),'') AS vFacebookCover,
                                        vFacebookName,
                                        vFacebookUrl,
                                        IF(vWhatsappCover!='null',CONCAT('$whatsapp_imagePath',vWhatsappCover),'') AS vWhatsappCover,
                                        vWhatsappName,
                                        vWhatsappUrl,
                                        IF(vYoutubeCover!='null',CONCAT('$youtube_imagePath',vYoutubeCover),'') AS vYoutubeCover,
                                        vYoutubeName,
                                        vYoutubeUrl,
										IF(vWebCover!='null',CONCAT('$web_imagePath',vWebCover),'') AS vWebCover,
                                        vWebName,
                                        vWebUrl,
                                        IF(vTelegramCover!='null',CONCAT('$telegram_imagePath',vTelegramCover),'') AS vTelegramCover,
                                        vTelegramName,
                                        vTelegramUrl
                                        FROM aboutus_master u
                                        WHERE iAboutusMasterID = 1");
        
        if ($query->num_rows() > 0){
            $result = array();
            $data = $query->row_array();
            $result['tText'] = $data['tText'];
            $result['name'][0] = $data['vInstaName'];
            $result['cover'][0] = $data['vInstaCover'];
            $result['url'][0] = $data['vInstaUrl'];

            $result['name'][1] = $data['vTwitterName'];
            $result['cover'][1] = $data['vTwitterCover'];
            $result['url'][1] = $data['vTwitterUrl'];

            $result['name'][2] = $data['vFacebookName'];
            $result['cover'][2] = $data['vFacebookCover'];
            $result['url'][2] = $data['vFacebookUrl'];

            $result['name'][3] = $data['vWhatsappName'];
            $result['cover'][3] = $data['vWhatsappCover'];
            $result['url'][3] = $data['vWhatsappUrl'];

            $result['name'][4] = $data['vYoutubeName'];
            $result['cover'][4] = $data['vYoutubeCover'];
            $result['url'][4] = $data['vYoutubeUrl'];
			
			$result['name'][5] = $data['vWebName'];
            $result['cover'][5] = $data['vWebCover'];
            $result['url'][5] = $data['vWebUrl'];

            $result['name'][6] = $data['vTelegramName'];
            $result['cover'][6] = $data['vTelegramCover'];
            $result['url'][6] = $data['vTelegramUrl'];

            return $result;
            
        }
        else
            return array();
    }

    function fullpageAdvertise(){
        $imagePath = DOMAIN_URL.'/admin/';
        $query= $this->db->query("SELECT
                                        IF(vCoverImage!='null',CONCAT('$imagePath',vCoverImage),'') AS vCoverImage
                                        FROM advertise_master u
                                        WHERE eType = 1 AND isStatus = 'Active'");
        
        if ($query->num_rows() > 0)
             return $query->result_array();
        else
            return array();
    }

    function sliderAdvertise(){
        $imagePath = DOMAIN_URL.'/admin/';
        
        $query= $this->db->query("SELECT
                                        vTitle,
                                        IF(vCoverImage!='null',CONCAT('$imagePath',vCoverImage),'') AS vCoverImage,
                                        IF(vDetailsImage!='null',CONCAT('$imagePath',vDetailsImage),'') AS vDetailsImage
                                        FROM advertise_master u
                                        WHERE eType = 2 
                                        ORDER BY iOrder ASC");
        
        if ($query->num_rows() > 0)
             return $query->result_array();
        else
            return array();
    }

    function serviceProviderAdvertise($iSubCategoryID){
        $imagePath = DOMAIN_URL.'/admin/';
        $query= $this->db->query("SELECT
                                        vTitle,
                                        IF(vCoverImage!='null',CONCAT('$imagePath',vCoverImage),'') AS vCoverImage,
                                        IF(vDetailsImage!='null',CONCAT('$imagePath',vDetailsImage),'') AS vDetailsImage
                                        FROM advertise_master u
                                        WHERE eType = 3 AND iSubCategoryID = $iSubCategoryID");
        
        if ($query->num_rows() > 0)
             return $query->row_array();
        else
            return array();
    }

    
    function getSetting(){

        $query = $this->db->get_where('tbl_setting', array('vFieldName' => 'vDeletePassword'));
        if ($query->num_rows() > 0)
            return $query->row_array();
        else
            return array();
    }
    
    function checkPhoneNumberForOperator($vPhone){

        $query = $this->db->get_where('operator_master', array('vPhoneNo' => $vPhone,'isDeleted'=>'No'));
        if ($query->num_rows() > 0)
            return $query->row_array();
        else
            return array();
    }

    function checkPhoneAvailableForLogin($vPhone,$vPassword)
     {
        
        $query= $this->db->query("SELECT
                                        *
                                        FROM tbl_user u
                                        INNER JOIN tbl_user_detail ud on u.iUserID = ud.iUserID
                                        WHERE vPhone = $vPhone  AND vPassword = '".md5($vPassword)."'");
        
        if ($query->num_rows() > 0)
             return $query->row_array();
        else
            return array();
    }

    function verifyCompanyEmploye($vEmail,$vPassword,$vEmployePhone){
        
        $query= $this->db->query("SELECT
                                        *,
                                        c.vCompanyName
                                        FROM tbl_employe e
                                        INNER JOIN tbl_company c on c.iCompanyID = e.iCompanyID 
                                        WHERE vEmployeEmail  = '$vEmail' AND vEmployePhone = '$vEmployePhone' AND vEmployePassword = '".md5($vPassword)."'");
        
        if ($query->num_rows() > 0)
             return $query->row_array();
        else
            return array();
    }

    

    function updateUserProfile($postData) {
        extract($postData);

        if($eType == 'privateUser'){
            if(isset($iUserID) && $iUserID!=''){
                $updateData = array(
                                    'vUserName'         =>$vName,
                                    'vUserEmail'        =>$vEmail,
                                    'vPhoneNumber'  =>$vPhone,
                                    'vUserAddress'      =>$vAddress
                                );

                $this->db->update('tbl_user', $updateData, array('iUserID' => $iUserID));
                return $iUserID;
            }else{

                $insertData = array(
                                    'vUserName'         =>$vName,
                                    'vUserEmail'        =>$vEmail,
                                    'vPhoneNumber'  =>$vPhone,
                                    'vUserAddress'      =>$vAddress,
                                    'vDeviceToken'      =>$vDeviceToken,
                                    'vUserProfilePic'   =>'',
                                    'isAlreadyRegister' =>'Yes',
                                    'dtCreated'         =>date('Y-m-d H:i:s')
                                    );
                $this->db->insert('tbl_user', $insertData);
                return $this->db->insert_id();
            }
        }
    }

    function getUserDetailByID($iUserID) {

        $query = $this->db->query("SELECT 
                                    *
                                    FROM tbl_user u
                                    WHERE iUserID = $iUserID
                                ");

        if ($this->db->affected_rows() > 0) {
            return $query->row_array();
        } else {
            return array();
        }
    }

    function historyList() {

        $imagePath = HISTORY_IMAGE_URL;
        $query = $this->db->query("SELECT 
                                        vTitleName,
                                        tDescription,
                                        IF(vImageName <>'',CONCAT('$imagePath',vImageName),'') AS vImageName
                                        FROM history_master
                                        WHERE isStatus = 'active'
                                    ");

        if ($this->db->affected_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function updateDeviceToken($postData){
        extract($postData);
        if($eUserType != 'employe'){
            $updateData = array(
                              'vDeviceToken' => $vDeviceToken,
                            );
            $this->db->update('tbl_user', $updateData, array('iUserID'=>$iUserID,'isDeleted'=>'no'));
        }else{
            $updateData = array(
                              'vDeviceToken' => $vDeviceToken,
                            );
            $this->db->update('tbl_employe', $updateData, array('iEmployeID'=>$iUserID,'isDeleted'=>'no'));
        }
        
        return 1;
    }

     function updateNewPassword($vEmail, $new_password){
        $query = $this->db->where(array('vEmail' => $vEmail ));
        $query = $this->db->update($this->table,array('vPassword'=>$new_password));

        if($this->db->affected_rows()>0)
            return 1;
        else
            return 0;
        
    }

    function checkLike($iNewsID,$vDeviceToken)
    {
         $check = array(
                        'iNewsID'=>$iNewsID,
                        'vDeviceToken'=>$vDeviceToken 
                        );

         $query = $this->db->get_where('tbl_news_like',$check);
        //$query = $this->db->get_where($this->table, array('vEmail' => $vEmail));
        if ($query->num_rows() > 0)
            return 0;
        else
            return 1;
    }

    function addLike($data)
    {
        $this->db->insert('tbl_news_like', $data);
        if ($this->db->affected_rows() > 0)
        {
            return 1;
        } else {
            return 0;
        }
    }

    function addShare($data)
    {
        $this->db->insert('tbl_news_share', $data);
        if ($this->db->affected_rows() > 0)
        {
            return 1;
        } else {
            return 0;
        }
    }

    function shareCount($iNewsID,$vDeviceToken)
    {
        $query= $this->db->query("SELECT count(iNewsShareID) as total FROM tbl_news_share WHERE iNewsID = $iNewsID  AND vDeviceToken = '$vDeviceToken'");
        
        if ($this->db->affected_rows() > 0)
        {
          $row =  $query->row();
          return $row->total;  
        } 
        else
         {
            return 0;
        }
    }
    

    function deleteLike($iNewsID,$vDeviceToken)
    {
         $check = array(
                        'iNewsID'=>$iNewsID,
                        'vDeviceToken'=>$vDeviceToken 
                        );

         $query = $this->db->delete('tbl_news_like', array('iNewsID' => $iNewsID , 'vDeviceToken'=> $vDeviceToken )); 
        //$query = $this->db->get_where($this->table, array('vEmail' => $vEmail));
        if ($this->db->affected_rows() > 0)
            return 1;
        else
            return 0;
    }
    
    function getAnnouncementList($iAnnouncementID)
    {
        $imagePath = ANNOUNCEMENT_IMAGE_URL;
        $videoPath = ANNOUNCEMENT_VIDEO_URL;
        $query = $this->db->query("SELECT 
                                    n.vTitle ,
                                    IF(n.vImage <>'',CONCAT('$imagePath',n.iAnnouncementID,'/',n.vImage),'') AS vImage,
                                    IF(n.vVideo <>'',CONCAT('$videoPath',n.iAnnouncementID,'/',n.vVideo),'') AS vVideo,
                                    n.dtCreatedate 
                                    FROM tbl_announcement n
                                    WHERE iAnnouncementID = $iAnnouncementID
                                    ");

        if ($query->num_rows() > 0)
         {
            return $query->row_array()  ;
        } 
        else
         {
            return array();
        }
    }

    function getNewsList($iLimitID,$vDeviceToken)
    {
      $imagePath = NEWS_IMAGE_URL;      
      $query= $this->db->query("SELECT 
                                    vText,
                                    vTitle,
                                    IF(n.vImage <>'',CONCAT('$imagePath',n.iNewsID,'/',n.vImage),'') AS vImage,
                                     n.dtCreatedate ,
                                    iNewsID,
                                    (SELECT 
                                        COUNT(iNewsLikeID)
                                        FROM tbl_news_like nl
                                        WHERE nl.iNewsID = n.iNewsID) AS likeCount,
                                    (SELECT 
                                        COUNT(iNewsShareID)
                                        FROM tbl_news_share ns
                                        WHERE ns.iNewsID = n.iNewsID AND ns.vDeviceToken = '$vDeviceToken') AS shareCount,
                                    IF( EXISTS(SELECT 
                                                    iNewsLikeID
                                                    FROM tbl_news_like nl
                                                    where nl.vDeviceToken = '$vDeviceToken'  and nl.iNewsID = n.iNewsID ),1,0)  AS is_liked
                                    FROM tbl_news n 
                                    ORDER BY dtCreatedate DESC
                                    LIMIT $iLimitID, 10"
                                    );

      if ($this->db->affected_rows() > 0)
      {
        /*$total = $this->db->query("SELECT iTweetID as total FROM tbl_tweet")->row()->total;
        return array('total'=>$total, 'resultData'=>$query->result_array());*/
        return $query->result_array();
      }
           // return $query->result_array();
        else
            return 0;
    }  

    function getServiceList()
    {
      $imagePath = SERVICE_IMAGE_URL;      
      $query= $this->db->query("SELECT 
                                    vText,
                                    vTitle,
                                    IF(n.vImage <>'',CONCAT('$imagePath',n.iServiceID,'/',n.vImage),'') AS vImage,
                                    n.dtCreatedate
                                    FROM tbl_service n 
                                    ORDER BY dtCreatedate DESC
                                    ");

      if ($this->db->affected_rows() > 0)
      {
        /*$total = $this->db->query("SELECT iTweetID as total FROM tbl_tweet")->row()->total;
        return array('total'=>$total, 'resultData'=>$query->result_array());*/
        return $query->result_array();
      }
           // return $query->result_array();
        else
            return 0;
    }

     function getFavouriteNewsList($vDeviceToken)
    {
      $imagePath = NEWS_IMAGE_URL;      
      $query= $this->db->query("SELECT 
                                    vText,
                                    vTitle,
                                    IF(n.vImage <>'',CONCAT('$imagePath',n.iNewsID,'/',n.vImage),'') AS vImage,
                                    n.dtCreatedate ,
                                    n.iNewsID,
                                    (SELECT 
                                        COUNT(iNewsLikeID)
                                        FROM tbl_news_like nl
                                        WHERE nl.iNewsID = n.iNewsID) AS likeCount,
                                    (SELECT 
                                        COUNT(iNewsShareID)
                                        FROM tbl_news_share ns
                                        WHERE ns.iNewsID = n.iNewsID AND ns.vDeviceToken = '$vDeviceToken') AS shareCount,
                                    IF( EXISTS(SELECT 
                                                    iNewsLikeID
                                                    FROM tbl_news_like nl
                                                    where nl.vDeviceToken = '$vDeviceToken'  and nl.iNewsID = n.iNewsID ),1,0)  AS is_liked
                                    FROM tbl_news n 
                                    INNER JOIN tbl_news_like l
                                    ON l.iNewsID = n.iNewsID
                                    WHERE l.vDeviceToken = '$vDeviceToken'
                                    ORDER BY dtCreatedate DESC
                                    "
                                    );

      if ($this->db->affected_rows() > 0)
      {
        /*$total = $this->db->query("SELECT iTweetID as total FROM tbl_tweet")->row()->total;
        return array('total'=>$total, 'resultData'=>$query->result_array());*/
        return $query->result_array();
      }
           // return $query->result_array();
        else
            return 0;
    }

    function searchResultOfNews($iLimitID,$vDeviceToken,$vTitle)
    {
      $imagePath = NEWS_IMAGE_URL;      
      $query= $this->db->query("SELECT 
                                    vText,
                                    vTitle,
                                    IF(n.vImage <>'',CONCAT('$imagePath',n.iNewsID,'/thumb/',n.vImage),'') AS vImage,
                                     n.dtCreatedate ,
                                    iNewsID,
                                    (SELECT 
                                        COUNT(iNewsLikeID)
                                        FROM tbl_news_like nl
                                        WHERE nl.iNewsID = n.iNewsID) AS likeCount,
                                    (SELECT 
                                        COUNT(iNewsShareID)
                                        FROM tbl_news_share ns
                                        WHERE ns.iNewsID = n.iNewsID AND ns.vDeviceToken = '$vDeviceToken') AS shareCount,
                                    IF( EXISTS(SELECT 
                                                    iNewsLikeID
                                                    FROM tbl_news_like nl
                                                    where nl.vDeviceToken = '$vDeviceToken'  and nl.iNewsID = n.iNewsID ),1,0)  AS is_liked
                                    FROM tbl_news n 
                                    WHERE vTitle like '%$vTitle%'
                                    ORDER BY dtCreatedate DESC
                                    LIMIT $iLimitID, 10"
                                    );

      if ($this->db->affected_rows() > 0)
      {
        /*$total = $this->db->query("SELECT iTweetID as total FROM tbl_tweet")->row()->total;
        return array('total'=>$total, 'resultData'=>$query->result_array());*/
        return $query->result_array();
      }
           // return $query->result_array();
        else
            return 0;
    } 

    function getNewsCount()
    {
        $query = $this->db->query("SELECT 
                                    COUNT(iNewsID) as total
                                        FROM tbl_news nl 
                                      ");

        if ($query->num_rows() > 0)
         {
            $row = $query->row();
            //mprd($row);
          return $row ->total;
        } 
        else
         {
            return array();
        }
    }

    

  function getCityList()
    {
        $query = $this->db->query("SELECT iCityID,vCityName FROM tbl_city");
        if ($query->num_rows() > 0)
         {
            return $query->result_array();
        } 
        else
         {
            return array();
        }
    } 

  function getAboutUsDetail()
    {
        $query = $this->db->query("SELECT tContent,vPageTitle FROM tbl_pagecontent WHERE iPageID ='2'");
        if ($query->num_rows() > 0)
         {
            return $query->row_array();
        } 
        else
         {
            return array();
        }
    }

    function getOrederTypeList()
    {
        $query = $this->db->query("SELECT * FROM tbl_order_type");
        if ($query->num_rows() > 0)
         {
            return $query->result_array();
        } 
        else
         {
            return array();
        }
    }

    function getHospitalList($iCityID)
    {
        
        $query = $this->db->query("SELECT iHospitalID,vName FROM tbl_hospital WHERE iCityID = $iCityID");
        if ($query->num_rows() > 0)
         {
            return $query->result_array();
        } 
        else
         {
            return array();
        }
    } 

  function getAllHospitalList()
    {
         $query = $this->db->query("SELECT 
                                      c.iCityID,
                                      h.vName,
                                      h.iHospitalID
                                       FROM tbl_hospital h
                                      INNER JOIN tbl_city c
                                      ON c.iCityID = h.iCityID
                                       ");
        if ($query->num_rows() > 0)
         {
            return $query->result_array();
        } 
        else
         {
            return array();
        }
    }

    function getFaqList()
    {
        
        $query = $this->db->query("SELECT iFaqID,vQuestion,vAnswer FROM tbl_faq WHERE vAnswer != '' ORDER BY iFaqID DESC");
        if ($query->num_rows() > 0)
         {
            return $query->result_array();
        } 
        else
         {
            return array();
        }
    }

    
  function getFaqCategoryList()
    {
        $query = $this->db->query("SELECT iFaqCatID,vFaqCatName FROM tbl_faq_category ");
        if ($query->num_rows() > 0)
         {
            return $query->result_array();
        } 
        else
         {
            return array();
        }
    }

    function addComplain($postData)
     {
        extract($postData);

        $data = array(
                        'iHospitalID'=>$iHospitalID,
                        'vName'=>$vName,
                        'vEmail'=>$vEmail,
                        'vPhone'=>$vPhone,
                        'vComplainTitle'=>$vComplainTitle,
                        'vContent'=>$vContent,
                        'dtCreatedate'=>gmdate('Y-m-d: H:i:s')
          );

        
        $query = $this->db->insert('tbl_complain', $data);
        //mprd($this->db->last_query());
        if ($this->db->affected_rows() > 0)
         {
            return $this->db->insert_id();
        }
        else
         {
            return 0;
          }
    }

    function addFaq($postData)
     {
        extract($postData);
        $data = array(
                        'vQuestion'=>$vQuestion,
                        'vName'=>$vName,
                        'vEmail'=>$vEmail,
                        'vPhone'=>$vPhone,
                        'eVisibility'=>'2',
                        'isAdded'=>'user',
                        'iFaqCatID'=>$iFaqCatID
          );
        
        $query = $this->db->insert('tbl_faq', $data);
        //mprd($this->db->last_query());
        if ($this->db->affected_rows() > 0)
         {
            return $this->db->insert_id();
        }
         else {
            return 0;
        }
    }


    function addOrder($postData)
     {
        extract($postData);
        //mprd(rand(100000,1000000));
        $postData['eOrderStatus'] = '1';
        $postData['iOrderNumber'] = rand(100000,1000000);
        $postData['isSeen'] = '0';
        $postData['eStatus'] = 'Active';
        $postData['dtCreatedate'] = gmdate('Y-m-d: H:i:s');
        $query = $this->db->insert('tbl_order', $postData);
        //mprd($this->db->last_query());
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return 0;
        }
    }

    function updateOrderDetail($postData)
     {
        extract($postData);
        //mprd(rand(100000,1000000));
        $updateData = array(
                          'vIssue'=>$vIssue,
                          'vUrl'=>$vUrl,
                          'eOrderType'=>$eOrderType,
                          'vName'=>$vName,
                          'vFileNumber'=>$vFileNumber,
                          'vPhoneNumber'=>$vPhoneNumber,
                          'vPinCode'=>$vPinCode,
                          'iCityID'=>$iCityID,
                          'iHospitalID'=>$iHospitalID,
                          'vDeviceToken'=>$vDeviceToken,
                          'vAPNSToken'=>$vAPNSToken,
                          'vEmail'=>$vEmail,
                          'ePlatform'=>$ePlatform,
                          'isSeen'=>'0',
                          'isAdminSeen'=>'0',
                          'eStatus'=>'Active'
                          );
       
        $query = $this->db->update('tbl_order', $updateData, array('iOrderID' => $iOrderID));
        //mprd($this->db->last_query());
        if($this->db->affected_rows()>0)
            return 1;
        else
            return 0;
    }

    function updateOrderReport($postData)
     {
        extract($postData);
        //mprd(rand(100000,1000000));
        $updateData = array(
                          'vReport'=>$vReport,
                          );
       
        $query = $this->db->update('tbl_order', $updateData, array('iOrderID' => $iOrderID));
        //mprd($this->db->last_query());
        if($this->db->affected_rows()>0)
            return 1;
        else
            return 0;
    }

    function getOrderList($postData)
    {
        extract($postData);

/*         $length =  strlen($vPhoneNumber);

        // mprd($length);
         if($length == '10')
         {
           $number =  substr($vPhoneNumber,1);
         }*/


       if($vPhoneNumber!='' AND $iOrderNumber!='' AND $vPinCode!='')
       {
            $query = $this->db->query("SELECT
                                   o.iOrderID, 
                                   o.iOrderNumber,
                                   o.vIssue,
                                   o.vUrl,
                                   o.vEmail,
                                   o.vName,
                                   DATE_FORMAT(o.dtCreatedate, '%h:%i %p, %D %M %Y') as dtCreatedate,
                                   o.eOrderStatus,
                                   o.eOrderType,
                                   o.vReply,
                                   os.vArabic as vOrderStatus,
                                   ot.vOrderTypeName,
                                   o.vFileNumber,
                                   o.vPinCode,
                                   o.iCityID,
                                   o.iHospitalID,
                                   c.vCityName,
                                   o.vPhoneNumber,
                                   h.vName  as vHospital
                                   FROM tbl_order o
                                   INNER JOIN tbl_order_type ot ON ot.iOrderTypeID = o.eOrderType
                                   INNER JOIN tbl_status os ON os.iStatusID = o.eOrderStatus
                                   INNER JOIN tbl_city c ON o.iCityID = c.iCityID
                                   INNER JOIN tbl_hospital h ON o.iHospitalID = h.iHospitalID
                                   WHERE vPhoneNumber = '$vPhoneNumber' or vPhoneNumber = IF(LENGTH($vPhoneNumber) = 10 , SUBSTRING($vPhoneNumber,1),$vPhoneNumber)
                                   AND vPinCode = '$vPinCode'
                                   AND iOrderNumber = $iOrderNumber
                                   ORDER BY dtCreatedate DESC
                                    ");
       }
       else if($vPhoneNumber!='' AND $vPinCode!='')
       {
            $query = $this->db->query("SELECT
                                       o.iOrderID, 
                                       o.iOrderNumber,
                                       o.vIssue,
                                       o.vUrl,
                                       o.vEmail,
                                       o.vName,
                                       DATE_FORMAT(o.dtCreatedate, '%h:%i %p, %D %M %Y') as dtCreatedate,
                                       o.eOrderStatus,
                                       os.vArabic as vOrderStatus,
                                       o.eOrderType,
                                       o.vReply,
                                       ot.vOrderTypeName,
                                       o.vFileNumber,
                                       o.vPinCode,
                                       o.iCityID,
                                       o.iHospitalID,
                                       c.vCityName,
                                       o.vPhoneNumber,
                                       h.vName  as vHospital
                                       FROM tbl_order o
                                       INNER JOIN tbl_order_type ot ON ot.iOrderTypeID = o.eOrderType
                                       INNER JOIN tbl_status os ON os.iStatusID = o.eOrderStatus
                                       INNER JOIN tbl_city c ON o.iCityID = c.iCityID
                                       INNER JOIN tbl_hospital h ON o.iHospitalID = h.iHospitalID
                                       AND vPinCode = '$vPinCode'
                                       WHERE vPhoneNumber = '$vPhoneNumber' or vPhoneNumber = IF(LENGTH($vPhoneNumber) = 10 , SUBSTRING($vPhoneNumber,1),$vPhoneNumber)
                                       AND vPinCode= '$vPinCode'
                                       ORDER BY dtCreatedate DESC
                                        ");
       }

       else if($iOrderNumber!='')
       {
            $query = $this->db->query("SELECT
                                       o.iOrderID, 
                                       o.iOrderNumber,
                                       o.vIssue,
                                       o.vUrl,
                                       o.vEmail,
                                       o.vName,
                                       DATE_FORMAT(o.dtCreatedate, '%h:%i %p, %D %M %Y') as dtCreatedate,
                                       o.eOrderStatus,
                                       os.vArabic as vOrderStatus,
                                       o.eOrderType,
                                       o.vReply,
                                       ot.vOrderTypeName,
                                       o.vFileNumber,
                                       o.vPinCode,
                                       o.iCityID,
                                       o.iHospitalID,
                                       c.vCityName,
                                       o.vPhoneNumber,
                                       h.vName  as vHospital
                                       FROM tbl_order o
                                       INNER JOIN tbl_order_type ot ON ot.iOrderTypeID = o.eOrderType
                                       INNER JOIN tbl_status os ON os.iStatusID = o.eOrderStatus
                                       INNER JOIN tbl_city c ON o.iCityID = c.iCityID
                                       INNER JOIN tbl_hospital h ON o.iHospitalID = h.iHospitalID
                                       WHERE iOrderNumber = $iOrderNumber
                                       ORDER BY dtCreatedate DESC
                                    ");
       }
        

        if ($query->num_rows() > 0)
         {
            return $query->result_array()  ;
        } 
        else
         {
            return array();
        }
    } 

    function getOrderDetail($postData)
    {
        extract($postData);


        $query = $this->db->query("SELECT
                                   o.iOrderID, 
                                   o.iOrderNumber,
                                   o.vIssue,
                                   o.vUrl,
                                   o.vEmail,
                                   o.vReply,
                                   o.vName,
                                   DATE_FORMAT(o.dtCreatedate, '%h:%i %p, %D %M %Y') as dtCreatedate,
                                   o.eOrderStatus,
                                   os.vArabic as vOrderStatus,
                                   o.eOrderType,
                                   ot.vOrderTypeName,
                                   o.vFileNumber,
                                   o.vPinCode,
                                   o.iCityID,
                                   o.iHospitalID,
                                   c.vCityName,
                                   o.vPhoneNumber,
                                   h.vName  as vHospital
                                   FROM tbl_order o
                                   INNER JOIN tbl_order_type ot ON ot.iOrderTypeID = o.eOrderType
                                   INNER JOIN tbl_status os ON os.iStatusID = o.eOrderStatus
                                   INNER JOIN tbl_city c ON o.iCityID = c.iCityID
                                   INNER JOIN tbl_hospital h ON o.iHospitalID = h.iHospitalID
                                   WHERE iOrderID = $iOrderID
                                   ORDER BY dtCreatedate DESC
                                   ");
        if ($query->num_rows() > 0)
         {
            return $query->row_array()  ;
        } 
        else
         {
            return array();
        }
    }


    function getMyOrderList($postData)
    {
        extract($postData);
        $query = $this->db->query("SELECT
                                   o.iOrderID, 
                                   o.iOrderNumber,
                                   o.vIssue,
                                   o.vUrl,
                                   o.vEmail,
                                   o.vName,
                                   DATE_FORMAT(o.dtCreatedate, '%h:%i %p, %D %M %Y') as dtCreatedate,
                                   o.eOrderStatus,
                                   os.vArabic as vOrderStatus,
                                   o.eOrderType,
                                   o.vReply,
                                   ot.vOrderTypeName,
                                   o.vFileNumber,
                                   o.vPinCode,
                                   o.iCityID,
                                   o.iHospitalID,
                                   c.vCityName,
                                   o.vPhoneNumber,
                                   h.vName  as vHospital
                                   FROM tbl_order o
                                   INNER JOIN tbl_order_type ot ON ot.iOrderTypeID = o.eOrderType
                                   INNER JOIN tbl_status os ON os.iStatusID = o.eOrderStatus
                                   INNER JOIN tbl_city c ON o.iCityID = c.iCityID
                                   INNER JOIN tbl_hospital h ON o.iHospitalID = h.iHospitalID
                                   WHERE vDeviceToken = '$vDeviceToken'
                                   ORDER BY o.iOrderID DESC
                                    ");

  //mprd($this->db->last_query());      
        if ($query->num_rows() > 0)
         {
            return $query->result_array()  ;
        } 
        else
         {
            return array();
        }
    }
    

    function updateUserLatLong($dLat,$dLong,$iUserID)
    {
        $updateData = array('dLat'=>$dLat,'dLong'=>$dLong);
        $query = $this->db->update('tbl_user', $updateData, array('iUserID' => $iUserID));

         
    }

    function getUserDetails($postData)
     {
        extract($postData);
        $this->db->select("iStaffID, vEmail,vName");
        $query = $this->db->get_where($this->table, array('vEmail' => $vEmail, 'vPassword' => md5($vPassword)));
        //mprd($this->db->last_query());
        if ($query->num_rows() > 0)
         {
            $res = $query->row_array();
            $updateData = array(
                'vAPNSToken' => $vAPNSToken,
                'ePlatform' => $ePlatform,
                //'vName'=>$vName
            );
            $this->db->update('tbl_staff', $updateData, array('iStaffID' => $res['iStaffID']));
            return $res;
        } 
        else
         {
            return '';
        }
    }

    function getvHmacFromEmail($vEmail) {
        $this->db->select("vHmac");
        return $this->db->get_where($this->table, array('vEmail' => $vEmail))->row()->vHmac;
    }

  function addUser($postData)
     {
        extract($postData);
        
        $addData = array(
                    'vEmail'=>$vEmail,
                    'vPassword'=>md5($vPassword),
                    'vPhone'=>$vPhone,
                    'ePlatform'=>$ePlatform,
                    'vDeviceToken'=>$vDeviceToken,
                    'eStatus'=>'Inactive',
                    'is_delete'=>'no',
                    'dtCreated'=>date('Y-m-d: H:i:s')
            );
        
        $query = $this->db->insert($this->table, $addData);
        //mprd($this->db->last_query());
        if ($this->db->affected_rows() > 0)
         {
            $iUserID = $this->db->insert_id();

            $addSubData = array(
                    'iUserID'=>$iUserID,
                    'vName'=>$vName,
                    'dDob'=>$dDob,
                    'eSex'=>$eSex,
                    'vHospitalName'=>$vHospitalName,
                    'vAddress'=>$vAddress,
                    'vWebsite'=>$vWebsite,
                    'vSpecialization'=>$vSpecialization,
                    'vRegNo'=>$vRegNo,
                    'dtCreatedDate'=>date('Y-m-d: H:i:s')
                    );
            $query = $this->db->insert('tbl_user_detail', $addSubData);   

            return $iUserID;
        } else {
            return 0;
        }
    }


  function addNotificationSetting($postData)
     {
        extract($postData);
        
        $data = array(
                      'iEservices'=>$iEservices,
                      'iNews'=>$iNews,
                      'iCircular'=>$iCircular,
                      'iSilent'=>$iSilent,
                      'iLocation'=>$iLocation,
                      'vAPNSToken'=>$vAPNSToken,
                      'ePlatform'=>$ePlatform,
                      'dLat'=>$dLat,
                      'dLong'=>$dLong
          );
        
        $query = $this->db->insert('tbl_notification_setting', $data);
        //mprd($this->db->last_query());
        if ($this->db->affected_rows() > 0)
         {
            return $this->db->insert_id();
        } else {
            return 0;
        }
    }

    function updateNotificationSetting($postData)
     {
        extract($postData);
        
        $data = array(
                      'iEservices'=>$iEservices,
                      'iNews'=>$iNews,
                      'iCircular'=>$iCircular,
                      'iSilent'=>$iSilent,
                      'iLocation'=>$iLocation,
                      'ePlatform'=>$ePlatform,
                      'dLat'=>$dLat,
                      'dLong'=>$dLong
          );
        
        $query = $this->db->update('tbl_notification_setting',$data,array('vAPNSToken'=>$vAPNSToken));
        //mprd($this->db->last_query());
        if ($this->db->affected_rows() > 0)
         {
            return 1;
        } else {
            return 0;
        }
    }

    function getProfilePic($iUserID)
    {
        $this->db->select("vProfilePic");
        return $this->db->get_where($this->table, array('iUserID' => $iUserID))->row()->vProfilePic;
    }
    
    function checkEmailAvailable($vEmail)
     {
        $query= $this->db->query("SELECT
                                        *
                                        FROM tbl_user u
                                        INNER JOIN tbl_user_detail ud on u.iUserID = ud.iUserID
                                        WHERE vEmail = '$vEmail'");
        
        if ($query->num_rows() > 0)
             return $query->row_array();
        else
            return 0;
    }

}

/* End of file admin_model.php */

/* Location: ./application/ws/models/admin_model.php */