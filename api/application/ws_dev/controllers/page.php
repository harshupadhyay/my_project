<?php



class Page extends CI_Controller

{   

    function __construct()

    {

        parent::__construct();

		//$this->load->library('Datatables.php');

        $this->load->model('page_model');   

        $this->controller = 'page';    

    }

    // INDEX

    function term(){   
        $iPageID = 1;
        $pageData = $this->page_model->getPage($iPageID);
        echo $pageData['tContent'];

    }

    function about(){   
        $iPageID = 2;
        $pageData = $this->page_model->getPage($iPageID);
        echo $pageData['tContent'];

    }

   

}