<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require(APPPATH . '/libraries/REST_Controller.php');

class User extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->table = 'tbl_user';
        
        if (!checkmac()) {
            $row = array("MESSAGE" => "You are not authorize to use this service", "SUCCESS" => '0');
            $this->response($row, 403);
        }
        $this->load->model('user_model');
    }


    function allDataForLocal_post() {
        $postData = $this->post();
        extract($postData);
        $finalArr = array();
        $finalArr['allVessel'] = $this->user_model->getVesselList();
        $finalArr['allLoading'] = $this->user_model->getLoadingList();
        $finalArr['allUnLoading'] = $this->user_model->getUnLoadingList();
        $finalArr['allTransporter'] = $this->user_model->getTransporterList();
        $finalArr['otherDetail'] = $this->user_model->getOtherDetail();
        
        if ($finalArr) {
            $this->send_success($finalArr);
        } else {
            $this->send_fail('No Data Found');
        }
    }

    function getPineWoodConsignmentDataByuser_post() {
        $postData = $this->post();
        extract($postData);
        
        $allPineWoodData = $this->user_model->getPineWoodConsignmentDataByuser();
        /*$result = array(
                        'file'=> 'http://kavitacareclinic.com/shyamimpex/jsondata/newfile.txt'
                    );*/
        if ($allPineWoodData) {
            $this->send_success($allPineWoodData);
        } else {
            $this->send_fail('Please try again');
        }
    }

    function addAllPineTransaction_post() {
        date_default_timezone_set('Asia/Calcutta'); 
        $postData = $this->post();
        extract($postData);
        /*$finalArr = array(
                        'consignment_id'    =>"4",
                        'truckno'           =>"GJ12AJ8520",
                        'iTransporterID'    =>"1",
                        'drivername'        =>"Rohit",
                        'remark'            =>"Fast delivary",
                        'iVesselID'         =>"1",
                        'iLoadingMasterID'  =>"1",
                        'shift'             =>"1",
                        'iUnloadingMasterID'=>"1",
                        'stack_1'=> array('tallyData'=>array("ZA1898100","ZA1898099","ZA1897609","ZA1898137"),
                                            "nontallyCount"=>"10",
                                            "lot_no"=>"1",
                                            "length"=>"6"),
                        'stack_2'=> array('tallyData'=>array("ZA1898100","ZA1898099","ZA1897609","ZA1898137"),
                                            "nontallyCount"=>"10",
                                            "lot_no"=>"1",
                                            "length"=>"6")
                            );
        $data = json_encode($finalArr,true);
        mprd($data);*/
        //mprd($postData['data']);
        $data = json_decode($postData['data'],true);

        $allPineWoodDate = $this->user_model->addAllPineTransaction($data);
        
        if ($allPineWoodDate) {
            $this->send_success_with_message('Data updated successfully');
        } else {
            $this->send_fail('Please try again');
        }
    }

    function updateStatus_post() {
        $postData = $this->post();
        extract($postData);
        $isUpdate = $this->user_model->updateStatus($postData);
        if($isUpdate != 0){
            $this->send_success_with_message('Delivered successfully');
        }else{
            $this->send_fail('Please try again');
        }
    }

    function allNurse_post() {
        $allNurse = $this->user_model->allNurse();

        if (!empty($allNurse)) {
            $this->send_success($allNurse);
        } else {
            $this->send_fail('No Data Found');
        }
    }

    function removePrescriptionMaster_post() {
        $postData = $this->post();
        extract($postData);
        $isDelete = $this->user_model->removePrescriptionMaster($iPrescriptionMasterID);

        if ($isDelete) {
            $data = array(
                'MESSAGE' => 'Removed Prescription successfully',
                'SUCCESS' => '1'
            );
            echo json_encode($data);
        } else {
            $data = array(
                'MESSAGE' => 'Please try again',
                'SUCCESS' => '0'
            );
            echo json_encode($data);
        }
    }

    function logout_get() {
        $key = $_SERVER['HTTP_ACCESSTOKEN'];
        $iUserID = getuserid();

        $updateToken = array(
                                'vDeviceToken' => ''
                            );
        $this->db->update('tbl_user', $updateToken, array('iUserID' => $iUserID));

        $data = array('vHmac' => $key);

        $this->db->delete('tbl_hmac', $data);

        if ($this->db->affected_rows() > 0) {
            $data = array('Message' => 'Logout successfully');
            $this->send_success($data);
        } else {
            $this->send_fail('Please try again');
        }
    }

    function send_fail($msg) {
        $row = array("MESSAGE" => "$msg", "SUCCESS" => 0);
        $this->response($row, 200);
    }

    function send_success($data) {
        $row = array("DATA" => $data, "SUCCESS" => 1);
        $this->response($row, 200);
    }

    function send_success_with_message($message) {
        $row = array("MESSAGE" => $message, "SUCCESS" => 1);
        $this->response($row, 200);
    }

}
