 <div id="main-content">
        <!-- Header -->
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-light sidebar-nav align-items-start bg-color-gray py-40px py-md-80 px-40px px-md-35">
            <div class="container-fluid">
                <a class="navbar-brand" href="index.php">
                    <img class="etcodes-normal-logo" src="images/logo1.png" width="190" height="115" alt="Logo">
                    <img class="etcodes-mobile-logo" src="images/logo1.png" width="190" height="115" alt="Logo">

                </a>
                <button class="navbar-toggler hamburger-menu-btn" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span>toggle menu</span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                      <li class="nav-item">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
						<li class="nav-item">
                            <a class="nav-link" href="page-about-us.html">About Us</a>
                        </li>
                       <!--   <li class="nav-item">
                            <a class="nav-link" href="shop-right-sidebar.html">Shop</a>
                        </li>  -->
                       
                    <li class="nav-item">
                        <a class="nav-link" href="page-contact.html">Contact Us</a>
                    </li>
                    </ul>
                </div>
                <div class="bar-module">
                    <!-- <div class="mb-15px">
                        <a href="https://www.energeticthemes.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="a5edc0c9c9cae5d0c8c1c48bc6cac8">[email&#160;protected]</a>
                        <br> + 08455-3354-202
                    </div> -->
                    <ul class="list-inline m-0">
                        <li class="list-inline-item">
                           <a class="navbar-brand" href="https://play.google.com/store/apps/details?id=com.vnerds.vignyapan&hl=en">
                    <img class="etcodes-normal-logo" src="images/logo.png" width="190" height="115" alt="Logo">
                    <img class="etcodes-mobile-logo" src="images/logo.png" width="190" height="115" alt="Logo">

                <h6><b>Vignayapan App For Android</h6></a>
                        </li>
                        <!--<li class="list-inline-item">
                            <a href="https://instagram.com/energeticthemes">
                                <i class="fab fa-instagram" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://behance.com/energeticthemes">
                                <i class="fab fa-behance" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://twitter.com/energeticthemes">
                                <i class="fab fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>-->
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Header -->
        <!-- main -->
        <div class="container-for-sidebar-menu page-container scene-main scene-main--fade_In mt-60px ">
            <!-- Work  -->
            <div class="bg-img-7">
                <div class="container">
                    <div class="row align-items-center mb-30px">
                        <div class="col-md-8">
                            <h4>Vignayapan Category</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                           <ul class="items_filter">
                        
									<?php
                                        $item = $db->selectQuery('select iCategoryMasterID,vCategoryName,vImageName  from category_master');	?>
                                       
                                            <li>
											<span data-filter=".si"><?php echo $item[0]['vCategoryName'];?></span>
											</li>
											
											<li>
											<span data-filter=".jb"><?php echo $item[1]['vCategoryName'];?></span>
											</li>
											
											<li>
											<span data-filter=".kl"><?php echo $item[2]['vCategoryName'];?></span>
											</li>
											
											<li>
											<span data-filter=".dhs"><?php echo $item[3]['vCategoryName'];?></span>
											</li>
											
											 <!--  <li>
											<span data-filter=".pl"><?php echo $item[4]['vCategoryName'];?></span>
											</li> -->
											
											<li>
											<span data-filter=".fst"><?php echo $item[5]['vCategoryName'];?></span>
											</li>
											
											<li>
											<span data-filter=".kak"><?php echo $item[6]['vCategoryName'];?></span>
											</li>
								
									
                           </ul>
                            <div class="row et-items masonry_layout mb-20px">
							
							 <?php
                                        $item = $db->selectQuery('select s.iSubCategoryID,s.vCoverImage,s.iCategoryMasterID,c.vCategoryName from  sub_category_master s LEFT JOIN category_master c on s.iCategoryMasterID = c.iCategoryMasterID where c.vCategoryName ="Sunday Issues"');
                                        
									if($item ){ 
									foreach($item as $loc){ ?>
								<article class="col-md-6 col-lg-4 et-item si">
                                    <div class="et-banner">
                                        <figure class="effect-default">
                                            <img src="admin/images/subcategory/cover_image/<?php echo $loc['vCoverImage']; ?>" alt="product image" width="450" height="536" />
                                            <figcaption>
                                                <a class="banner-link-cover" href="admin/images/subcategory/<?php echo $loc['vCoverImage']; ?>" alt="product image" width="450" height="536">View More</a>

                                            </figcaption>
                                        </figure>
                                        
                                    </div>
                                </article>
                                    <?php }} ?>
									
									
								
								 <?php
                                        $item = $db->selectQuery('select i.vImageName from category_image i LEFT JOIN category_master m on m.iCategoryMasterID = i.iCategoryMasterID where m.vCategoryName ="Job"');
                                        
                                        foreach($item as $loc){ ?>
                                <article class="col-md-6 col-lg-4 et-item jb">
                                    <div class="et-banner">
                                        <figure class="effect-default">
                                            <img src="admin/images/category_Image/<?php echo $loc['vImageName']; ?>" alt="product image" width="540" height="450" />
                                            <figcaption>
                                                <a class="banner-link-cover" href="#">View More</a>
                                            </figcaption>
                                        </figure>
                                      
                                    </div>
                                </article>
								 <?php } ?>
								 
								  <?php
                                        $item = $db->selectQuery('select i.vImageName from category_image i LEFT JOIN category_master m on m.iCategoryMasterID = i.iCategoryMasterID where m.vCategoryName ="Kalol Live"');
                                        
                                        foreach($item as $loc){ ?>
                                <article class="col-md-6 col-lg-4 et-item interior kl">

                                    <div class="et-banner">
                                        <figure class="effect-default">
                                            <img src="admin/images/category_image/<?php echo $loc['vImageName']; ?>" alt="product image" width="488" height="518" />
                                            <figcaption>
                                                <a class="banner-link-cover" href="#">View More</a>
                                            </figcaption>
                                        </figure>
                                       
                                    </div>

                                </article>
								
								 <?php } ?>
								
								  <?php
                                        $item = $db->selectQuery('select s.iSubCategoryID,s.vCoverImage,s.iCategoryMasterID,c.vCategoryName from  sub_category_master s LEFT JOIN category_master c on s.iCategoryMasterID = c.iCategoryMasterID where c.vCategoryName ="Daily Handy Service"');
                                        if($item ){
                                        foreach($item as $loc){ ?>
                                <article class="col-md-6 col-lg-4 et-item interior dhs">
                                    <div class="et-banner">
                                        <figure class="effect-default">
                                            <img src="admin/images/subcategory/cover_image/<?php echo $loc['vCoverImage']; ?>" alt="product image" width="1024" height="768" />
                                            <figcaption>
                                                <a class="banner-link-cover" href="#">View More</a>
                                            </figcaption>
                                        </figure>
                                       
                                    </div>
                                </article>
								 <?php }} ?>
								 
								 
								  <!-- <?php
                                        $item = $db->selectQuery('select iCategoryMasterID,vCategoryName,vImageName  from category_master where vCategoryName ="Poll"');
                                        if($item ){
                                        foreach($item as $loc){ ?>
                                <article class="col-md-6 col-lg-4 et-item interior pl">
                                    <div class="et-banner">
                                        <figure class="effect-default">
                                            <img src="admin/images/subcategory/cover_image/<?php echo $loc['vImageName']; ?>" alt="product image" width="540" height="404" />
                                            <figcaption>
                                                <a class="banner-link-cover" href="#">View More</a>
                                            </figcaption>
                                        </figure>
                                       
                                    </div>
                                </article>
								 <?php } } ?>-->
								
								
								 <?php
                                        $item = $db->selectQuery('select s.iSubCategoryID,s.vCoverImage,s.iCategoryMasterID,c.vCategoryName from  sub_category_master s LEFT JOIN category_master c on s.iCategoryMasterID = c.iCategoryMasterID where c.vCategoryName ="Festival"');
                                        if($item ){
                                        foreach($item as $loc){ ?>
                                <article class="col-md-6 col-lg-4 et-item interior fst">
                                    <div class="et-banner">
                                        <figure class="effect-default">
                                            <img src="admin/images/subcategory/cover_image/<?php echo $loc['vCoverImage']; ?>" alt="product image" width="540" height="540" />
                                            <figcaption>
                                                <a class="banner-link-cover" href="#">View More</a>
                                            </figcaption>
                                        </figure>
                                       
                                    </div>
                                </article>
								 <?php }} ?>
								 
								 
								  <?php
                                        $item = $db->selectQuery('select i.vImageName from category_image i LEFT JOIN category_master m on m.iCategoryMasterID = i.iCategoryMasterID where m.vCategoryName ="Kalol Aaj Kaal"');
                                        if($item ){
                                        foreach($item as $loc){ ?>
                                <article class="col-md-6 col-lg-4 et-item interior kak">
                                    <div class="et-banner">
                                        <figure class="effect-default">
                                            <img src="admin/images/category_Image/<?php echo $loc['vImageName']; ?>" alt="product image" width="540" height="540" />
                                            <figcaption>
                                                <a class="banner-link-cover" href="#">View More</a>
                                            </figcaption>
                                        </figure>

                                    </div>
                                </article>
                               <?php }} ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>