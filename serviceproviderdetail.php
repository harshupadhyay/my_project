<?php 
include 'Config/DB.php';
$db = new DB();
$iSubCategoryID = base64_decode($_GET['id']);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- favicon -->
    <link rel="shortcut icon" href="images/logo.png">
    <!-- Site Title -->
    <title>Vignayapan </title>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">
    <!-- Fonts for icons -->
    <link href="css/fontawesome-all.min.css" rel="stylesheet">
    <!-- bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Plugins CSS -->
    <link href="css/plugins/owl.carousel.min.css" rel="stylesheet">
    <link href="css/plugins/magnific-popup.css" rel="stylesheet">
    <link href="css/plugins/aos.css" rel="stylesheet">
    <link href="css/plugins/spacing-and-height.css" rel="stylesheet">
    <!-- Style CSS -->
	<link rel="stylesheet" href="http://processpeak.com/msi/brightstar/zor/bscwelcome/fancybox/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="fancybox/source/helpers/jquery.fancybox-buttons.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="fancybox/source/helpers/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/theme-modules.css" rel="stylesheet">
	<meta content="text/html; charset=iso-8859-2" http-equiv="Content-Type">
	 <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/jssor.slider-27.5.0.min.js" type="text/javascript"></script>
<!------ Include the above in your HEAD tag ---------->

 <style>
 
        /*jssor slider loading skin spin css*/
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from { transform: rotate(0deg); }
            to { transform: rotate(360deg); }
        }

        /*jssor slider bullet skin 053 css*/
        .jssorb053 .i {position:absolute;cursor:pointer;}
        .jssorb053 .i .b {fill:#fff;fill-opacity:0.5;}
        .jssorb053 .i:hover .b {fill-opacity:.7;}
        .jssorb053 .iav .b {fill-opacity: 1;}
        .jssorb053 .i.idn {opacity:.3;}

        /*jssor slider arrow skin 093 css*/
        .jssora093 {display:block;position:absolute;cursor:pointer;}
        .jssora093 .c {fill:none;stroke:#fff;stroke-width:400;stroke-miterlimit:10;}
        .jssora093 .a {fill:none;stroke:#fff;stroke-width:400;stroke-miterlimit:10;}
        .jssora093:hover {opacity:.8;}
        .jssora093.jssora093dn {opacity:.6;}
        .jssora093.jssora093ds {opacity:.3;pointer-events:none;}
    </style>
</head>

<body>




    <div id="main-content">
        <!-- Header -->
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-light sidebar-nav align-items-start bg-color-gray py-40px py-md-80 px-40px px-md-35">
            <div class="container-fluid">
                <a class="navbar-brand" href="index.php">
                    <img class="etcodes-normal-logo" src="images/logo1.png" width="190" height="115" alt="Logo">
                    <img class="etcodes-mobile-logo" src="images/logo1.png" width="190" height="115" alt="Logo">

                </a>
                <button class="navbar-toggler hamburger-menu-btn" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span>toggle menu</span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                      <li class="nav-item">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
						<li class="nav-item">
                            <a class="nav-link" href="page-about-us.html">About Us</a>
                        </li>
                       <!--   <li class="nav-item">
                            <a class="nav-link" href="shop-right-sidebar.html">Shop</a>
                        </li>  -->
                       
                    <li class="nav-item">
                        <a class="nav-link" href="page-contact.html">Contact Us</a>
                    </li>
                    </ul>
                </div>
                <div class="bar-module">
                    <!-- <div class="mb-15px">
                        <a href="https://www.energeticthemes.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="a5edc0c9c9cae5d0c8c1c48bc6cac8">[email&#160;protected]</a>
                        <br> + 08455-3354-202
                    </div> -->
                    <ul class="list-inline m-0">
                        <li class="list-inline-item">
                           <a class="navbar-brand" href="https://play.google.com/store/apps/details?id=com.vnerds.vignyapan&hl=en">
                    <img class="etcodes-normal-logo" src="images/ico.png" width="190" height="115" alt="Logo">
                    <img class="etcodes-mobile-logo" src="images/ico.png" width="190" height="115" alt="Logo">

                <h6><b> ANDROID APPLICATION  </h6></a>
                        </li>
                        <!--<li class="list-inline-item">
                            <a href="https://instagram.com/energeticthemes">
                                <i class="fab fa-instagram" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://behance.com/energeticthemes">
                                <i class="fab fa-behance" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://twitter.com/energeticthemes">
                                <i class="fab fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>-->
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Header -->
        <!-- main -->
        <div class="container-for-sidebar-menu page-container scene-main scene-main--fade_In mt-60px ">
		<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
           	<?php
                 $item = $db->selectQuery('select vDetailsImage from advertise_master where eType="2"');?>
            
			<?php if($item ){ 
				foreach($item as $loc){ ?>
                <div>
					<img data-u="image" src="../vignyapan_dev/admin/<?php echo $loc['vDetailsImage']; ?>"/>
				</div>
			<?php }} ?>
            
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb053" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <path class="b" d="M11400,13800H4600c-1320,0-2400-1080-2400-2400V4600c0-1320,1080-2400,2400-2400h6800 c1320,0,2400,1080,2400,2400v6800C13800,12720,12720,13800,11400,13800z"></path>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora093" style="width:50px;height:50px;top:0px;left:30px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                <polyline class="a" points="7777.8,6080 5857.8,8000 7777.8,9920 "></polyline>
                <line class="a" x1="10142.2" y1="8000" x2="5857.8" y2="8000"></line>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora093" style="width:50px;height:50px;top:0px;right:30px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                <polyline class="a" points="8222.2,6080 10142.2,8000 8222.2,9920 "></polyline>
                <line class="a" x1="5857.8" y1="8000" x2="10142.2" y2="8000"></line>
            </svg>
        </div>
    </div><br><br>
    <!-- #endregion Jssor Slider End -->
            <!-- Work  -->
            <div class="bg-img-7">
                <div class="container">
                    <div class="row align-items-center mb-30px">
                        <div class="col-md-8">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                           						   
                            <div class="row et-items masonry_layout mb-20px">
							
								<?php
									$item = $db->selectQuery("select vName,vAddress,vProfilePic from service_provider where iSubCategoryID = $iSubCategoryID");
                                    
									if($item){ 
									foreach($item as $loc){ ?>
        								<article class="col-md-6 col-lg-4 et-item">
                                            <div class="et-banner">
                                                <figure class="effect-default" style="text-align: center;">
                                                    <a href="serviceproviderdetail.php?id=<?php echo base64_encode($loc['iSubCategoryID']) ; ?>" style="float: left;">  
                                                        <img src="admin/images/category/<?php echo $loc['vProfilePic']; ?>" alt="product image" width="50" height="50" style="margin-left: 5px;margin-top: 10px;"/>
                                                    </a>
                                                    <h5><?php echo $loc['vName']; ?></h5>  
        										    <span><?php echo $loc['vAddress']; ?></span>  
                                                </figure>
                                                
                                            </div>
                                        </article>							
								
                                    <?php }} ?>
									
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		     <div class="footer-container-for-sidebar-menu">
            <footer class="web-footer footer footer-top-border py-50px">
                <div class="container">
                    <div class="row large-gutters">
                        <div class="col-md-6">
                            <div class="footer-widget">
                                <p class="m-0">© 2018 Vignyapan</p>
                            </div>
                        </div> 
                        <div class="col-md-6">
                            <div class="footer-widget">
                                <ul class="list-inline m-0 text-right footer-nav">
                                    <li class="list-inline-item">
                                        <a href="index.php">
                                            Home
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="page-about-us.html">
                                            About us
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="page-contact.html">
                                            Contact
                                        </a>
                                    </li>
                                  <!--   <li class="list-inline-item">
                                        <a href="https://twitter.com/energeticthemes">
                                            Store location
                                        </a>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div> </div>
        <!-- End main 
        <div class="footer-container-for-sidebar-menu">
            <footer class="web-footer footer footer-top-border py-50px">
                <div class="container">
                    <div class="row large-gutters">
                        <div class="col-md-6">
                            <div class="footer-widget">
                                <p class="m-0">© 2018 Vignyapan</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="footer-widget">
                                <ul class="list-inline m-0 text-right footer-nav">
                                    <!--<li class="list-inline-item">
                                        <a href="https://www.facebook.com/energeticthemes">
                                            Home
                                        </a>
                                    </li>
                                </ul>
                                    <li class="list-inline-item">
                                        <a href="https://instagram.com/energeticthemes">
                                            About us
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="https://behance.com/energeticthemes">
                                            Contact
                                        </a>
                                    </li>
                                   <!-- <li class="list-inline-item">
                                        <a href="https://twitter.com/energeticthemes">
                                            Store location
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!-- ================================================== -->
    <!-- Placed js files at the end of the document so the pages load faster -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/plugins/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="js/plugins/aos.js"></script>
    <script type="text/javascript" src="js/plugins/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.countTo.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.easing.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/plugins/onepage.min.js"></script>
    <script type="text/javascript" src="js/plugins/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/plugins/instafeed.min.js"></script>
    <script type="text/javascript" src="js/plugins/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="js/plugins/contact-us.min.js"></script>
    <script type="text/javascript" src="js/plugins/twitterFetcher_min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAE_JprYsi2sHzUcl8u1DbcUgQnDveJWs4"></script>
	<!-- Fancybox code -->
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript" src="http://processpeak.com/msi/brightstar/zor/bscwelcome/fancybox/jquery-1.4.3.min.js"></script>
<script type="text/javascript" src="http://processpeak.com/msi/brightstar/zor/bscwelcome/fancybox/fancybox/jquery.fancybox-1.3.4.js"></script>
<script type="text/javascript" src="http://processpeak.com/msi/brightstar/zor/bscwelcome/fancybox/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="http://processpeak.com/msi/brightstar/zor/bscwelcome/fancybox/fancybox/jquery.easing-1.3.pack.js"></script> -->

<script type="text/javascript" src="fancybox/lib/jquery.mousewheel.pack.js"></script>
<script type="text/javascript" src="fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
<script type="text/javascript" src="fancybox/source/helpers/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>

    <script type="text/javascript" src="js/main.js"></script>
	   <script type="text/javascript">
        jQuery(document).ready(function ($) {

            var jssor_1_SlideshowTransitions = [
              {$Duration:500,$Delay:12,$Cols:10,$Rows:5,$Opacity:2,$Clip:15,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:2049,$Easing:$Jease$.$OutQuad},
              {$Duration:500,$Delay:40,$Cols:10,$Rows:5,$Opacity:2,$Clip:15,$SlideOut:true,$Easing:$Jease$.$OutQuad},
              {$Duration:1000,x:-0.2,$Delay:20,$Cols:16,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Assembly:260,$Easing:{$Left:$Jease$.$InOutExpo,$Opacity:$Jease$.$InOutQuad},$Opacity:2,$Outside:true,$Round:{$Top:0.5}},
              {$Duration:1600,y:-1,$Delay:40,$Cols:24,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Easing:$Jease$.$OutJump,$Round:{$Top:1.5}},
              {$Duration:1200,x:0.2,y:-0.1,$Delay:16,$Cols:10,$Rows:5,$Opacity:2,$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:260,$Easing:{$Left:$Jease$.$InWave,$Top:$Jease$.$InWave,$Clip:$Jease$.$OutQuad},$Round:{$Left:1.3,$Top:2.5}},
              {$Duration:1500,x:0.3,y:-0.3,$Delay:20,$Cols:10,$Rows:5,$Opacity:2,$Clip:15,$During:{$Left:[0.2,0.8],$Top:[0.2,0.8]},$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:260,$Easing:{$Left:$Jease$.$InJump,$Top:$Jease$.$InJump,$Clip:$Jease$.$OutQuad},$Round:{$Left:0.8,$Top:2.5}},
              {$Duration:1500,x:0.3,y:-0.3,$Delay:20,$Cols:10,$Rows:5,$Opacity:2,$Clip:15,$During:{$Left:[0.1,0.9],$Top:[0.1,0.9]},$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:260,$Easing:{$Left:$Jease$.$InJump,$Top:$Jease$.$InJump,$Clip:$Jease$.$OutQuad},$Round:{$Left:0.8,$Top:2.5}}
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
			
			 $(".fancybox").fancybox({
					scrolling : 'no',
			 });
			
        });
    </script>
</body>
</html>