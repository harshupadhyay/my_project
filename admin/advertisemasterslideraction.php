<?php

include 'Config/DB.php';

$db = new DB();

$tblName = 'advertise_master';

if(isset($_POST['action_type']) && !empty($_POST['action_type'])){

    if($_POST['action_type'] == 'add'){
		if(!empty($_FILES['vCoverImage']) and !empty($_FILES['vDetailsImage'])){
		
				$vTitle = $_POST['vTitle'];
				$iOrder = $_POST['iOrder'];
				$isStatus = $_POST['isStatus'];
				
				$vCoverImage = $_FILES['vCoverImage']['name'];
				$vDetailsImage = $_FILES['vDetailsImage']['name'];
				
				$cover_ext = pathinfo($vCoverImage, PATHINFO_EXTENSION);
				$details_ext = pathinfo($vDetailsImage, PATHINFO_EXTENSION);
				
				$cover_image_name = time().'_slider_advertise_cover.'.$cover_ext;
				$details_image_name = time().'_slider_advertise_details.'.$details_ext;
				
				$cover_folder = "images/advertise/slider/cover_images/".$cover_image_name;
				$details_folder = "images/advertise/slider/details_images/".$details_image_name;
				
				move_uploaded_file($_FILES["vCoverImage"]["tmp_name"], "$cover_folder");
				move_uploaded_file($_FILES["vDetailsImage"]["tmp_name"], "$details_folder");
				
				$userData = array(
					'vTitle' => $vTitle,
					'iOrder' => $iOrder,
					'isStatus' => $isStatus,
					'vCoverImage' => $cover_folder,
					'vDetailsImage' => $details_folder,
					'eType' => "2", //2 FOR SLIDER ADVERTISEMENTS
				);

            $insert = $db->insert($tblName,$userData);
			echo $insert?"ok":"err";
			
			

	

		}else{
			echo "err";
		}
	
    }elseif($_POST['action_type'] == 'edit'){

        	if(!empty($_FILES['vCoverImage']) and !empty($_FILES['vDetailsImage'])){
		
				$vTitle = $_POST['vTitle'];
				$iOrder = $_POST['iOrder'];
				$isStatus = $_POST['isStatus'];
				
				$userData = array(
					'vTitle' => $vTitle,
					'iOrder' => $iOrder,
					'isStatus' => $isStatus,
	
				);


            $condition = array('iAdvertiseID' => $_POST['iAdvertiseID']);

            $update = $db->update($tblName,$userData,$condition);
			
			  if($_FILES["vCoverImage"]["error"]==0 and $_FILES["vDetailsImage"]["error"]==0)
			  {
				$vCoverImage = $_FILES['vCoverImage']['name'];
				$vDetailsImage = $_FILES['vDetailsImage']['name'];
				
				$cover_ext = pathinfo($vCoverImage, PATHINFO_EXTENSION);
				$details_ext = pathinfo($vDetailsImage, PATHINFO_EXTENSION);
				
				$cover_image_name = time().'_slider_advertise_cover.'.$cover_ext;
				$details_image_name = time().'_slider_advertise_details.'.$details_ext;
				
				$cover_folder = "images/advertise/slider/cover_images/".$cover_image_name;
				$details_folder = "images/advertise/slider/details_images/".$details_image_name;
				
				move_uploaded_file($_FILES["vCoverImage"]["tmp_name"], "$cover_folder");
				move_uploaded_file($_FILES["vDetailsImage"]["tmp_name"], "$details_folder");
				
				$userData = array(
					'vCoverImage' => $cover_folder,
					'vDetailsImage' => $details_folder,
					'eType' => "2", //2 FOR SLIDER ADVERTISEMENTS
				);


            $condition = array('iAdvertiseID' => $_POST['iAdvertiseID']);

            $update = $db->update($tblName,$userData,$condition);

		}
		
		echo 'ok';
		}
    }elseif($_POST['action_type'] == 'delete'){

        if(!empty($_POST['iAdvertiseID'])){
			$id =  $_POST['iAdvertiseID'];
			
			$images = $db->selectQuery("select vCoverImage,vDetailsImage from advertise_master where iAdvertiseID =$id");
		
			$un = unlink($images[0]['vCoverImage'])?true:false;
			$un = unlink($images[0]['vDetailsImage'])?true:false;
			if($un){
				$condition = array('iAdvertiseID' => $_POST['iAdvertiseID']);
				$delete = $db->delete($tblName,$condition);
				echo $delete?'ok':'err';
			}
        }

	}elseif($_POST['action_type'] == 'data'){

        $conditions['where'] = array('iAdvertiseID'=>$_POST['iAdvertiseID']);

        $conditions['return_type'] = 'single';

        $user = $db->getRows($tblName,$conditions);

        echo json_encode($user);

	 }

	 

	 

	 /*  

	  elseif($_POST['action_type'] == 'validateusername')

	 {

		$conditions['select'] = 'username';

		$conditions['where'] = array('username'=>$_POST['username']);

        //$conditions['return_type'] = 'single';

        $user = $db->getRows("usermaster",$conditions);

		

		echo $user?'err':'ok';

		

	 }

	 

	 elseif($_POST['action_type'] == 'validateemail')

	 {

		$conditions['select'] = 'email';

		$conditions['where'] = array('email'=>$_POST['email']);

        //$conditions['return_type'] = 'single';

        $user = $db->getRows("usermaster",$conditions);

		

		echo $user?'err':'ok';

		

	 }

	 

	 

	  */

	 

    }

    exit;

 

?>







