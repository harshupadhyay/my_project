<?php include'header.php';

if($_SESSION['user_role'] == 'admin')

	{}

	else

	{

	header('Location: index.php');

	exit(0);	

	}

include 'Config/DB.php';

$db = new DB();

?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/dropzone.css" type="text/css" rel="stylesheet" />

<script src="dropzone.min.js"></script>
<script>

//for validation

function validation(method)

{	   


	var vTitleName = false;

	var vImageName = false;
	
	var tDescription = false;


	if(!$('#vTitleName').val())

	{

			$('#vTitleName').parent().addClass('has-error');

			$('#vTitleName_help').show();

	}			

	else{$('#vTitleName').parent().removeClass('has-error');$('#vTitleName_help').hide();vTitleName=true;}

	

	if(!$('#vImageName').val())

	{

			$('#vImageName').parent().addClass('has-error');

			$('#vImageName_help').show();

	}			

	else{$('#vImageName').parent().removeClass('has-error');$('#vImageName_help').hide();vImageName=true;}
	
	if(!$('#tDescription').val())

	{

			$('#tDescription').parent().addClass('has-error');

			$('#tDescription_help').show();

	}			

	else{$('#tDescription').parent().removeClass('has-error');$('#tDescription_help').hide();tDescription=true;}

	if(vTitleName && vImageName && tDescription ) 

	{

			vaction(method);

	} 

	else{alert("There is some problem occured. Please try again.")}

		

}

//for table data 

function editEntry(id)

{   



	$('.help-block').hide();

	$('.has-error').removeClass('has-error');

	

	$.ajax({

	type: 'POST',

	dataType:'JSON',

	url: 'historymasteraction.php',

	data: 'action_type=data&iHistoryID='+id,

	success:function(data){

	$('#iHistoryID').val(data.iHistoryID);	

	$('#vTitleName').val(data.vTitleName);

	$('#tDescription').val(data.tDescription);
	
	$('#isStatus').val(data.isStatus);	

	/* if(data.isSubCategory == 'Yes'){
					$("#realtime").attr('checked', true);
				}else{
					$("#realtime").attr('checked', false);
				} */
		
				if(data.vImageName){
					var src = 'images/history/'+data.vImageName;
					$('#blah').attr('src', src);
				}else{
					var src = 'images/image_upload.jpg';
					$('#blah').attr('src', src);
				}

	}

});

	}
//for rack master

</script>
<script>

function vaction(type,id){

    id = (typeof id == "undefined")?'':id;

    var userData = '';

 

 if (type == 'add') 

 {

     var userData = new FormData($(".Category_entry")[0]);

	userData.append('action_type', type);

    $.ajax({

        url: 'historymasteraction.php',

        type: 'POST',

        data: userData,

        async: false,

        success: function (data) {

			if(data == 'ok'){

			$('#example').DataTable().ajax.reload(null, false);

			showSuccesMessage();

			resetdata();

			$('#blah').attr('src', 'images/image_upload.jpg');

           }

		},

        cache: false,

        contentType: false,

        processData: false,

    });



    return false;

    }

	else if (type == 'edit'){

         var userData = new FormData($(".Category_entry")[0]);

		userData.append('action_type', type);

		

		$.ajax({

			url: 'historymasteraction.php',

			type: 'POST',

			data: userData,

			async: false,

			success: function (data) {

			if(data == 'ok'){

				$('#example').DataTable().ajax.reload(null, false);

				showEditSuccesMessage();

				resetdata();

				$(btnUpdate).hide();

				$(btnSave).show();

			}

        },
		cache: false,

        contentType: false,

        processData: false,

    });
	  return false;

     }


	else if (type == 'delete'){

     

		userData ='action_type=delete&iHistoryID='+id; 

		$.ajax({	

			url: 'historymasteraction.php',

			type: 'POST',

			data: userData,

			async: false,

			success: function (data) {

				

				$('#example').DataTable().ajax.reload(null, false);

        },

       

    });

				

    }

    

    }

function resetdata(){

	

	  $('.Category_entry')[0].reset();

	  $('.help-block').hide();

	  $('.has-error').removeClass('has-error');

}



function prevent(e) {

   e.preventDefault();

}

</script>

<?php include'navbar.php' ?>

<?php include'sidebar.php' ?>

<section class="content">

<div class="container-fluid">

           

        </div>

				<div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">

                        <div class="header">

                            <h2><i class="fa fa-gear" aria-hidden="true"></i>  History Details</h2>

                         

                        </div>

                        <div class="body">
						<form id="form_validation" enctype="multipart/form-data" method="POST" class="Category_entry">
						<div class="row">

									<div class="col-sm-6">

									<label class="form-label">Title Name</label>

                                        <div class="form-line">

                                            <input type="text" class="form-control" placeholder="Enter Title Name" name="vTitleName" id="vTitleName" />

                                        </div>

										<span class="help-block" id="vTitleName_help" style="display:none;font-size:12px;color:red;">Please Enter Category Name.</span>

                                    </div> 

									<div class="row">

									<div class="col-sm-4">

									<div class="form-line">

									<div class="form-line">

									<i class="fa fa-image"></i> Upload Image

									<input type="file" name="vImageName" id="vImageName" accept=".jpg, .jpeg, .png" tabindex="0" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">

							
									<span class="help-block" id="vImageName_help" style="display:none;font-size:12px;color:red;">Please Enter Upload Image.</span>
									</div>

									<p class="help-block">Max size 1 Mb</p>

								  </div>

								  </div>

									

				<div class="row">

				<label class="margin-left">&nbsp;</label>

				<img id="blah" src="images/image_upload.jpg" class="img-thumbnail" width="180px" height="150px" />

				</div>	
								
								</div>	
								</div>
									<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6">
									<label class="form-label">Description</label>
                                    <div class="form-line">
                                     <textarea class="form-control" rows="4" id="tDescription" name="tDescription" placeholder="Enter Description..." tabindex="0"></textarea>
									<span class="help-block" id="tDescription_help" style="display:none;">Please Enter Poll Question.</span>
                                    </div>
                                    </div> 
									
									<div class="col-md-6 col-sm-6 col-xs-6">
									<label class="form-label">Status</label><span style="color:red"> * </span>
                                      
                                            <select class="form-control input-md" name="isStatus" id="isStatus">
										<option>active</option>
										<option>deactive</option>
                  
										</select>
										<span class="help-block" id="isStatus_help" style="display:none;">Please Select Status.</span>
                                      
                                    </div>
									</div>
							
								
								 <input type="hidden" id="iHistoryID" name="iHistoryID"/>

								 <div class="row clearfix js-sweetalert">

                                <button id="btnSave" class="btn bg-blue btn-sm waves-effect" type="button" style="margin-left:20px;" onclick="validation('add');"><i class="material-icons">save</i><span class="icon-name">Save</span></button>

                                <button type="button" id="btnUpdate" class="btn bg-orange btn-sm waves-effect"  style="display: none;margin-left:20px;" onclick="vaction('edit');">

								<i class="material-icons">edit</i><span class="icon-name">Update</span></button>

								
                                 <button type="button" id="btnReset" class="btn bg-red btn-sm waves-effect" style="margin-left:20px;"  onclick="resetdata();$('#btnUpdate').hide();$('#btnSave').show();"><i class="material-icons">repeat</i><span class="icon-name">Reset</span></button>

							

                        </div>
						</div>
						</div>
                    </div>
                </div>
           
			 <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">

                        <div class="header">

                            <h2>

                               <i class="fa fa-table" aria-hidden="true"></i>  History List

                            </h2> 
							</div>

                        <div class="body">

                            <div class="table-responsive">

                                <table id="example" class="table table-bordered table-striped table-hover dataTable">

                                    <thead>

                                        <tr>

                                            <th>&nbsp;</th>

                                            <th>SR NO</th>

                                            <th>Title Name</th> 

											<th>Image Name</th>

											<th>Description</th>
											
											<th>Status</th>
											
                                            <th>Date</th>  

											<th>Action</th>

                                        </tr>

                                    </thead>
                               </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div> 

    </section>


<?php include'footer.php' ?>
<script>

 	  function toChangeDateFormat(dateStr)

{

    var parts = dateStr.split("-");

    //return new Date(parts[2], parts[1], parts[0]);

	var dt=  parts[2]+"/"+parts[1]+"/"+parts[0];

	return dt;

}



//ADD FOCUS ON SIDEBAR

	$("#sidebar_ul").find(".active").removeClass("active");

	$("#sidebar_history_master").addClass("active").focus();

	$( document ).ready(function() {



	var otable = $('#example').DataTable( {

	dom: 'Bfrtip',

	buttons: [

            'copy', 'csv', 'excel', 'pdf', 'print'

        ],

"ajax": "historymasterdata.php",

"bPaginate":true,

"bProcessing": true,





"dataSrc":"data",

 "columns": [

{ mData: 'iHistoryID' },

{ mData: 'count' },

{ mData: 'vTitleName' } ,

{ mData: 'vImageName' } ,

{ mData: 'tDescription' } ,

{ mData: 'isStatus' } ,

{ mData: 'created'  },

{ mData: 'action' }

],

"columnDefs": [



{ targets: [0],

        "mData": "iHistoryID",

		"sClass": "center",

		"width": "1%",

		"visible":false

 },

{targets: [3],

        "mData": "vImageName",

        "sClass": "center",

        "mRender": function (data, type, row) {
		if(row.vImageName!=null){
			return '<img src="images/history/'+row.vImageName+'" width="42" height="42">'
		}else{
			return '';
		}
		

        }

    },   
{targets: [5],
				"mData": "isStatus",
					"sClass": "center",
				"mRender": function (data, type, row) {
				if(row.isStatus == "active"){$color = '<b><span style="color:#42ce42">'}else{$color = '<b><span style="color:red">'}
					return $color+row.isStatus+'</span></b>'
					}
				},
	
{targets: [7],

        "mData": "action",

        "sClass": "center",

        "mRender": function (data, type, row) {

		return '<button type="button" class="btn bg-blue waves-effect pull-left btn-sm" onclick="$(\'#btnSave\').hide();$(\'#btnUpdate\').show();editEntry('+row.iHistoryID +')"><i class="material-icons">edit</i></button>' +

		'<button class="btn bg-red waves-effect pull-left btn-sm" onclick="showConfirmMessage('+row.iHistoryID +');"><i class="material-icons">delete_forever</i></button>'

        }

    },

]		



});

});



</script>

<script>



function showStopMessage() {

    swal({

        title: "Stop!",

        text: "Dont cut this log.!",

        type: "error",



        closeOnConfirm: false

    });

}



function showSuccesMessage() {

    swal({

        title: "Success!",

        text: "history Details has been Added succesfully.!",

        type: "success",



        closeOnConfirm: false

    });

}



function showEditSuccesMessage() {

    swal({

        title: "Success!",

        text: "history Details has been Updated succesfully.!",

        type: "success",



        closeOnConfirm: false

    });

}



function showWarningMessage() {

    swal({

        title: "oops!",

        text: "Something went wrong, please check the values and try again.!",

        type: "warning",



        closeOnConfirm: false

    });

}



function showConfirmMessage(iHistoryID) {

    swal({

        title: "Are you sure?",

        text: "You will not be able to recover this data!",

        type: "warning",

        showCancelButton: true,

        confirmButtonColor: "#DD6B55",

        confirmButtonText: "Yes, delete it!",

        closeOnConfirm: false

    }, function () {

		vaction('delete',iHistoryID);

        swal("Deleted!", "history data has been deleted successfully.", "success");

    });

}



</script>

<!--  <script type="text/javascript">

	$(document).ready(function() {

		$("#loading").hide();

		var options = {

			beforeSubmit:  showRequest,

			success:       showResponse,

			url:       'subcategorymasteraction.php',  // your upload script

			dataType:  'json'

		};

		$('#Form1').submit(function() {

			$('#message').html('');

			$(this).ajaxSubmit(options);

			return false;

		});

	}); 

	function showRequest(formData, jqForm, options) { 

		var fileToUploadValue = $('#fileToUpload').fieldValue();

		if (!fileToUploadValue[0]) { 

			$('#message').html('You need to select a file!'); 

			return false; 

		}

		$("#loading").show();

		return true; 

	} 

	function showResponse(data, statusText, xhr, $form)  {

		$("#loading").hide();

		if (statusText == 'success') {

			var msg = data.error.replace(/##/g, "<br />");

			if (data.img != '') {

				$('#result').html('<br /><img src="files/photo/' + data.img + '" />');

				$('#message').html('<br />' + msg + '<a href="index.html">Click here</a> to upload another file.'); 

				$('#formcont').html('');

			} else {

				$('#message').html(msg); 

			}

		} else {

			$('#message').html('Unknown error!'); 

		}

	} 

	</script> */

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="http://malsup.github.com/min/jquery.form.min.js"></script>

</body>

</html>