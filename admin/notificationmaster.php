<?php include'header.php';

if($_SESSION['user_role'] != 'admin'  && $_SESSION['user_role'] != 'supervisor')

	{

	header('Location: index.php');

	exit(0);	

}

?>

<script>

//for validation

function validation(method)

{	   





	var vNotificationName = false;

	var vImageName = false;

	

	

	//$length=$('#form').parent().find('length');



	if(!$('#vNotificationName').val())

	{

			$('#vNotificationName').parent().addClass('has-error');

			$('#vNotificationName_help').show();

	}			

	else{$('#vNotificationName').parent().removeClass('has-error');$('#vNotificationName_help').hide();vNotificationName=true;}

		

		if(!$('#vImageName').val())

	{

			$('#vImageName').parent().addClass('has-error');

			$('#vImageName_help').show();

	}			

	else{$('#vImageName').parent().removeClass('has-error');$('#vImageName_help').hide();vImageName=true;}

	

	

	

	

	

	if(vNotificationName && vImageName ) 

	{

			vaction(method);

	} 

	else{alert("There is some problem occured. Please try again.")}

		

}

//for table data 

	function editEntry(id){   
		$('.help-block').hide();

		$('.has-error').removeClass('has-error');

		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url: 'notificationaction.php',
			data: 'action_type=data&iNotificationID='+id,
			success:function(data){

				$('#iNotificationID').val(data.iNotificationID);	

				$('#vNotificationName').val(data.vNotificationName);
				
				$('#eNotificationStatus').val(data.eNotificationStatus);	

				// if(data.isSubNotification == 'Yes'){
					// $("#realtime").attr('checked', true);
				// }else{
					// $("#realtime").attr('checked', false);
				// }
				var src = 'images/notification/'+data.vImageName;
				$('#blah').attr('src', src);
			}
		});
	}

	



	

//for rack master

</script>

<script>

function vaction(type,id){

    id = (typeof id == "undefined")?'':id;

    var userData = '';

 

 if (type == 'add') 

 {

     var userData = new FormData($(".Notification_entry")[0]);

	userData.append('action_type', type);

    $.ajax({

        url: 'notificationaction.php',

        type: 'POST',

        data: userData,

        async: false,

        success: function (data) {

			if(data == 'ok'){

			$('#example').DataTable().ajax.reload(null, false);

			showSuccesMessage();

			resetdata();

			$('#blah').attr('src', 'images/image_upload.jpg');

           }

		},

        cache: false,

        contentType: false,

        processData: false,

    });



    return false;



	

    }

	else if (type == 'edit'){

      
     var userData = new FormData($(".Notification_entry")[0]);

	userData.append('action_type', type);

		$.ajax({

			url: 'notificationaction.php',

			type: 'POST',

			data: userData,

			async: false,

			success: function (data) {

			if(data == 'ok'){

				$('#example').DataTable().ajax.reload(null, false);

				showEditSuccesMessage();

				var src = 'images/image_upload.jpg';
				$('#blah').attr('src', src);
				
				resetdata();

				$(btnUpdate).hide();

				$(btnSave).show();

			}

        },
		
		
        cache: false,

        contentType: false,

        processData: false,

    });

	  return false;
	
     }

	 

	 

	else if (type == 'delete'){

     

		userData ='action_type=delete&iNotificationID='+id; 

		$.ajax({	

			url: 'notificationaction.php',

			type: 'POST',

			data: userData,

			async: false,

			success: function (data) {

				

				$('#example').DataTable().ajax.reload(null, false);

        },

       

    });

				

    }

    

    }

function resetdata(){

	

	  $('.Notification_entry')[0].reset();

	  $('.help-block').hide();

	  $('.has-error').removeClass('has-error');

	var src = 'images/image_upload.jpg';
				$('#blah').attr('src', src);
}



function prevent(e) {

   e.preventDefault();

}

</script>



<?php include'navbar.php' ?>

    <?php include'sidebar.php' ?>

  <section class="content">

        <div class="container-fluid">

           

        </div>

		 

		 <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">

                        <div class="header">

                            <h2><i class="fa fa-gear" aria-hidden="true"></i>  Notification Details</h2>

                         

                        </div>

                        <div class="body">

                            <form id="form_validation" method="POST" class="Notification_entry">

							<div class="row">

								<div class="col-sm-12">

									<label class="form-label">Notification Text</label>

                                        <div class="form-line">

                                            <input type="text" class="form-control" placeholder="Enter Notification Text" name="vNotificationName" id="vNotificationName" />

                                        </div>

										<span class="help-block" id="vNotificationName_help" style="display:none;font-size:12px;color:red;">Please Enter Notification Name.</span>

                                    </div>

                                   

									<div class="col-sm-4">

									<div class="form-line">

										<i class="fa fa-image"></i> Upload Image

										<input type="file" name="vImageName" id="vImageName" accept=".jpg, .jpeg, .png" tabindex="0" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">

										<span class="help-block" id="vImageName_help" style="display:none;font-size:12px;color:red;">Please Upload Image.</span>

										<!--<input type="file" name="image" id="image">-->

										</div>

										<p class="help-block">Max size 1 Mb</p>

							
									</div>

									

				<div class="col-sm-4">

				<label class="margin-left">&nbsp;</label>

				<img id="blah" src="images/image_upload.jpg" class="img-thumbnail" width="200px" height="150px" />

				</div>

				

				<!--IMAGE PREVIEW CODE-->
								
									
									
									<div class="col-md-4">
									<label class="form-label">Status</label>
                                      
                                            <select class="form-control input-md" name="eNotificationStatus" id="eNotificationStatus">
										<option>active</option>
										<option>deactive</option>
                  
										</select>
                                      
                                    </div>
									
									
								</div>

							

								<input type="hidden" id="iNotificationID" name="iNotificationID"/>

								 <div class="row clearfix js-sweetalert">

								

                                <button id="btnSave" class="btn bg-blue btn-sm waves-effect" type="button" style="margin-left:20px;" onclick="validation('add');"><i class="material-icons">save</i><span class="icon-name">Save</span></button>

								

                                <button type="button" id="btnUpdate" class="btn bg-orange btn-sm waves-effect"  style="display: none;margin-left:20px;" onclick="vaction('edit');">

								<i class="material-icons">edit</i><span class="icon-name">Update</span></button>

								

                                 <button type="button" id="btnReset" class="btn bg-red btn-sm waves-effect" style="margin-left:20px;"  onclick="resetdata();$('#btnUpdate').hide();$('#btnSave').show();"><i class="material-icons">repeat</i><span class="icon-name">Reset</span></button>

                            </form>

                        </div>

						</div>

                    </div>

                </div>

				

            </div>

			 <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">

                        <div class="header">

                            <h2>

                               <i class="fa fa-table" aria-hidden="true"></i>   Notification List

                            </h2>

                         

                        </div>

                        <div class="body">

                            <div class="table-responsive">

                                <table id="example" class="table table-bordered table-striped table-hover dataTable">

                                    <thead>

                                        <tr>

                                            <th>&nbsp;</th>

                                            <th>SR NO</th>

                                            <th>Notification Text</th>  

											<th>Image</th>
											
											<th>Status</th>

                                            <th>Date</th>  

											<th>Action</th>

                                        </tr>

                                    </thead>

                       

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>



    </section>







 <?php include'footer.php' ?>

 <script>

 	  function toChangeDateFormat(dateStr)

{

    var parts = dateStr.split("-");

    //return new Date(parts[2], parts[1], parts[0]);

	var dt=  parts[2]+"/"+parts[1]+"/"+parts[0];

	return dt;

}



//ADD FOCUS ON SIDEBAR

	$("#sidebar_ul").find(".active").removeClass("active");

	$("#sidebar_notification").addClass("active").focus();

$( document ).ready(function() {



	var otable = $('#example').DataTable( {

	dom: 'Bfrtip',

	buttons: [

            'copy', 'csv', 'excel', 'pdf', 'print'

        ],

"ajax": "notificationdata.php",

"bPaginate":true,

"bProcessing": true,





"dataSrc":"data",

 "columns": [

 { mData: 'iNotificationID' },

 { mData: 'count' },

{ mData: 'vNotificationName' } ,

{ mData: 'vImageName' } ,


{ mData: 'eNotificationStatus' } ,



{ mData: 'created'  },



{ mData: 'action' }

],

"columnDefs": [



{ targets: [0],

        "mData": "iNotificationID",

		"sClass": "center",

		"width": "1%",

		"visible":false

 },



  {targets: [3],

        "mData": "vImageName",

        "sClass": "center",

        "mRender": function (data, type, row) {

		return '<img src="images/notification/'+row.vImageName+'" width="42" height="42">'

        }

    },  

	
	
	
	{targets: [4],
				"mData": "eNotificationStatus",
					"sClass": "center",
				"mRender": function (data, type, row) {
				if(row.eNotificationStatus == "active"){$color = '<b><span style="color:#42ce42">'}else{$color = '<b><span style="color:red">'}
					return $color+row.eNotificationStatus+'</span></b>'
					}
				},


 
	

	{targets: [6],

        "mData": "action",

        "sClass": "center",

        "mRender": function (data, type, row)
	{

		var a = '<button type="button" class="btn bg-blue waves-effect pull-left btn-sm" onclick="$(\'#btnSave\').hide();$(\'#btnUpdate\').show();editEntry('+row.iNotificationID+')"><i class="material-icons">edit</i></button>' ;

		if(row.iNotificationID > 5)
		{
			
			a +='<button class="btn bg-red waves-effect pull-left btn-sm" onclick="showConfirmMessage('+row.iNotificationID+');"><i class="material-icons">delete_forever</i></button>';

		} 
		return a;
    }

    },

]		



});

});



</script>

<script>



function showStopMessage() {

    swal({

        title: "Stop!",

        text: "Dont cut this log.!",

        type: "error",



        closeOnConfirm: false

    });

}



function showSuccesMessage() {

    swal({

        title: "Success!",

        text: "Notification Details has been Added succesfully.!",

        type: "success",



        closeOnConfirm: false

    });

}



function showEditSuccesMessage() {

    swal({

        title: "Success!",

        text: "Notification Details has been Updated succesfully.!",

        type: "success",



        closeOnConfirm: false

    });

}



function showWarningMessage() {

    swal({

        title: "oops!",

        text: "Something went wrong, please check the values and try again.!",

        type: "warning",



        closeOnConfirm: false

    });

}



function showConfirmMessage(iNotificationID) {

    swal({

        title: "Are you sure?",

        text: "You will not be able to recover this data!",

        type: "warning",

        showCancelButton: true,

        confirmButtonColor: "#DD6B55",

        confirmButtonText: "Yes, delete it!",

        closeOnConfirm: false

    }, function () {

		vaction('delete',iNotificationID);

        swal("Deleted!", "Notification data has been deleted successfully.", "success");

    });

}



</script>

</body>



</html>