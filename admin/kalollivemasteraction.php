<?php



include 'Config/DB.php';



$db = new DB();



$tblName = 'category_image';
if(isset($_POST['action_type']) && !empty($_POST['action_type'])){

    if($_POST['action_type'] == 'add'){

		$count = count($_POST['iOrder']);
		for($i=0;$i<$count;$i++){
			if(!empty($_POST['iCategoryMasterID'])){
	            $userData = array(
					
					'isStatus' => $_POST['isStatus'],
					'iCategoryMasterID' => $_POST['iCategoryMasterID'],
					// 'iSubCategoryID' => $_POST['iSubCategoryID'],
					'iOrder' => $_POST['iOrder'][$i],
	            );
				
	        	$insert = $db->insert($tblName,$userData);
				if($insert){
					$image_id=$insert;
					$image_name = time().'_'.$_FILES["vImageName"]["name"][$i];
					$folder = "images/category_image/".$image_name;
					move_uploaded_file($_FILES["vImageName"]["tmp_name"][$i], "$folder");
					$imgData = array(
									'vImageName' => $image_name,
								);
					$condition = array('iCategoryImageID' => $image_id);
					$update = $db->update($tblName,$imgData,$condition);
					$msg = $update?"ok":"err";
				}
				
			}
		}
		echo "ok";

    }elseif($_POST['action_type'] == 'edit'){

        if(!empty($_POST['iCategoryImageID'])){

            $userData = array(
				'iCategoryMasterID' => $_POST['iCategoryMasterID'],
				'isStatus' => 'active',
				'iOrder' => $_POST['iOrder'][0],
            );
			$msg="";
            $condition = array('iCategoryImageID' => $_POST['iCategoryImageID']);
            $update = $db->update($tblName,$userData,$condition);
			$msg = $update?"ok":"err";
			if($_FILES["vImageName"]["error"][0]== 0){

				$image_name = time().'_'.$_FILES["vImageName"]["name"][0];



				$folder = "images/category_image/".$image_name;



				move_uploaded_file($_FILES["vImageName"]["tmp_name"][0], "$folder");

				$imgData = array(

						'vImageName' => $image_name,

					);

				$condition = array('iCategoryImageID' => $_POST['iCategoryImageID']);

				$update = $db->update($tblName,$imgData,$condition);

				$msg = $update?"ok":"err";
			}

			echo $msg;
			
		}

    }elseif($_POST['action_type'] == 'delete'){

        if(!empty($_POST['iCategoryImageID'])){

            $condition = array('iCategoryImageID' => $_POST['iCategoryImageID']);

            $delete = $db->delete($tblName,$condition);

            echo $delete?'ok':'err';

        }

	}elseif($_POST['action_type'] == 'data'){

        $conditions['where'] = array('iCategoryImageID'=>$_POST['iCategoryImageID']);

        $conditions['return_type'] = 'single';

        $user = $db->getRows($tblName,$conditions);

        echo json_encode($user);

 	}

}



    exit;



 



?>















