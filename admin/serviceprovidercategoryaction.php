<?php

include 'Config/DB.php';

$db = new DB();

$tblName = 'sub_category_master';

if(isset($_POST['action_type']) && !empty($_POST['action_type'])){

    if($_POST['action_type'] == 'add'){
		if(!empty($_POST['iCategoryNameID'])){
            $userData = array(
				'iCategoryMasterID' => $_POST['iCategoryNameID'],
				'vText' => $_POST['vText'],
				'isStatus' => $_POST['isStatus'],
            );

            $iSubCategoryID = $db->insert($tblName,$userData);
			
			if($iSubCategoryID){
				$path = $_FILES['vCoverImage']['name'];
				$ext = pathinfo($path, PATHINFO_EXTENSION);
				$image_name = time().'_.'.$ext;
				$folder = "images/subcategory/cover_image/".$image_name;
				//copy($_FILES["image1"]["name"] , "$folder".$_FILES["image1"]["name"]);
				move_uploaded_file($_FILES["vCoverImage"]["tmp_name"], "$folder");
				
				$imgData = array(
					'vCoverImage' => $image_name
				);

				$condition = array('iSubCategoryID' => $iSubCategoryID);

				$update = $db->update($tblName,$imgData,$condition);

			}else{

				echo "err";

			}


		}
	
		echo "ok";
    }elseif($_POST['action_type'] == 'edit'){
        if(!empty($_POST['iSubCategoryID'])){
            $userData = array(
				'iCategoryMasterID' => $_POST['iCategoryNameID'],
				'vText' => $_POST['vText'],
				'isStatus' => $_POST['isStatus'],
			
            );
            $condition = array('iSubCategoryID' => $_POST['iSubCategoryID']);
            $update = $db->update($tblName,$userData,$condition);
			 if($_FILES["vCoverImage"]["error"] == 0){
				$image_name = time().'_'.$_FILES["vCoverImage"]["name"];
				$folder = "images/subcategory/cover_image/".$image_name;
				move_uploaded_file($_FILES["vCoverImage"]["tmp_name"], "$folder");
				$imgData = array(
						'vCoverImage' => $image_name,
					);
				$condition = array('iSubCategoryID' => $_POST['iSubCategoryID']);
				$update = $db->update($tblName,$imgData,$condition);
			}
			echo 'ok';
		}
    }elseif($_POST['action_type'] == 'delete'){

        if(!empty($_POST['iSubCategoryID'])){

            $condition = array('iSubCategoryID' => $_POST['iSubCategoryID']);

            $delete = $db->delete($tblName,$condition);

            echo $delete?'ok':'err';

        }

	}elseif($_POST['action_type'] == 'data'){

        $conditions['where'] = array('iSubCategoryID'=>$_POST['iSubCategoryID']);

        $conditions['return_type'] = 'single';

        $user = $db->getRows($tblName,$conditions);

        echo json_encode($user);

	 }

	 

	 

	 /*  

	  elseif($_POST['action_type'] == 'validateusername')

	 {

		$conditions['select'] = 'username';

		$conditions['where'] = array('username'=>$_POST['username']);

        //$conditions['return_type'] = 'single';

        $user = $db->getRows("usermaster",$conditions);

		

		echo $user?'err':'ok';

		

	 }

	 

	 elseif($_POST['action_type'] == 'validateemail')

	 {

		$conditions['select'] = 'email';

		$conditions['where'] = array('email'=>$_POST['email']);

        //$conditions['return_type'] = 'single';

        $user = $db->getRows("usermaster",$conditions);

		

		echo $user?'err':'ok';

		

	 }

	 

	 

	  */

	 

    }

    exit;

 

?>







