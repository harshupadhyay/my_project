<?php

include 'Config/DB.php';

$db = new DB();

$tblName = 'product_ofthe_day';

if(isset($_POST['action_type']) && !empty($_POST['action_type'])){
    if($_POST['action_type'] == 'add'){
		
			$ppart= explode('/',$_POST['product_date']);
			$product_date=$ppart[2]."-".$ppart[1]."-".$ppart[0];

			$epart= explode('/',$_POST['expiry_date']);
			$expiry_date=$epart[2]."-".$epart[1]."-".$epart[0];
		
		if(!empty($_POST['vProductName'])){
            $userData = array(
				'vProductName' => $_POST['vProductName'],
				'iCount' => $_POST['iCount'],
				'product_date' => $product_date,
				'expiry_date' => $expiry_date,
				'isStatus' => $_POST['isStatus'],
				'product_start_time' => $_POST['product_start_time'],
				'product_end_time' => $_POST['product_end_time'],
				'product_expiry_time' => $_POST['product_expiry_time'],
            );

            $insert = $db->insert($tblName,$userData);
           // echo $insert?"ok":"err";
            
			if(isset($_FILES["vImageName"]["name"])){
				$image_id=$insert;
				$image_name = time().'_'.$_FILES["vImageName"]["name"];
				$folder = $_SERVER['DOCUMENT_ROOT']."/project/vignyapan/admin/images/product/".$image_name;
				move_uploaded_file($_FILES["vImageName"]["tmp_name"], "$folder");
				$imgData = array(
					'vImageName' => $image_name,
				);

				$condition = array('iProductID' => $image_id);
				$update = $db->update($tblName,$imgData,$condition);
			}
			echo $update?"ok":"err";
		}
    }elseif($_POST['action_type'] == 'edit'){
		
			$ppart= explode('/',$_POST['product_date']);
			$product_date=$ppart[2]."/".$ppart[1]."/".$ppart[0];

			$epart= explode('/',$_POST['expiry_date']);
			$expiry_date=$epart[2]."-".$epart[1]."-".$epart[0];
        if(!empty($_POST['iProductID'])){
            $userData = array(
				'vProductName' => $_POST['vProductName'],
				'product_date' => $product_date,
				'expiry_date' => $expiry_date,
				'iCount' => $_POST['iCount'],
				'isStatus' => $_POST['isStatus'],				
				'product_start_time' => $_POST['product_start_time'],
				'product_end_time' => $_POST['product_end_time'],
				'product_expiry_time' => $_POST['product_expiry_time'],
            );

            $condition = array('iProductID' => $_POST['iProductID']);
            $update = $db->update($tblName,$userData,$condition);
			
			if($_FILES["vImageName"]["error"]== 0){
				$image_name = time().'_'.$_FILES["vImageName"]["name"];
				$folder = $_SERVER['DOCUMENT_ROOT']."/project/vignyapan/admin/images/product/".$image_name;
				
				move_uploaded_file($_FILES["vImageName"]["tmp_name"], "$folder");
				$imgData = array(
						'vImageName' => $image_name,
					);
				$condition = array('iProductID' => $_POST['iProductID']);
				$update = $db->update($tblName,$imgData,$condition);
			}
			echo $update?"ok":"err";
			// echo $_SERVER['DOCUMENT_ROOT'];
		}
		
    }elseif($_POST['action_type'] == 'delete'){
        if(!empty($_POST['iProductID'])){
            $condition = array('iProductID' => $_POST['iProductID']);
            $delete = $db->delete($tblName,$condition);
            echo $delete?'ok':'err';
        }
	}elseif($_POST['action_type'] == 'data'){
        $conditions['where'] = array('iProductID'=>$_POST['iProductID']);
        $conditions['return_type'] = 'single';
        $user = $db->getRows($tblName,$conditions);
        echo json_encode($user);
 	}
}

    exit;

 

?>







