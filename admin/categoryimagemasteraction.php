<?php

include 'Config/DB.php';

$db = new DB();

$tblName = 'category_image';

if(isset($_POST['action_type']) && !empty($_POST['action_type'])){
    if($_POST['action_type'] == 'add'){
		if(!empty($_POST['iCategoryMasterID'])){
    		
		// Count total files
		$count = count($_FILES['files']['name']);
			
			
			for($i=0;$i<$count;$i++){
						
				$image_name = time().'_'.$_FILES["files"]["name"][$i];
				$folder = "images/category_image/".$image_name;

				move_uploaded_file($_FILES["files"]["tmp_name"][$i], "$folder");
					
				$userData = array(
						'iCategoryMasterID' => $_POST['iCategoryMasterID'],
						'vImageName' => $image_name,
				);

				$insert = $db->insert($tblName,$userData);
			}
			echo 'ok';
        
		}
    }
	elseif($_POST['action_type'] == 'edit'){
        if(!empty($_POST['iCategoryMasterID'])){
    		
		// Count total files
		$count = count($_FILES['files']['name']);
			
			
			for($i=0;$i<$count;$i++){
						
				$image_name = time().'_'.$_FILES["files"]["name"][$i];
				$folder = "images/category_image/".$image_name;

				move_uploaded_file($_FILES["files"]["tmp_name"][$i], "$folder");
					
				$userData = array(
						'iCategoryMasterID' => $_POST['iCategoryMasterID'],
						'vImageName' => $image_name,
				);

				$insert = $db->insert($tblName,$userData);
			}
					echo $insert?"ok":"err";
        
		}
    }
    elseif($_POST['action_type'] == 'delete'){
        if(!empty($_POST['iCategoryImageID'])){
            $condition = array('iCategoryImageID' => $_POST['iCategoryImageID']);
            $delete = $db->delete($tblName,$condition);
            echo $delete?'ok':'err';
        }
	}elseif($_POST['action_type'] == 'data'){
        $conditions['where'] = array('iCategoryImageID'=>$_POST['iCategoryImageID']);
        $conditions['return_type'] = 'single';
        $user = $db->getRows($tblName,$conditions);
        echo json_encode($user);
 	}



    exit;
}
 

?>







