<?php

include 'Config/DB.php';

$db = new DB();

$tblName = 'poll_question';

date_default_timezone_set('Asia/Kolkata');
$today_date = date('Y-m-d', time()); 		

if(isset($_POST['action_type']) && !empty($_POST['action_type'])){
    
	if($_POST['action_type'] == 'add'){
		
			$msg = "";
			$ppart= explode('/',$_POST['expiry_date']);
			$date=$ppart[2]."-".$ppart[1]."-".$ppart[0];
		
		//CHECK FOR ExPIRY DATE CANT BE SET TADAY OR PREVIOUS DATE
		if($date <= $today_date){
			$msg = "expiry_date";
		}else{
			$userData = array(
				'vPollQuestion' => $_POST['vPollQuestion'],
				'expiry_date' => $date
            );

            $insert = $db->insert("poll_question",$userData);
			
			//QUESTION ID
			$q_id = $insert;

            //TOTAL ANSWER
			$count = count($_POST['vPollOption']);
			
			if($insert){
			
				for($i=0;$i<$count;$i++){
							
					$vPollOption = $_POST['vPollOption'][$i];
					$userData = array(
							'iPollQuestionID' =>$q_id,
							'vPollOption' => $vPollOption,
					);
					
					$insert = $db->insert("poll_option",$userData);
					
					$msg = $insert?"ok":"err";
				}
			}
		

		}
		echo $msg;
    }
	
	elseif($_POST['action_type'] == 'edit'){
		$msg='';
		if(!empty($_POST['vPollQuestion'])){
           			
			$ppart= explode('/',$_POST['expiry_date']);
			$date=$ppart[2]."-".$ppart[1]."-".$ppart[0];
			
			//CHECK FOR ExPIRY DATE CANT BE SET TADAY OR PREVIOUS DATE
		if($date <= $today_date){
			$msg = "expiry_date";
		}else{
			$userData = array(
				'vPollQuestion' => $_POST['vPollQuestion'],
				'expiry_date' => $date,
            );
			
			
			$condition = array('iPollQuestionID' => $_POST['iPollQuestionID']);
            $update = $db->update("poll_question",$userData,$condition);
			
			
            //TOTAL ANSWER
			$count = count($_POST['vPollOption']);
			
			//DELETE OLD OPTIONS
			$condition = array('iPollQuestionID' => $_POST['iPollQuestionID']);
            $delete = $db->delete("poll_option",$condition);
            
				
			$count = count($_POST['vPollOption']);
			
			if($delete)
			{
				for($i=0;$i<$count;$i++)
					{	
						$vPollOption = $_POST['vPollOption'][$i];
						$userData = array(
								'iPollQuestionID' => $_POST['iPollQuestionID'],
								'vPollOption' => $vPollOption,
						);
						$insert = $db->insert("poll_option",$userData);
						$msg =  $insert?"ok":"err";
					}
			}
		
			
		}
		 echo $msg;
		}
		
    }elseif($_POST['action_type'] == 'delete'){
        if(!empty($_POST['iPollQuestionID'])){
            $condition = array('iPollQuestionID' => $_POST['iPollQuestionID']);
            $delete = $db->delete($tblName,$condition);
            echo $delete?'ok':'err';
        }
	}elseif($_POST['action_type'] == 'data')
	{
		
       $conditions['where'] = array('iPollQuestionID'=>$_POST['iPollQuestionID']);
       $pollmaster = $db->getRows("poll_option",$conditions);
		
		foreach($pollmaster as $poll_question){
		
		$iPollQuestionID = $poll_question['iPollQuestionID'];
		$iPollOptionID = $poll_question['iPollOptionID'];
		
		// echo $iPollOptionID;
		$poll = $db->selectQuery("SELECT p.iPollOptionID,p.iPollQuestionID,p.vPollOption,q.iPollQuestionID,q.vPollQuestion,q.expiry_date FROM poll_option p LEFT JOIN poll_question q ON p.iPollQuestionID=q.iPollQuestionID where p.iPollQuestionID = $iPollQuestionID order by p.iPollOptionID");
		
		$poll['poll_question'] = $poll; 
	
		}
		
        echo json_encode($poll);
	}
	
	
	 elseif($_POST['action_type'] == 'getOptions')
	 {
		$iPollQuestionID = $_POST['iPollQuestionID'];
        $user = $db->selectQuery("SELECT p.vPollOption,count(pa.iPollAnswerID) as answ,(select count(iPollAnswerID) from poll_answer_by_user where iPollQuestionID = $iPollQuestionID) as total FROM poll_option p LEFT JOIN poll_answer_by_user pa ON pa.iPollOptionID = p.iPollOptionID where p.iPollQuestionID = $iPollQuestionID GROUP BY p.iPollOptionID");
		
		
		if($user[0]['total'] != 0){
			$total = $user[0]['total'];
		}else{
			$total = 1;
		}
		
		foreach($user as $key => $value){
			$tot = floatval($user[$key]['answ']*100);
			$perc = round(floatval($tot/$total),2);
			$user[$key]['perc'] = $perc;
		}
		
        echo json_encode($user);
		
	 }
	
	
}

    exit;

 

?>







