<?php include'header.php';

if($_SESSION['user_role'] != 'admin'  && $_SESSION['user_role'] != 'supervisor')

	{

	header('Location: index.php');

	exit(0);	

}

?>

<script>

//for validation

function validation(method)

{	   

	var vProductName = false;
	
	var iCount = false;
	
	var product_date = false;

	var vImageName = false;

	if(!$('#vProductName').val())

	{

			$('#vProductName').parent().addClass('has-error');

			$('#vProductName_help').show();

	}			

	else{$('#vProductName').parent().removeClass('has-error');$('#vProductName_help').hide();vProductName=true;}

		

		if(!$('#vImageName').val())

	{

			$('#vImageName').parent().addClass('has-error');

			$('#vImageName_help').show();

	}			

	else{$('#vImageName').parent().removeClass('has-error');$('#vImageName_help').hide();vImageName=true;}
	
	if(!$('#iCount').val())

	{

			$('#iCount').parent().addClass('has-error');

			$('#iCount_help').show();

	}			

	else{$('#iCount').parent().removeClass('has-error');$('#iCount_help').hide();iCount=true;}
	
	if(!$('#product_date').val())

	{

			$('#product_date').parent().addClass('has-error');

			$('#product_date_help').show();

	}			

	else{$('#product_date').parent().removeClass('has-error');$('#expiry_date_help').hide();product_date=true;}


	if(vProductName && vImageName && iCount && product_date) 

	{

			vaction(method);

	} 

	else{alert("There is some problem occured. Please try again.")}

		

}

//for table data 

	function editEntry(id){   
		$('.help-block').hide();

		$('.has-error').removeClass('has-error');

		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url: 'futureproductaction.php',
			data: 'action_type=data&iProductImageID='+id,
			success:function(data){

				$('#iProductImageID').val(data.iProductImageID);	

				$('#vProductName').val(data.vProductName);
				
				
				$('#product_date').val(toChangeDateFormat(data.product_date));	
				
				$('#iCount').val(data.iCount);	
				
				$('#product_start_time').val(data.product_start_time);	
				
				$('#product_end_time').val(data.product_end_time);	
				
				$('#product_expiry_time').val(data.product_expiry_time);	

				if(data.isSubProduct == 'Yes'){
					$("#realtime").attr('checked', true);
				}else{
					$("#realtime").attr('checked', false);
				}
				var src = 'images/product/'+data.vImageName;
				$('#blah').attr('src', src);
			}
		});
	}

	

//for rack master

</script>

<script>

function vaction(type,id){

    id = (typeof id == "undefined")?'':id;

    var userData = '';

 

 if (type == 'add') 

 {

     var userData = new FormData($(".Product_entry")[0]);

	userData.append('action_type', type);

    $.ajax({

        url: 'futureproductaction.php',

        type: 'POST',

        data: userData,

        async: false,

        success: function (data) {

			if(data == 'ok'){

			$('#example').DataTable().ajax.reload(null, false);

			showSuccesMessage();

			resetdata();

			$('#blah').attr('src', 'images/image_upload.jpg');

           }

		},

        cache: false,

        contentType: false,

        processData: false,

    });



    return false;



	

    }

	else if (type == 'edit'){

      
     var userData = new FormData($(".Product_entry")[0]);

	userData.append('action_type', type);

		$.ajax({

			url: 'futureproductaction.php',

			type: 'POST',

			data: userData,

			async: false,

			success: function (data) {

			if(data == 'ok'){

				$('#example').DataTable().ajax.reload(null, false);

				showEditSuccesMessage();

				var src = 'images/image_upload.jpg';
				$('#blah').attr('src', src);
				
				resetdata();

				$(btnUpdate).hide();

				$(btnSave).show();

			}

        },
		
		
        cache: false,

        contentType: false,

        processData: false,

    });

	  return false;
	
     }

	 

	 

	else if (type == 'delete'){

     

		userData ='action_type=delete&iProductImageID='+id; 

		$.ajax({	

			url: 'futureproductaction.php',

			type: 'POST',

			data: userData,

			async: false,

			success: function (data) {

				

				$('#example').DataTable().ajax.reload(null, false);

        },

       

    });

				

    }

    

    }

function resetdata(){

	

	  $('.Product_entry')[0].reset();

	  $('.help-block').hide();

	  $('.has-error').removeClass('has-error');

	var src = 'images/image_upload.jpg';
				$('#blah').attr('src', src);
}



function prevent(e) {

   e.preventDefault();

}

</script>



<?php include'navbar.php' ?>

    <?php include'sidebar.php' ?>

  <section class="content">

        <div class="container-fluid">

           

        </div>

		 

		 <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">

                        <div class="header">

                            <h2><i class="fa fa-gear" aria-hidden="true"></i>Future  Product Details</h2>

                         

                        </div>

                        <div class="body">

                            <form id="form_validation" method="POST" class="Product_entry">

							<div class="row">

								<div class="col-sm-4">

									<label class="form-label">Product  Name</label>

                                        <div class="form-line">

                                            <input type="text" class="form-control" placeholder="Enter Product  Name" name="vProductName" id="vProductName" />

                                        </div>

										<span class="help-block" id="vProductName_help" style="display:none;font-size:12px;color:red;">Please Enter Product Name.</span>

                                    </div>
									
									
									<div class="col-sm-4">

										<label class="form-label">Count</label>

                                        <div class="form-line">

                                            <input type="text" class="form-control" placeholder="Enter Count" name="iCount" id="iCount" />

                                        </div>

										<span class="help-block" id="iCount_help" style="display:none;font-size:12px;color:red;">Please Enter Count.</span>

                                    </div>
									
									<div class="col-md-4">
										<div class="form-line">
											<label class="form-label">Product Date</label>
												<input type="text" class="datepicker form-control" placeholder="Please choose a date..." name="product_date" id="product_date">
											<span class="help-block" id="product_date_help" style="display:none;font-size:12px;color:red;">Please Enter Expiry Date.</span>
										
										</div> 
									</div>
								

									<div class="col-sm-4">

										<div class="form-line">

										  <i class="fa fa-image"></i> Upload Image

										  <input type="file" name="vImageName" id="vImageName" accept=".jpg, .jpeg, .png" tabindex="0" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">

										  <span class="help-block" id="vImageName_help" style="display:none;font-size:12px;color:red;">Please Upload Image.</span>

										<!--<input type="file" name="image" id="image">-->

										</div>

									<p class="help-block">Max size 1 Mb</p>

							
									</div>

									

				<div class="col-sm-4">

				<label class="margin-left">&nbsp;</label>

				<img id="blah" src="images/image_upload.jpg" class="img-thumbnail" width="200px" height="150px" />

				</div>

				

				<!--IMAGE PREVIEW CODE-->
								
									
									
								</div>

							

								<input type="hidden" id="iProductImageID" name="iProductImageID"/>

								 <div class="row clearfix js-sweetalert">

								

                                <button id="btnSave" class="btn bg-blue btn-sm waves-effect" type="button" style="margin-left:20px;" onclick="validation('add');"><i class="material-icons">save</i><span class="icon-name">Save</span></button>

								

                                <button type="button" id="btnUpdate" class="btn bg-orange btn-sm waves-effect"  style="display: none;margin-left:20px;" onclick="vaction('edit');">

								<i class="material-icons">edit</i><span class="icon-name">Update</span></button>

								

                                 <button type="button" id="btnReset" class="btn bg-red btn-sm waves-effect" style="margin-left:20px;"  onclick="resetdata();$('#btnUpdate').hide();$('#btnSave').show();"><i class="material-icons">repeat</i><span class="icon-name">Reset</span></button>

                            </form>

                        </div>

						</div>

                    </div>

                </div>

				

            </div>

			 <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">

                        <div class="header">

                            <h2>

                               <i class="fa fa-table" aria-hidden="true"></i> Future  Product List

                            </h2>

                         

                        </div>

                        <div class="body">

                            <div class="table-responsive">

                                <table id="example" class="table table-bordered table-striped table-hover dataTable">

                                    <thead>

                                        <tr>

                                            <th>&nbsp;</th>

                                            <th>SR NO</th>

                                            <th>Product Name</th>  
											
                                            <th>Count</th>  

											<th>Image</th>

                                            <th>Product Date</th>

											<th>Action</th>

                                        </tr>

                                    </thead>

                       

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>



    </section>







 <?php include'footer.php' ?>

 <script>

 	  function toChangeDateFormat(dateStr)

{

    var parts = dateStr.split("-");

    //return new Date(parts[2], parts[1], parts[0]);

	var dt=  parts[2]+"/"+parts[1]+"/"+parts[0];

	return dt;

}



//ADD FOCUS ON SIDEBAR

	$("#sidebar_ul").find(".active").removeClass("active");

	$("#sidebar_future_product").addClass("active").focus();

$( document ).ready(function() {



	var otable = $('#example').DataTable( {

	dom: 'Bfrtip',

	buttons: [

            'copy', 'csv', 'excel', 'pdf', 'print'

        ],

"ajax": "futureproductdata.php",

"bPaginate":true,

"bProcessing": true,

"dataSrc":"data",

 "columns": [

{ mData: 'iProductImageID' },

{ mData: 'count' },

{ mData: 'vProductName' } ,

{ mData: 'iCount' } ,

{ mData: 'vImageName' } ,

{ mData: 'product_date'  },

{ mData: 'action' }

],

"columnDefs": [



{ targets: [0],

        "mData": "iProductImageID",

		"sClass": "center",

		"width": "1%",

		"visible":false

 },



  {targets: [4],

        "mData": "vImageName",

        "sClass": "center",

        "mRender": function (data, type, row) {

		return '<img src="images/product/'+row.vImageName+'" width="42" height="42">'

        }

    },  

	
	

	// {targets: [7],

        // "mData": "action",

        // "sClass": "center",

        // "mRender": function (data, type, row)
	// {

		// var a = '<button type="button" class="btn bg-blue waves-effect pull-left btn-sm" onclick="$(\'#btnSave\').hide();$(\'#btnUpdate\').show();editEntry('+row.iProductImageID+')"><i class="material-icons">edit</i></button>' ;

		// if(row.iProductImageID > 5)
		// {
			
			// a +='<button class="btn bg-red waves-effect pull-left btn-sm" onclick="showConfirmMessage('+row.iProductImageID+');"><i class="material-icons">delete_forever</i></button>';

		// } 
		// return a;
    // }

    // },
	
	 {targets: [6],
        "mData": "action",
        "sClass": "center",
        "mRender": function (data, type, row) {
		return '<span style="align:center"><button type="button" class="btn bg-blue waves-effect btn-sm" onclick="$(\'#btnSave\').hide();$(\'#btnUpdate\').show();editEntry('+row.iProductImageID+')"><i class="material-icons">edit</i></button>'+'<button class="btn bg-red waves-effect btn-sm" onclick="showConfirmMessage('+row.iProductImageID+');"><i class="material-icons">delete_forever</i></button></span>'
        }
    },

]		



});

});



</script>

<script>



function showStopMessage() {

    swal({

        title: "Stop!",

        text: "Dont cut this log.!",

        type: "error",



        closeOnConfirm: false

    });

}



function showSuccesMessage() {

    swal({

        title: "Success!",

        text: "Product Details has been Added succesfully.!",

        type: "success",



        closeOnConfirm: false

    });

}



function showEditSuccesMessage() {

    swal({

        title: "Success!",

        text: "Product Details has been Updated succesfully.!",

        type: "success",



        closeOnConfirm: false

    });

}



function showWarningMessage() {

    swal({

        title: "oops!",

        text: "Something went wrong, please check the values and try again.!",

        type: "warning",



        closeOnConfirm: false

    });

}



function showConfirmMessage(iProductImageID) {

    swal({

        title: "Are you sure?",

        text: "You will not be able to recover this data!",

        type: "warning",

        showCancelButton: true,

        confirmButtonColor: "#DD6B55",

        confirmButtonText: "Yes, delete it!",

        closeOnConfirm: false

    }, function () {

		vaction('delete',iProductImageID);

        swal("Deleted!", "Product data has been deleted successfully.", "success");

    });

}
//Date picker
    $('#product_date').datepicker({
      autoclose: true
    });
	$("#product_date").datepicker().datepicker("setDate", new Date());


</script>
<script src="js/pages/forms/advanced-form-elements.js"></script>
<script src="js/pages/forms/basic-form-elements.js"></script>
</body>



</html>