<?php

include 'Config/DB.php';

$db = new DB();

$tblName = 'aboutus_master';

if(isset($_POST['action_type']) && !empty($_POST['action_type'])){
    if($_POST['action_type'] == 'add'){
		if(!empty($_POST['tText'])){
            $userData = array(
				'tText' => $_POST['tText'],
				'vInstaName' => $_POST['vInstaName'],
				'vInstaUrl' => $_POST['vInstaUrl'],
				'vTwitterName' => $_POST['vTwitterName'],
				'vTwitterUrl' => $_POST['vTwitterUrl'],
				'vFacebookName' => $_POST['vFacebookName'],
				'vFacebookUrl' => $_POST['vFacebookUrl'],
				'vWhatsappName' => $_POST['vWhatsappName'],
				'vWhatsappUrl' => $_POST['vWhatsappUrl'],
				'vYoutubeName' => $_POST['vYoutubeName'],
				'vYoutubeUrl' => $_POST['vYoutubeUrl'],
				'vWebName' => $_POST['vWebName'],
				'vWebUrl' => $_POST['vWebUrl'],
				'vTelegramName' => $_POST['vTelegramName'],
				'vTelegramUrl' => $_POST['vTelegramUrl'],
				
            );

            $insert = $db->insert($tblName,$userData);
              echo $insert?'ok':'err';
            
		
				$image_id=$insert;
				
				
				$image_name = time().'_'.$_FILES["vInstaCover"]["name"];
				$folder = "images/aboutus/Insta/".$image_name;
				move_uploaded_file($_FILES["vInstaCover"]["tmp_name"], "$folder");
				$imgData = array(
					'vInstaCover' => $image_name,
				);

				$condition = array('iAboutusMasterID' => $image_id);
				$update = $db->update($tblName,$imgData,$condition); 
		
		
				$image_id=$insert;
				
				
				$image_name = time().'_'.$_FILES["vTwitterCover"]["name"];
				$folder = "images/aboutus/Twitter/".$image_name;
				move_uploaded_file($_FILES["vTwitterCover"]["tmp_name"], "$folder");
				$imgData = array(
					'vTwitterCover' => $image_name,
				);

				$condition = array('iAboutusMasterID' => $image_id);
				$update = $db->update($tblName,$imgData,$condition);
				
				
				$image_id=$insert;
				
				
				$image_name = time().'_'.$_FILES["vFacebookCover"]["name"];
				$folder = "images/aboutus/Facebook/".$image_name;
				move_uploaded_file($_FILES["vFacebookCover"]["tmp_name"], "$folder");
				$imgData = array(
					'vFacebookCover' => $image_name,
				);

				$condition = array('iAboutusMasterID' => $image_id);
				$update = $db->update($tblName,$imgData,$condition);
				
				$image_id=$insert;
				
				
				$image_name = time().'_'.$_FILES["vWhatsappCover"]["name"];
				$folder = "images/aboutus/Whatsapp/".$image_name;
				move_uploaded_file($_FILES["vWhatsappCover"]["tmp_name"], "$folder");
				$imgData = array(
					'vWhatsappCover' => $image_name,
				);

				$condition = array('iAboutusMasterID' => $image_id);
				$update = $db->update($tblName,$imgData,$condition);
				
				
				$image_id=$insert;
				
				
				$image_name = time().'_'.$_FILES["vYoutubeCover"]["name"];
				$folder = "images/aboutus/Youtube/".$image_name;
				move_uploaded_file($_FILES["vYoutubeCover"]["tmp_name"], "$folder");
				$imgData = array(
					'vYoutubeCover' => $image_name,
				);

				$condition = array('iAboutusMasterID' => $image_id);
				$update = $db->update($tblName,$imgData,$condition);
				
				$image_id=$insert;
				
				
				$image_name = time().'_'.$_FILES["vWebCover"]["name"];
				$folder = "images/aboutus/Web/".$image_name;
				move_uploaded_file($_FILES["vWebCover"]["tmp_name"], "$folder");
				$imgData = array(
					'vWebCover' => $image_name,
				);

				$condition = array('iAboutusMasterID' => $image_id);
				$update = $db->update($tblName,$imgData,$condition);
				
				
				$image_id=$insert;
				
				
				$image_name = time().'_'.$_FILES["vTelegramCover"]["name"];
				$folder = "images/aboutus/Telegram/".$image_name;
				move_uploaded_file($_FILES["vTelegramCover"]["tmp_name"], "$folder");
				$imgData = array(
					'vTelegramCover' => $image_name,
				);

				$condition = array('iAboutusMasterID' => $image_id);
				$update = $db->update($tblName,$imgData,$condition);
				
				
			
		}
    }elseif($_POST['action_type'] == 'edit'){
        if(!empty($_POST['iAboutusMasterID'])){
			$iAboutusMasterID = $_POST['iAboutusMasterID'];
            $updatedata = array(
				'tText' => $_POST['tText'],
				'vInstaName' => $_POST['vInstaName'],
				'vInstaUrl' => $_POST['vInstaUrl'],
				'vTwitterName' => $_POST['vTwitterName'],
				'vTwitterUrl' => $_POST['vTwitterUrl'],
				'vFacebookName' => $_POST['vFacebookName'],
				'vFacebookUrl' => $_POST['vFacebookUrl'],
				'vWhatsappName' => $_POST['vWhatsappName'],
				'vWhatsappUrl' => $_POST['vWhatsappUrl'],
				'vYoutubeName' => $_POST['vYoutubeName'],
				'vYoutubeUrl' => $_POST['vYoutubeUrl'],
				'vWebName' => $_POST['vWebName'],
				'vWebUrl' => $_POST['vWebUrl'],
				'vTelegramName' => $_POST['vTelegramName'],
				'vTelegramUrl' => $_POST['vTelegramUrl'],
				
            );
			
				if($_FILES["vInstaCover"]["error"] == 0){
					$insta_image_name = time().'_'.$_FILES["vInstaCover"]["name"];
					$folder = "images/aboutus/Insta/".$insta_image_name;
					move_uploaded_file($_FILES["vInstaCover"]["tmp_name"], "$folder");
					
					$updatedata['vInstaCover'] = $insta_image_name;
				}
								
				if($_FILES["vTwitterCover"]["error"] == 0){
					$Twitter_image_name = time().'_'.$_FILES["vTwitterCover"]["name"];
					$folder = "images/aboutus/Twitter/".$Twitter_image_name;
					move_uploaded_file($_FILES["vTwitterCover"]["tmp_name"], "$folder");
					
					$updatedata['vTwitterCover'] = $Twitter_image_name;
				}
				
				if($_FILES["vFacebookCover"]["error"] == 0){
					$Facebook_image_name = time().'_'.$_FILES["vFacebookCover"]["name"];
					$folder = "images/aboutus/Facebook/".$Facebook_image_name;
					move_uploaded_file($_FILES["vFacebookCover"]["tmp_name"], "$folder");
					
					$updatedata['vFacebookCover'] = $Facebook_image_name;
				}
				
				if($_FILES["vWhatsappCover"]["error"] == 0){
					$Whatsapp_image_name = time().'_'.$_FILES["vWhatsappCover"]["name"];
					$folder = "images/aboutus/Whatsapp/".$Whatsapp_image_name;
					move_uploaded_file($_FILES["vWhatsappCover"]["tmp_name"], "$folder");
					
					$updatedata['vWhatsappCover'] = $Whatsapp_image_name;
				}
				
				if($_FILES["vYoutubeCover"]["error"] == 0){
					$Youtube_image_name = time().'_'.$_FILES["vYoutubeCover"]["name"];
					$folder = "images/aboutus/Youtube/".$Youtube_image_name;
					move_uploaded_file($_FILES["vYoutubeCover"]["tmp_name"], "$folder");
					
					$updatedata['vYoutubeCover'] = $Youtube_image_name;
				}	
				
			
				if($_FILES["vWebCover"]["error"] == 0){
					$Web_image_name = time().'_'.$_FILES["vWebCover"]["name"];
					$folder = "images/aboutus/Web/".$Web_image_name;
					move_uploaded_file($_FILES["vWebCover"]["tmp_name"], "$folder");
					
					$updatedata['vWebCover'] = $Web_image_name;
				}
				
				if($_FILES["vTelegramCover"]["error"] == 0){
					$Telegram_image_name = time().'_'.$_FILES["vTelegramCover"]["name"];
					$folder = "images/aboutus/Telegram/".$Telegram_image_name;
					move_uploaded_file($_FILES["vTelegramCover"]["tmp_name"], "$folder");
					
					$updatedata['vTelegramCover'] = $Telegram_image_name;
				}

				$condition = array('iAboutusMasterID' => $iAboutusMasterID);
				$update = $db->update($tblName,$updatedata,$condition);
				echo 'ok';
		}
    }elseif($_POST['action_type'] == 'delete'){
        if(!empty($_POST['iAboutusMasterID'])){
            $condition = array('iAboutusMasterID' => $_POST['iAboutusMasterID']);
            $delete = $db->delete($tblName,$condition);
            echo $delete?'ok':'err';
        }
	}elseif($_POST['action_type'] == 'data'){
        $conditions['where'] = array('iAboutusMasterID'=>$_POST['iAboutusMasterID']);
        $conditions['return_type'] = 'single';
        $user = $db->getRows($tblName,$conditions);
        echo json_encode($user);
 	}
}

    exit;

 

?>







