<?php include'header.php';
if($_SESSION['user_role'] != 'admin')
	{
	header('Location: index.php');
	exit(0);	
}
include 'Config/DB.php';
$db = new DB();
?>

<script>
//for validation
 
/* function validateEmail(sEmail) {
var filter =/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {return true;}
	else {return false;}
}
 */


/* function randomPass(){
		$.ajax({
		type: 'POST',
		url: 'usermasteraction.php',
		data: 'action_type=getPass',
		async: false,
		success:function(pass)
		{
			$("#password").val(pass);
		}
		});
} */
	
function validation(method)
{	   


	var vUserName = false;
	var vPassWord = false;
	var vEmailAddress = false;
	var vContactNumber = false;
	var vAddress = false;
	var vCity = false;
	var vRole = false;
	var vStatus = false;

	
	
	/* if($('#vUserName').val() == '')
	{
			 $('#vUserName').parent().addClass('has-error');
			 $('#vUserName_help').show();
	}
	
	if($('#username').val())
	{
		if(method == 'edit'){
			$.ajax({
			type: 'POST',
			url: 'usermasteraction.php',
			data: 'action_type=validateusernameEdit&username='+$('#username').val()+'&uid='+$('#uid').val(),
			async: false,
			success:function(msg)
			{
			if(msg == "err")
			{
			alert("This Supervisor Name is already registered");
			}
			else if(msg == "ok"){
				$('#username').parent().removeClass('has-error');$('#username_help').hide();
				username=true;
				}
			}
			});
			
		}
		else{
			$.ajax({
			type: 'POST',
			url: 'usermasteraction.php',
			data: 'action_type=validateusername&username='+$('#username').val(),
			async: false,
			success:function(msg)
			{
			if(msg == "err")
			{
			alert("This Supervisor Name is already registered on our database.");
			}
			else if(msg == "ok"){
				$('#username').parent().removeClass('has-error');$('#username_help').hide();
				username=true;
				}
			}
			});
		}
	}
	
	
	if(!$('#email').val())
	{
			 $('#email').parent().addClass('has-error');
			 $('#email_help').show();
	}	
	
	if($('#email').val())
	{
	if(validateEmail($('#email').val())){
		
	if(method == 'edit'){
		$customer = $('#email').val();	
		
		$.ajax({
		type: 'POST',
		url: 'usermasteraction.php',
		data: 'action_type=validateemailEdit&email='+$customer+'&uid='+$('#uid').val(),
		async: false,
		success:function(msg)
		{
		if(msg == "err")
		{
		alert("This email is already registered, please enter another email address.");
		}
		if(msg == "ok"){
			$('#email').parent().removeClass('has-error');$('#email_help').hide();
			email=true;
			}
		}
		});
		}
		
	
		else{
		$customer = $('#email').val();	
	
		$.ajax({
		type: 'POST',
		url: 'usermasteraction.php',
		data: 'action_type=validateemail&email='+$customer,
		async: false,
		success:function(msg)
		{
		if(msg == "err")
		{
		alert("This email is already registered, please enter another email address.");
		}
		if(msg == "ok"){
			$('#email').parent().removeClass('has-error');$('#email_help').hide();
			email=true;
			}
		}
		});
		}
		}
	}
	else{
				alert("Please enter valid email address.");
	} */
	

	if(!$('#vUserName').val())
	{
			$('#vUserName').parent().addClass('has-error');
			$('#vUserName_help').show();
	}			
	else{$('#vUserName').parent().removeClass('has-error');$('#vUserName_help').hide();vUserName=true;}
	
	if(!$('#vPassWord').val())
	{
			$('#vPassWord').parent().addClass('has-error');
			$('#vPassWord_help').show();
	}			
	else{$('#vPassWord').parent().removeClass('has-error');$('#vPassWord_help').hide();vPassWord=true;}
	
	if(!$('#vEmailAddress').val())
	{
			$('#vEmailAddress').parent().addClass('has-error');
			$('#vEmailAddress_help').show();
	}			
	else{$('#vEmailAddress').parent().removeClass('has-error');$('#vEmailAddress_help').hide();vEmailAddress=true;}
	
	if(!$('#vContactNumber').val())
	{
		$('#vContactNumber').parent().addClass('has-error');
		$('#vContactNumber_help').show();
	}
	else{$('#vContactNumber').parent().removeClass('has-error');$('#vContactNumber_help').hide();vContactNumber=true;}

	if(!$('#vCity').val())
	{
			$('#vCity').parent().addClass('has-error');
			$('#vCity_help').show();
	}
	else{$('#vCity').parent().removeClass('has-error');$('#vCity_help').hide();vCity=true;}

	
	if(!$('#vRole').val())
	{
			$('#vRole').parent().addClass('has-error');
			$('#vRole_help').show();
	}
	else{$('#vRole').parent().removeClass('has-error');$('#vRole_help').hide();vRole=true;}
	
	if(!$('#vStatus').val())
	{
			$('#vStatus').parent().addClass('has-error');
			$('#vStatus_help').show();
	}
	else{$('#vStatus').parent().removeClass('has-error');$('#vStatus_help').hide();vStatus=true;}
	
	if(!$('#vAddress').val())
	{
			$('#vAddress').parent().addClass('has-error');
			$('#vAddress_help').show();
	}
	else{$('#vAddress').parent().removeClass('has-error');$('#vAddress_help').hide();vAddress=true;}
	

	
	
	

	
	
	
	if( vUserName && vPassWord && vEmailAddress && vContactNumber && vCity && vAddress && vRole && vStatus  ) 
	{
			vaction(method);
	} 
	else{alert("There is some problem occured. Please try again.")}
		
}

//for table data 
function editEntry(id)
{   
	$.ajax({
	type: 'POST',
	dataType:'JSON',
	url: 'adminmasteraction.php',
	data: 'action_type=data&iAdminMasterID='+id,
	success:function(data){
	$('#iAdminMasterID').val(data.iAdminMasterID);	
	$('#vUserName').val(data.vUserName);	
	$('#vEmailAddress').val(data.vEmailAddress);	
	$('#vPassWord').val(data.vPassWord);	
	$('#vContactNumber').val(data.vContactNumber);	
	$('#vCity').val(data.vCity);
	$('#vAddress').val(data.vAddress);
	$('#vStatus').val(data.vStatus);	
	$('#vRole').val(data.vRole);	
	
	
	
	}
});
}
	

	
//for rack master
</script>
<script>
function vaction(type,id){
    id = (typeof id == "undefined")?'':id;
    var userData = '';
 
 if (type == 'add') 
 {
    var userData = 	$(".user_entry").serialize()+'&action_type='+type;
    $.ajax({
        url: 'adminmasteraction.php',
        type: 'POST',
        data: userData,
        async: false,
        success: function (data){
			if(data == 'ok'){
			$('#example').DataTable().ajax.reload(null, false);
			resetdata();
			/*  randomPass(); */
			showSuccesMessage();	
        }
	}
});

	
    }
	else if (type == 'edit'){
        var userData = $(".user_entry").serialize()+'&action_type='+type;
		
		$.ajax({
			url: 'adminmasteraction.php',
			type: 'POST',
			data: userData,
			async: false,
			success: function (data) {
				
				$('#example').DataTable().ajax.reload(null, false);
				resetdata();
				$(btnUpdate).hide();
				$(btnSave).show();
				showEditSuccesMessage();
			
        }
    });
     }
	 
	 
	else if (type == 'delete'){
     
		userData ='action_type=delete&iAdminMasterID='+id; 
		$.ajax({	
			url: 'adminmasteraction.php',
			type: 'POST',
			data: userData,
			async: false,
			success: function (data) {
				
				$('#example').DataTable().ajax.reload(null, false);
        },
       
    });
				
    }
    
    }
function resetdata(){
	
	  $('.user_entry')[0].reset();
}

function prevent(e) {
   e.preventDefault();
}
</script>

<?php include'navbar.php'?>
    <?php include'sidebar.php'?>
  <section class="content">
  <!--      <div class="container-fluid">
            <div class="block-header">
                <h2>Consignment Details</h2>
            </div>
        </div>
		 -->
		 <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2><i class="fa fa-fw fa-user"></i><span>  Admin Master</span></h2>
                         
                        </div>
                        <div class="body">
                            <form id="form_validation" method="POST" class="user_entry">
							
								<div class="row">
									<div class="col-md-3">
									<label class="form-label">Admin  Name</label><span style="color:red"> * </span>
                                        <div class="form-line">
                                           <input type="text"  id="vUserName" class="form-control input-md" tabindex="0" name="vUserName"  placeholder="Enter User Name..." autocomplete="off">
											<span class="help-block" id="vUserName_help" style="display:none;">Please Enter User Name.</span>
                                        </div>
                                    </div>
								
								
								<div class="col-md-3">
									<label class="form-label">E-mail</label><span style="color:red"> * </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control input-md" id="vEmailAddress"  name="vEmailAddress" placeholder="Enter E-mail..." tabindex="0">
										<span class="help-block" id="vEmailAddress_help" style="display:none;">Please Enter E-mail.</span>
                                        </div>
                                    </div>
									<div class="col-md-3">
									<label class="form-label">Password</label><span style="color:red"> * </span>
                                        <div class="form-line">
                                             <input type="text" class="form-control input-md" id="vPassWord"  name="vPassWord" placeholder="Enter Password...">
										<span class="help-block" id="vPassWord_help" style="display:none;">Please Enter Password.</span>
                                        </div>
                                    </div>
                                   
									
									<div class="col-md-3">
									<label class="form-label">Role</label><span style="color:red"> * </span>
                                      
                                            <select class="form-control input-md" name="vRole" id="vRole">
										<option value="">Select Role..</option>	
										<option  value="role1">role 1</option>
										<option  value="role2">role 2</option>
										
										</select>
                                      <span class="help-block" id="vRole_help" style="display:none;">Please Select Role.</span>
                                    </div>
									 </div>
								<div class="row">
									<div class="col-md-4">
									<label class="form-label">Contact Number</label><span style="color:red"> * </span>
                                        <div class="form-line">
                                             <input type="number" class="form-control input-md" id="vContactNumber"  name="vContactNumber" placeholder="Enter Contact No..." tabindex="0" autocomplete="off">
											<span class="help-block" id="vContactNumber_help" style="display:none;">Please Enter Contact No.</span>
                                        </div>
                                    </div>
                                
								
								<div class="col-md-4">
									<label class="form-label">City</label><span style="color:red"> * </span>
                                        <div class="form-line">
                                           <input type="text"  id="vCity" class="form-control input-md" tabindex="0" name="vCity"  placeholder="Enter City..." autocomplete="off">
											<span class="help-block" id="vCity_help" style="display:none;">Please Enter City.</span>
                                        </div>
                                    </div>
									
									<div class="col-md-4">
									<label class="form-label">Status</label><span style="color:red"> * </span>
                                      
                                            <select class="form-control input-md" name="vStatus" id="vStatus">
										<option>Active</option>
										<option>Deactive</option>
                  
										</select>
										<span class="help-block" id="vStatus_help" style="display:none;">Please Select Status.</span>
                                      
                                    </div>
									
								
                                </div>
									
								<div class="row">
									<div class="col-md-12">
									<label class="form-label">Address</label>
                                        <div class="form-line">
                                           <textarea class="form-control" rows="4" id="vAddress" name="vAddress" placeholder="Enter Address..." tabindex="0"></textarea>
										<span class="help-block" id="vAddress_help" style="display:none;">Please Enter Address.</span>
                                        </div>
                                    </div>
								</div>
									
								<input type="hidden" id="iAdminMasterID" name="iAdminMasterID"/>
								 <div class="row clearfix js-sweetalert">
								
								<div class="row">
								<div class="col-md-12">
                                <button id="btnSave" class="btn bg-blue waves-effect"  type="button" style="margin-left:20px;" onclick="validation('add');"><i class="material-icons">save</i><span class="icon-name">Save</span></button>
								
                                <button type="button" id="btnUpdate" class="btn bg-orange waves-effect"  style="display: none;margin-left:20px;" onclick="vaction('edit');">
								<i class="material-icons">edit</i><span class="icon-name">Update</span></button>
								
                                 <button type="button" id="btnReset" class="btn bg-red" style="margin-left:20px;"  onclick="resetdata();$('#btnUpdate').hide();$('#btnSave').show();randomPass();"><i class="material-icons">repeat</i><span class="icon-name">Reset</span></button>
								 </div>
								 </div>
                            </form>
                        </div>
						</div>
                    </div>
                </div>
				
            </div>
			 <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                            <i class="fa fa-fw fa-user"></i>   Admin List
                            </h2>
                          
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="example" class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
											<th>&nbsp;</th>
                                            <th>SR NO</th>
											<th>Admin Name  </th>
											<th>Email</th>
											<th>Contact No</th>
											<th>City</th>
											<th>Address</th>
											<th>Status</th>
											<th>Role</th>
											<th>Date</th>
											<th>action</th>
                                        </tr>
                                    </thead>
                       
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </section>



 <?php include'footer.php'?>
 <script>
 	  function toChangeDateFormat(dateStr)
{
    var parts = dateStr.split("-");
    //return new Date(parts[2], parts[1], parts[0]);
	var dt=  parts[2]+"/"+parts[1]+"/"+parts[0];
	return dt;
}

//ADD FOCUS ON SIDEBAR
	$("#sidebar_ul").find(".active").removeClass("active");
	$("#sidebar_admin_master").addClass("active").focus();
$( document ).ready(function() {

	var otable = $('#example').DataTable( {
	dom: 'Bfrtip',
	buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
"ajax": "adminmasterdata.php",
"bPaginate":true,
"bProcessing": true,


"dataSrc":"data",
 "columns": [
{ mData: 'iAdminMasterID' },
{ mData: 'count' },
{ mData: 'vUserName' } ,
{ mData: 'vEmailAddress' } ,
{ mData: 'vContactNumber' } ,
{ mData: 'vCity' } ,
{ mData: 'vAddress' } ,
{ mData: 'vStatus' } ,
{ mData: 'vRole' } ,
{ mData: 'created'  },
{ mData: 'action' }
],
"columnDefs": [

{ targets: [0],
        "mData": "iAdminMasterID",
		"sClass": "center",
		"width": "1%",
		"visible":false
 },
 
 {targets: [7],
				"mData": "vStatus",
					"sClass": "center",
					"mRender": function (data, type, row) {
						return '<button type="button" class="btn btn-flat btn-green pull-left bg-green btn-sm" >'+row.vStatus+'</button>'
					}
				},
				{targets: [8],
					"mData": "vRole",
					"sClass": "center",
					"mRender": function (data, type, row) {
						return '<button type="button" class="btn btn-flat btn- pull-left bg-indigo btn-sm" >'+row.vRole+'</button>'
					}
				},

  {targets: [10],
        "mData": "action",
        "sClass": "center",
        "mRender": function (data, type, row) {
		return '<button type="button" class="btn bg-blue waves-effect pull-left btn-sm" onclick="$(\'#btnSave\').hide();$(\'#btnUpdate\').show();editEntry('+row.iAdminMasterID	+')"><i class="material-icons">edit</i></button>' +
		'<button class="btn bg-red waves-effect pull-left btn-sm" onclick="showConfirmMessage('+row.iAdminMasterID	+');"><i class="material-icons">delete_forever</i></button>'
        }
    },
]		

});
});

</script>
<script>

function showStopMessage() {
    swal({
        title: "Stop!",
        text: "Dont cut this log.!",
        type: "error",

        closeOnConfirm: false
    });
}

function showSuccesMessage() {
    swal({
        title: "Success!",
        text: "User Details has been Added succesfully.!",
        type: "success",

        closeOnConfirm: false
    });
}

function showEditSuccesMessage() {
    swal({
        title: "Success!",
        text: "User Details has been Updated succesfully.!",
        type: "success",

        closeOnConfirm: false
    });
}

function showWarningMessage() {
    swal({
        title: "oops!",
        text: "Something went wrong, please check the values and try again.!",
        type: "warning",

        closeOnConfirm: false
    });
}

function showConfirmMessage(iAdminMasterID) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
		vaction('delete',iAdminMasterID);
        swal("Deleted!", "User data has been deleted successfully.", "success");
    });
}

</script>
<script src="js/pages/forms/basic-form-elements.js"></script>
</body>

</html>