<?php



ob_start();

session_start();

   $error='';

   if(isset($_POST['submit'])){

    

      // username and password sent from form 

     include 'Config/config.php';

	  $status = 'Active';

	  $email = mysqli_real_escape_string($conn,$_POST['email']);

      $password = mysqli_real_escape_string($conn,$_POST['password']); 

      

      $sql = "SELECT * FROM user_master WHERE email = '$email' and password = '$password'";

      $result = mysqli_query($conn,$sql);

      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);

      $role = $row['role'];

	  $uid = $row['uid'];

	  $usertitle = $row['username'];

	  $status = $row['status'];

	  $sawmill_id = $row['sawmill_id'];

      

	  $count = mysqli_num_rows($result);

      // If result matched $myusername and $mypassword, table row must be 1 row

      if($count == 1) {

         //session_register("myusername");

		 

   if($role == 'admin')

		 {

			 $_SESSION['email'] = $email;

			 $_SESSION['user_role'] = $role;

			 $_SESSION['user_id'] = $uid;

			 $_SESSION['username'] = $usertitle;

			 header("location: categorymaster.php");

		 }

	

	else if($role == 'supervisor')

		 {

			if($status == 'Active')

			{ 	

				$_SESSION['email'] = $email;

				$_SESSION['user_role'] = $role;

				$_SESSION['user_id'] = $uid;

				$_SESSION['username'] = $usertitle;

				$_SESSION['sawmill_id'] = $sawmill_id;

				header("location: dispatchdetails.php");

			}

			else if($status == 'Deactive')

			{

				$error = "Can't Login, Your account is deactivated!";

			}

		 }

		 

	else if($role == 'dispatcher')

 {

	if($status == 'Active')

	{ 

		$_SESSION['email'] = $email;

		$_SESSION['user_role'] = $role;

		$_SESSION['user_id'] = $uid;

		$_SESSION['username'] = $usertitle;

		

				$_SESSION['sawmill_id'] = $sawmill_id;

		header("location: workermaster.php");

	}

	else if($status == 'Deactive')

	{

		$error = "Can't Login, Your account is deactivated!";

	}

 }

      }

	  else 

	  {

         $error = "Your Email or Password is invalid. Please try again.";

      }

		

   

   }

 ?>

 

<!DOCTYPE html>

<html>



<head>

    <meta charset="UTF-8">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <title>Sign In | VIGNYAPAN</title>

    <!-- Favicon-->

    <link rel="icon" href="favicon.ico" type="image/x-icon">



    <!-- Google Fonts -->

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">



    <!-- Bootstrap Core Css -->

    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">



    <!-- Waves Effect Css -->

    <link href="plugins/node-waves/waves.css" rel="stylesheet" />



    <!-- Animation Css -->

    <link href="plugins/animate-css/animate.css" rel="stylesheet" />



    <!-- Custom Css -->

    <link href="css/style.css" rel="stylesheet">

</head>



<body class="login-page">

     <div class="login-box">

        <div class="logo">

            <a href="javascript:void(0);"><b>VIGNYAPAN</b></a>

       <!--     <small>Login</small> -->

        </div>

        <div class="card">

            <div class="body">

                <form id="sign_in" method="POST">

                    <div class="msg">Sign in to start your session</div>

                    <div class="input-group">

                        <span class="input-group-addon">

                            <i class="material-icons">person</i>

                        </span>

                        <div class="form-line">

                            <input type="text" class="form-control"  placeholder="Email" id="email" name="email"  required autofocus>

                        </div>

                    </div>

                    <div class="input-group">

                        <span class="input-group-addon">

                            <i class="material-icons">lock</i>

                        </span>

                        <div class="form-line">

                            <input type="password" class="form-control"  placeholder="Password" id="password" name="password" required>

                        </div>

                    </div>

                    <div class="row">

                  

                        <div class="col-xs-4">

                            <button type="submit" class="btn btn-block bg-blue waves-effect" name="submit" id="submit" onclick="javascript:void(0);">SIGN IN</button>

                        </div>

                    </div>

					 <h5><?php echo $error;?></h5>

                 <!--       <div class="col-xs-6">

                            <a href="sign-up.html">Register Now!</a>

                        </div>

                        <div class="col-xs-6 align-right">

                            <a href="forgot-password.html">Forgot Password?</a>

                        </div>-->

                    </div>

                </form>

				

            </div>

        </div>

    </div>

<script>

  $(function () {

    $('input').iCheck({

      checkboxClass: 'icheckbox_square-blue',

      radioClass: 'iradio_square-blue',

      increaseArea: '20%' // optional

    });

  });

</script>

    <!-- Jquery Core Js -->

    <script src="plugins/jquery/jquery.min.js"></script>



    <!-- Bootstrap Core Js -->

    <script src="plugins/bootstrap/js/bootstrap.js"></script>



    <!-- Waves Effect Plugin Js -->

    <script src="plugins/node-waves/waves.js"></script>



    <!-- Validation Plugin Js -->

    <script src="plugins/jquery-validation/jquery.validate.js"></script>



    <!-- Custom Js -->

    <script src="js/admin.js"></script>

    <script src="js/pages/examples/sign-in.js"></script>

</body>



</html>