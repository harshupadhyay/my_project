<?php

include 'Config/DB.php';

$db = new DB();

$tblName = 'product_image';

if(isset($_POST['action_type']) && !empty($_POST['action_type'])){
    if($_POST['action_type'] == 'add'){
		
			$ppart= explode('/',$_POST['product_date']);
			$product_date=$ppart[2]."-".$ppart[1]."-".$ppart[0];
		
		if(!empty($_POST['vProductName'])){
            $userData = array(
				'vProductName' => $_POST['vProductName'],
				'iCount' => $_POST['iCount'],
				'product_date' => $product_date,
            );

            $insert = $db->insert($tblName,$userData);
			// echo $insert?"ok":"err";
            
			if(isset($_FILES["vImageName"]["name"])){
				$image_id=$insert;
				$image_name = time().'_'.$_FILES["vImageName"]["name"];
				$folder = "images/product/".$image_name;
				move_uploaded_file($_FILES["vImageName"]["tmp_name"], "$folder");
				$imgData = array(
					'vImageName' => $image_name,
				);

				$condition = array('iProductImageID' => $image_id);
				$update = $db->update($tblName,$imgData,$condition);
				echo $update?"ok":"err";
				
			}
		}
    }elseif($_POST['action_type'] == 'edit'){
		
			$ppart= explode('/',$_POST['product_date']);
			$product_date=$ppart[2]."/".$ppart[1]."/".$ppart[0];
			
			
        if(!empty($_POST['iProductImageID'])){
            $userData = array(
				'vProductName' => $_POST['vProductName'],
				'product_date' => $product_date,
				'iCount' => $_POST['iCount'],			
				);
            $condition = array('iProductImageID' => $_POST['iProductImageID']);
            $update = $db->update($tblName,$userData,$condition);
			
			if($_FILES["vImageName"]["error"]== 0){
				$image_name = time().'_'.$_FILES["vImageName"]["name"];
				//$folder = $_SERVER['DOCUMENT_ROOT']."/project/vignyapan/admin/images/product/".$image_name;
				$folder = "images/product/".$image_name;
				move_uploaded_file($_FILES["vImageName"]["tmp_name"], "$folder");
				$imgData = array(
						'vImageName' => $image_name,
					);
				$condition = array('iProductImageID' => $_POST['iProductImageID']);
				$update = $db->update($tblName,$imgData,$condition);
			}
			echo $update?"ok":"err";
		}
		
    }elseif($_POST['action_type'] == 'delete'){
        if(!empty($_POST['iProductImageID'])){
            $condition = array('iProductImageID' => $_POST['iProductImageID']);
            $delete = $db->delete($tblName,$condition);
            echo $delete?'ok':'err';
        }
	}elseif($_POST['action_type'] == 'data'){
        $conditions['where'] = array('iProductImageID'=>$_POST['iProductImageID']);
        $conditions['return_type'] = 'single';
        $user = $db->getRows($tblName,$conditions);
        echo json_encode($user);
 	}
}

    exit;

 

?>







