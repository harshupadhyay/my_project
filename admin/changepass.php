<?php include'header.php';
if($_SESSION['user_role'] == 'admin' or $_SESSION['user_role'] == 'supervisor')
	{}
	else
	{
	header('Location: index.php');
	exit(0);	
	}
?>
<script>
//for validation

function validation(method)
{	   
	var password = false;
	var confirm = false;
	
	if(!$('#password').val())
	{
			$('#password').parent().addClass('has-error');
			$('#password_help').show();
	}			
	else{$('#password').parent().removeClass('has-error');$('#password_help').hide();password=true;}
	
	if(!$('#confirm').val())
	{
		$('#confirm').parent().addClass('has-error');
		$('#confirm_help').show();
	}
	else{$('#confirm').parent().removeClass('has-error');$('#confirm_help').hide();confirm=true;}

	if( password &&  confirm ) 
	{
			vaction(method);
	} 
	else{alert("There is some problem occured. Please try again.")}
		
}
 
function vaction(type){
 var statusArr = {add:"added",edit:"updated",delete:"deleted"};
    var userData = '';
    if (type == 'edit'){
		$pass = $("#password").val();
		$confirm = $("#confirm").val();
        //userData = 'password='+$pass+'&confirm='+$confirm+'&action_type='+type;
        userData = $("#centry").serialize()+'&action_type='+type;
		
		$.ajax({
        type: 'POST',
        url: 'changepassaction.php',
        data: userData,
        success:function(msg){
			
			$('#example').DataTable().ajax.reload(null, false);
			
			
            if(msg == 'ok'){
                showSuccessMessageEdit();
				resetdata();
            }
			else if(msg == 'dontmatch'){
                alert('password doesnt match. please enter again.');
                resetdata();
				}
			else if(msg == 'err'){
                alert(msg);
                resetdata();
				}
			else if(msg == 'same'){
               alert('This password is same as your existing password.');
			   resetdata();
            }
        }
    });
     }
	
	else{
    }

}


function resetdata(){
	  $('#centry')[0].reset();
}
</script>

<?php include'navbar.php'?>
    <?php include'sidebar.php'?>
  <section class="content">
  <!--      <div class="container-fluid">
            <div class="block-header">
                <h2>Consignment Details</h2>
            </div>
        </div>
		 -->
		 <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2><i class="fa fa-fw fa-lock"></i>  Change Password</h2>
                         
                        </div>
                        <div class="body">
                            <form method="POST" id="centry">
							
									 <div class="col-md-6">
									<label class="form-label">New Password</label><span style="color:red"> * </span>
                                      
                                            <input type="text" class="form-control input-md"  name="password" id="password" placeholder="Enter password.."/>	
                                  
										<span class="help-block" id="password_help" style="display:none;font-size:12px;color:red;">Please Enter password</span>
                                    </div>
								
								
								
							 
								
							 <div class="col-md-6">
								<label class="form-label">Confirm Password</label><span style="color:red"> * </span>  
                                        <div class="form-line">
									
                                            <input type="text" class="form-control input-md"  name="confirm" id="confirm" placeholder="Confirm Password..">
                                        </div>
											<span class="help-block" id="confirm_help" style="display:none;font-size:12px;color:red;">Please Enter State.</span>
                                    </div>
								
							<input type="hidden"  name="uid" id="uid" value="<?php echo $_SESSION['user_id'] ?>">
								 <div class="row clearfix js-sweetalert">
									
									
                                 <button type="button" id="btnSave" class="btn btn-info" style="margin-left:20px;" onclick="vaction('edit');" ><i class="material-icons">save</i><span class="icon-name">Save</span></button>
								<button type="button" class="btn btn-danger" style="; margin-left:20px;"  onclick="resetdata();"><i class="material-icons">repeat</i><span class="icon-name">Reset</span></button>
								 
                            </form>
                        </div>
						</div>
                    </div>
                </div>
				
            </div>
			

    </section>



 <?php include'footer.php'?>
 <script>
 
//ADD FOCUS ON SIDEBAR
	$("#sidebar_ul").find(".active").removeClass("active");
	$("#sidebar_change_pass").addClass("active").focus();	
	
$( document ).ready(function() {

});

</script>

<script>

function showSuccessMessage() {
    swal({
        title: "OK!",
        text: "Consignment Details has been added successfully!",
        type: "success",

        closeOnConfirm: false
    });
}

function showSuccessMessageEdit() {
    swal({
        title: "OK!",
        text: "password has been Updated successfully!",
        type: "success",

        closeOnConfirm: false
    });
}

function showConfirmMessage(cid) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
		
        swal("Deleted!", "Your imaginary file has been deleted.", "success");
		vaction('delete',cid);
    });
}
</script>
</body>

</html>