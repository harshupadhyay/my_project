<?php
include 'Config/DB.php';
$db = new DB();
$tblName = 'service_provider';
if(isset($_POST['action_type']) && !empty($_POST['action_type'])){
    if($_POST['action_type'] == 'add'){
		if(!empty($_POST['iSubCategoryID'])){
            $userData = array(
				'iSubCategoryID' => $_POST['iSubCategoryID'],
				'vName' => $_POST['vName'],
				'vAddress' => $_POST['vAddress'],
				'dLat' => $_POST['dLat'],
				'dLong' => $_POST['dLong'],
				'dRating' => $_POST['dRating'],
				'isStatus' => $_POST['isStatus'],
				'vProfilePic' => ''				
            );
            $insert = $db->insert($tblName,$userData);
            echo $insert?'ok':'err';
           
			if(isset($_FILES["vProfilePic"]["name"])){
				$image_id=$insert;
				$image_name = time().'_'.$_FILES["vProfilePic"]["name"];
				
				$folder = "images/category/".$image_name;
				
			
				move_uploaded_file($_FILES["vProfilePic"]["tmp_name"], "$folder");
				$imgData = array(
					'vProfilePic' => $image_name,
				);
				$condition = array('iServiceProviderID' => $image_id);
				$update = $db->update($tblName,$imgData,$condition);
				echo $update?"ok":"err";
			}
		}
    }elseif($_POST['action_type'] == 'edit'){
        if(!empty($_POST['iServiceProviderID'])){
            $userData = array(
				'iSubCategoryID' => $_POST['iSubCategoryID'],
				'vName' => $_POST['vName'],
				'vAddress' => $_POST['vAddress'],
				'dLat' => $_POST['dLat'],
				'dLong' => $_POST['dLong'],
				'dRating' => $_POST['dRating'],
				'isStatus' => $_POST['isStatus'],
			
            );
            $condition = array('iServiceProviderID' => $_POST['iServiceProviderID']);
            $update = $db->update($tblName,$userData,$condition);
			// echo $update?"ok":"err";
			
			    if($_FILES["vProfilePic"]["error"]==0){
				$image_name = time().'_'.$_FILES["vProfilePic"]["name"];
				$folder = "images/category/".$image_name;
				move_uploaded_file($_FILES["vProfilePic"]["tmp_name"], "$folder");
				$imgData = array(
						'vProfilePic' => $image_name,
					);
				$condition = array('iServiceProviderID' => $_POST['iServiceProviderID']);
				$update = $db->update($tblName,$imgData,$condition);
			
			}
			echo 'ok';
		}
    }elseif($_POST['action_type'] == 'delete'){
        if(!empty($_POST['iServiceProviderID'])){
            $condition = array('iServiceProviderID' => $_POST['iServiceProviderID']);
            $delete = $db->delete($tblName,$condition);
            echo $delete?'ok':'err';
        }
	}elseif($_POST['action_type'] == 'data'){
        $conditions['where'] = array('iServiceProviderID'=>$_POST['iServiceProviderID']);
        $conditions['return_type'] = 'single';
        $user = $db->getRows($tblName,$conditions);
        echo json_encode($user);
 	}
}

    exit;
?>