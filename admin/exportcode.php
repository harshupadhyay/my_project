<?php include 'header.php';
if($_SESSION['user_role'] != 'admin')
	{
	header('Location: index.php');
	exit(0);	
}
include 'Config/DB.php';
$db = new DB();?>
<script>

//for table data 
function editEntry(id)
{   
	$.ajax({
	type: 'POST',
	dataType:'JSON',
	url: 'sawmillaction.php',
	data: 'action_type=data&sid='+id,
	success:function(data){
	$('#sid').val(data.sid);	
	$('#sawmill_name').val(data.sawmill_name);	
	
	
	}
});
}

function clearSearch(id)
{   
	 $('#searchFilter').parent().find("#reservation").val("");
	 $('#searchFilter').parent().find("#consignment_id").val("");
	 $('#searchFilter').parent().find("#lot_no_out").val("");
	 $('#searchFilter').parent().find("#sawmill_id").val("");
	 //$('#example').DataTable().column().draw();
}


	
//for rack master
</script>
<script>
function vaction(type,id){
    id = (typeof id == "undefined")?'':id;
    var userData = '';
 
 if (type == 'add') 
 {
    var userData = 	$(".sawmill_entry").serialize()+'&action_type='+type;
    $.ajax({
        url: 'sawmillaction.php',
        type: 'POST',
        data: userData,
        async: false,
        success: function (data) {
			if(data == 'ok'){
			$('#example').DataTable().ajax.reload(null, false);
			showSuccesMessage();
			resetdata();
        }
        }
    });

	
    }
	
else if (type == 'totalFilter') 
 {
	var logData = $("#search_entry").serialize()+"&action_type="+type;
    $.ajax({
        url: 'issuedlogsummaryaction.php',
        type: 'POST',
        data: logData,
        //async: true,
        success: function (datat) {
			var data = JSON.parse(datat);
			$('#filTotalNoOfLogs').text(data.totalNoOfLogs);	
			$('#filRemainingLogs').text(data.remainingLogs);	
			$('#filTotalPurNetCbm').text(parseFloat(data.totalPurNetCbm).toFixed(3));	
			$('#filTotalNetCbm').text(parseFloat(data.totalNetCbm).toFixed(3));	
			$('#filTotalJasCbm').text(parseFloat(data.totalJasCbm).toFixed(3));	
			$('#filTotalNetCft').text(parseFloat(data.totalNetCft).toFixed(2));	
			$('#filTotalPurNetCft').text(parseFloat(data.totalPurNetCft).toFixed(2));	
			$('#filcbmDiff').text(parseFloat(data.cbmDiff).toFixed(2));	
			$('#filcbmGainPer').text(parseFloat(data.cbmGainPer).toFixed(2)+"%");
		
        }
    });
    return false; 
}
	
else if (type == 'getLotNo') 
 {
	 if(id == ''){id=0}
	 var logData = "action_type="+type+"&consignment_id="+id;
    $.ajax({
        url: 'issuedlogsummaryaction.php',
        type: 'POST',
        data: logData,
        async: true,
        success: function (data) {
			var options='<option value="">Select Lot No</option>';
			var json = $.parseJSON(data);
			$.each(json,function(i,item){
				options += "<option value=\""+json[i].lot_no+"\">"+json[i].lot_no+"</option>"
			});
			$("#lot_no_out").html(options);
        }
    });
    return false; 
}


	else if (type == 'edit'){
        var userData = $(".sawmill_entry").serialize()+'&action_type='+type;
		
		$.ajax({
			url: 'sawmillaction.php',
			type: 'POST',
			data: userData,
			async: false,
			success: function (data) {
			if(data == 'ok'){
				$('#example').DataTable().ajax.reload(null, false);
				showSuccesMessage();
				resetdata();
				$(btnUpdate).hide();
				$(btnSave).show();
			}
        }
    });
     }
	 
	 
	else if (type == 'delete'){
     
		userData ='action_type=delete&sid='+id; 
		$.ajax({	
			url: 'sawmillaction.php',
			type: 'POST',
			data: userData,
			async: false,
			success: function (data) {
				
				$('#example').DataTable().ajax.reload(null, false);
        },
    });
				
    }    
 }
function resetdata(){
	$('.sawmill_entry')[0].reset();
}

function prevent(e) {
   e.preventDefault();
}
</script>

<?php include 'navbar.php'?>
    <?php include 'sidebar.php'?>
  <section class="content">
 
			
			 <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               <i class="fa fa-table" aria-hidden="true"></i>  Issued Log Summary
							</h2>
						</div>
							
                        <div class="body">
						
					<!--DATE RANGE PICKER-->

						<div class="row" style="margin-bottom:10px;border-bottom: 1px solid rgba(204, 204, 204, 0.35);" >
						<form method="POST" action="exportdiawisereport.php" id="search_entry">
						<div class="col-md-4">
							<b>Search by Date:</b>
							
						<!--	<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>-->
							<div class="form-line">
							<input type="text" class="form-control pull-right" name="date" id="reservation">
							</div>
						<!--	</div>-->
		
						<!--		<div>
							<button type="button" id="clearRange1" name="clearRange1" class="btn bg-red btn-flat pull-right" onclick="clearDateRange1();"><i class="fa fa-fw fa-undo"></i>  Clear</button>
						</div>	-->
						</div>
						
						
									<div class="col-md-4">
									<label class="form-label">Consignment No.</label>
                                        <div class="form-line">
											<select class="form-control show-tick" name="consignment_id" id="consignment_id"><option value="">Select Consignment No.</option><?php $users = $db->getRows('consignment_master'); if(!empty($users)): $count = 0; foreach($users as $user): $count++;?>
											<option value="<?php echo $user['cid']; ?>"><?php echo $user['consignment_no']; ?></option><?php endforeach; else: ?> <?php endif; ?>
											</select>
                                        </div>
									</div>
							
							
							<div class="col-sm-4">
									<label for="lot_no_out">Lot No</label>
								<div class="form-line">
                                    <select class="form-control show-tick" name="lot_no_out" id="lot_no_out">
												<option value="">Select Lot No.</option>	
                                    </select>
                                </div>
							</div>
							
							<div class="col-sm-4">
									<label for="sawmill_id">Sawmill Name</label>
									<div class="form-line">
                                    <select class="form-control show-tick" name="sawmill_id" id="sawmill_id">
										<option value="">Select Sawmill Name.</option>
                                       <?php $users = $db->getRows('sawmill_master');
										if(!empty($users)): $count = 0; foreach($users as $user): $count++;?><option value="<?php echo $user['sid']; ?>"><?php echo $user['sawmill_name']; ?></option><?php endforeach; else: ?> <?php endif; ?>
                                    </select>
									</div>
							</div>
							
							<div class="col-sm-8">
							<br/>
						<!--	<button type="button" id="btnSave" class="btn bg-blue btn-md waves-effect"  onclick="">
								 <i class="fa fa-search" aria-hidden="true"></i>  Search</button>-->
								 
								 <button type="button" id="clearSearchBtn" class="btn bg-red btn-md waves-effect"  onclick="">
								 <i class="material-icons">clear</i><span class="icon-name">Clear</span></button>
								 
								  <button type="submit" id="btnSave" class="btn bg-green btn-md waves-effect pull-right" style="margin-left:20px;"><i class="material-icons">file_download</i><span class="icon-name">Export DIA Report - Excel</span></button>
							</div>
							</form>
							
							
						</div>
					
					<!--  FILTERED DATA  -->
						<div class="row" id="searchFilter">
						<div class="col-sm-3 col-md-3" style="margin-bottom: 5px;">
								<div class="bg-pink" style="padding:10px 0;text-align: center;margin-bottom: 10px;">
									<div style="font-size: 17px;">Total Logs: <span id="filTotalNoOfLogs">0</span></div>
								</div>
						</div>


						<div class="col-sm-3 col-md-3" style="margin-bottom: 5px;">
								<div class="bg-indigo" style="padding:10px 0;text-align: center;margin-bottom: 10px;">
									<div style="font-size: 17px;">Remaining Logs: <span id="filRemainingLogs">0</span></div>
								</div>
						</div>
						
						<div class="col-sm-3 col-md-3" style="margin-bottom: 5px;">
								<div class="bg-purple" style="padding:10px 0;text-align: center;margin-bottom: 10px;">
								<div style="font-size: 17px;">Total Pur. Net CBM : <span id="filTotalPurNetCbm">0</span></div>
								</div>
						</div>
						
						<div class="col-sm-3 col-md-3" style="margin-bottom: 5px;">
								<div class="bg-deep-purple" style="padding:10px 0;text-align: center;margin-bottom: 10px;">
								<div style="font-size: 17px;">Total Net CBM : <span id="filTotalNetCbm">0</span></div>
								</div>
						</div>
						
						<div class="col-sm-3 col-md-3" style="margin-bottom: 5px;">
								<div class="bg-green" style="padding:10px 0;text-align: center;margin-bottom: 10px;">
								<div style="font-size: 17px;">Total JAS CBM : <span id="filTotalJasCbm">0</span></div>
								</div>
						</div>
						
						<div class="col-sm-3 col-md-3" style="margin-bottom: 5px;">
								<div class="bg-deep-orange" style="padding:10px 0;text-align: center;margin-bottom: 10px;">
								<div style="font-size: 17px;">Total CBM Diff : <span id="filcbmDiff">0</span></div>
								</div>
						</div>
						
						<div class="col-sm-3 col-md-3" style="margin-bottom: 5px;">
								<div class="bg-orange" style="padding:10px 0;text-align: center;margin-bottom: 10px;">
								<div style="font-size: 17px;">CBM Gain (%): <span id="filcbmGainPer">0</span></div>
								</div>
						</div>
				</div>

                            <div class="table-responsive">
                                <table id="example" class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>S.R</th>
                                            <th>Log No</th>
                                            <th>Vessel Name</th>
                                            <th>Lot No.</th>
                                            <th>Sawmill</th>
                                            <th>Length(Pur)</th>  
                                            <th>Dia(Pur)</th>  
											<th>CBM(Pur)</th>
											<th>Length(Machine)</th>  
                                            <th>Girth(Machine)</th>  
											<th>Indian CBM</th>
											<th>CBM(JAS)</th>
											<th>CBM Diff.</th>
											<th>CBM Gain(%)</th>
											<th>Issued Date</th> 
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </section>



 <?php include'footer.php'?>
 <!-- daterangepicker -->
<script src="plugins/moment/min/moment.min.js"></script>
<script src="plugins/bootstrap-daterangepicker/daterangepicker.js"></script>



 <script>
 


function toChangeDateFormat(dateStr)
{
    var parts = dateStr.split("-");
    //return new Date(parts[2], parts[1], parts[0]);
	var dt=  parts[2]+"/"+parts[1]+"/"+parts[0];
	return dt;
}
	
	
$( document ).ready(function() {
//ADD FOCUS ON SIDEBAR
	$("#sidebar_ul").find(".active").removeClass("active");
	$("#sidebar_issued_log_summary").addClass("active").focus();
	
	 vaction('totalFilter');
var otable = $('#example').DataTable( {
	dom: 'Bfrtip',
	buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
"ajax": "issuedlogsummarydata.php",
"bPaginate":true,
"bProcessing": true,


"dataSrc":"data",
 "columns": [
{ mData: 'lid' },
{ mData: 'log_no' },
{ mData: 'consignment_no' },
{ mData: 'lot_no' },
{ mData: 'sawmill_name' } ,
{ mData: 'length' } ,
{ mData: 'diameter' } ,
{ mData: 'pur_cbm' } ,

{ mData: 'msr_length' } ,
{ mData: 'msr_diameter' } ,
{ mData: 'msr_cbm' } ,
{ mData: 'jas_cbm' } ,

{ mData: 'difference' } ,
{ mData: 'diff_perc' } ,

{ mData: 'issued_date' },

],
});

//Search Consignment
$("#consignment_id").on('keyup change', function() {
	vaction('totalFilter');
	$cons = $("#consignment_id option:selected").text();
  
	if($("#consignment_id").val() != ""){
		otable.column(2).search("^"+$cons+"$",true, false, true).draw();
	}
	else{
		otable.column(2).search("").draw();
	}
});

//Search Lot No
$("#lot_no_out").on('keyup change', function() {
	vaction('totalFilter');
	$lot = $("#lot_no_out").val();
    
	if($("#lot_no_out").val() != ""){
		otable.column(3).search("^"+$lot+"$",true, false, true).draw();
		
	}
	else{
		otable.column(3).search("").draw();
	}
});

//Search Sawmill Name
$("#sawmill_id").on('keyup change', function() {
	vaction('totalFilter');
	$saw = $("#sawmill_id option:selected").text();
	
	if($("#sawmill_id").val() != ""){
		otable.column(4).search("^"+$saw+"$",true, false, true).draw();
	}
	else{
		otable.column(4).search("").draw();
	}
});


//DATE RANGE SEARCH
$('#reservation').change( function() { otable.draw(); } );

//CLEAR SEARCH
$('#clearSearchBtn').on('keyup click', function() { 
	clearSearch();
	otable.column(2).search("").draw();
	otable.column(3).search("").draw();
	otable.column(4).search("").draw(); 
	vaction('totalFilter');
});

$.fn.dataTableExt.afnFiltering.push(
    function( oSettings, aData, iDataIndex ) {
		if ( oSettings.nTable.id === 'example' ) {
		if($('#reservation').val() != ""){
        var iFini = $('#reservation').data('daterangepicker').startDate.format('DD-MM-YYYY');
        var iFfin = $('#reservation').data('daterangepicker').endDate.format('DD-MM-YYYY');
		
	    var iStartDateCol = 14;
        var iEndDateCol = 14;
		
        iFini=iFini.substring(6,10) + iFini.substring(3,5)+ iFini.substring(0,2);
        iFfin=iFfin.substring(6,10) + iFfin.substring(3,5)+ iFfin.substring(0,2);
 
 
        var datofini=aData[iStartDateCol].substring(6,10) + aData[iStartDateCol].substring(3,5)+ aData[iStartDateCol].substring(0,2);
        var datoffin=aData[iEndDateCol].substring(6,10) + aData[iEndDateCol].substring(3,5)+ aData[iEndDateCol].substring(0,2);
 
        if ( iFini === "" && iFfin === "" )
        {
            return true;
        }
        else if ( iFini <= datofini && iFfin === "")
        {
            return true;
        }
        else if ( iFfin >= datoffin && iFini === "")
        {
            return true;
        }
        else if (iFini <= datofini && iFfin >= datoffin)
        {
            return true;
        }
        return false;
    }else{return true;}
	}
	else{return true;}
    }
);



});

$(function() {
    $("#reservation").daterangepicker({
        locale: {
            format: 'DD-MMM-YYYY',
        }
    });
	
$('#reservation').val("");
 $('#reservation').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
	  vaction('totalFilter');
  });
  
  $("#reservation").on('cancel.daterangepicker', function(ev, picker) {
      clearDateRange();
	  vaction('totalFilter');
  });
			
})	

function clearDateRange(){
	
	$('#reservation').val("");
	$('#example').DataTable().column( '0:visible' ).order( 'desc' ).draw();
}


</script>
										
<script>

function showStopMessage() {
    swal({
        title: "Stop!",
        text: "Dont cut this log.!",
        type: "error",

        closeOnConfirm: false
    });
}

function showSuccessMessage() {
    swal({
        title: "Success!",
        text: "Sawmill has been Added succesfully.!",
        type: "success",

        closeOnConfirm: false
    });
}

function showWarningMessage() {
    swal({
        title: "oops!",
        text: "Something went wrong, please check the values and try again.!",
        type: "warning",

        closeOnConfirm: false
    });
}

function showConfirmMessage(lid) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
		vaction('delete',lid);
        swal("Deleted!", "Sawmill data has been deleted successfully.", "success");
    });
}

$("#consignment_id" ).change(function() {
	vaction("getLotNo",$(this).val());
});

</script>
</body>

</html>