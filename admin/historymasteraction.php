<?php

include 'Config/DB.php';

$db = new DB();

$tblName = 'history_master';

if(isset($_POST['action_type']) && !empty($_POST['action_type'])){
    if($_POST['action_type'] == 'add'){
		if(!empty($_POST['vTitleName'])){
            $userData = array(
				'vTitleName' => $_POST['vTitleName'],
				'isStatus' => $_POST['isStatus'],
				'tDescription' => $_POST['tDescription'],
				
            );

            $insert = $db->insert($tblName,$userData);
            echo $insert?'ok':'err';
            
			if(isset($_FILES["vImageName"]["name"])){
				$image_id=$insert;
				$image_name = time().'_'.$_FILES["vImageName"]["name"];
				$folder = "images/history/".$image_name;
				move_uploaded_file($_FILES["vImageName"]["tmp_name"], "$folder");
				$imgData = array(
					'vImageName' => $image_name,
				);

				$condition = array('iHistoryID' => $image_id);
				$update = $db->update($tblName,$imgData,$condition);
			}
		}
    }elseif($_POST['action_type'] == 'edit'){
    	if(!empty($_POST['iHistoryID'])){
            $userData = array(
				'vTitleName' => $_POST['vTitleName'],
				'isStatus' => $_POST['isStatus'],
				'tDescription' => $_POST['tDescription'],
            );

            $condition = array('iHistoryID' => $_POST['iHistoryID']);
            $update = $db->update($tblName,$userData,$condition);
			
			 if($_FILES["vImageName"]["error"] == 0){
				$image_name = time().'_'.$_FILES["vImageName"]["name"];

				$folder = "images/history/".$image_name;

				move_uploaded_file($_FILES["vImageName"]["tmp_name"], "$folder");
				$imgData = array(
						'vImageName' => $image_name,
					);
				$condition = array('iHistoryID' => $_POST['iHistoryID']);
				$update = $db->update($tblName,$imgData,$condition);
			}
			echo 'ok';
		}
    }elseif($_POST['action_type'] == 'delete'){
        if(!empty($_POST['iHistoryID'])){
            $condition = array('iHistoryID' => $_POST['iHistoryID']);
            $delete = $db->delete($tblName,$condition);
            echo $delete?'ok':'err';
        }
	}elseif($_POST['action_type'] == 'data'){
        $conditions['where'] = array('iHistoryID'=>$_POST['iHistoryID']);
        $conditions['return_type'] = 'single';
        $user = $db->getRows($tblName,$conditions);
        echo json_encode($user);
 	}
}

    exit;

 

?>







