<?php

include 'Config/DB.php';

$db = new DB();

$tblName = 'tbl_notification';

if(isset($_POST['action_type']) && !empty($_POST['action_type'])){
    if($_POST['action_type'] == 'add'){
		if(!empty($_POST['vNotificationName'])){
            $userData = array(
				'vNotificationName' => $_POST['vNotificationName'],
				'eNotificationStatus' => $_POST['eNotificationStatus'],
				
            );

            $insert = $db->insert($tblName,$userData);
            // echo $insert?'ok':'err';
            
			if(isset($_FILES["vImageName"]["name"])){
				$image_id=$insert;
				$image_name = time().'_'.$_FILES["vImageName"]["name"];
				$folder = "images/notification/".$image_name;
			
				move_uploaded_file($_FILES["vImageName"]["tmp_name"], $folder);
				$imgData = array(
					'vImageName' => $image_name,
				);

				$condition = array('iNotificationID' => $image_id);
				$update = $db->update($tblName,$imgData,$condition);
				echo $update?"ok":"err";
			}
		}
    }elseif($_POST['action_type'] == 'edit'){
        if(!empty($_POST['iNotificationID'])){
            $userData = array(
				'vNotificationName' => $_POST['vNotificationName'],
				'eNotificationStatus' => $_POST['eNotificationStatus'],
            );

            $condition = array('iNotificationID' => $_POST['iNotificationID']);
            $update = $db->update($tblName,$userData,$condition);
			
			
			if($_FILES["vImageName"]["error"]== 0){
				$image_name = time().'_'.$_FILES["vImageName"]["name"];

				$folder = "images/notification/".$image_name;
	
				move_uploaded_file($_FILES["vImageName"]["tmp_name"], "$folder");
				$imgData = array(
									'vImageName' => $image_name,
								);
				$condition = array('iNotificationID' => $_POST['iNotificationID']);
				$update = $db->update($tblName,$imgData,$condition);
			}
			echo $update?"ok":"err";
		}
		
    }elseif($_POST['action_type'] == 'delete'){
        if(!empty($_POST['iNotificationID'])){
            $condition = array('iNotificationID' => $_POST['iNotificationID']);
            $delete = $db->delete($tblName,$condition);
            echo $delete?'ok':'err';
        }
	}elseif($_POST['action_type'] == 'data'){
        $conditions['where'] = array('iNotificationID'=>$_POST['iNotificationID']);
        $conditions['return_type'] = 'single';
        $user = $db->getRows($tblName,$conditions);
        echo json_encode($user);
 	}
}

    exit;

 

?>







