    <section>

        <!-- Left Sidebar -->

        <aside id="leftsidebar" class="sidebar">

            <!-- User Info -->

            <div class="user-info">

                <div class="image">

                    <img src="images/user.png" width="48" height="48" alt="User" />

                </div>

                <div class="info-container">

                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['username'];?></div>

                    <div class="email"><?php echo $_SESSION['user_role'];?></div>

                    <div class="btn-group user-helper-dropdown">

                       

                        <ul class="dropdown-menu pull-right">

                         <!--   <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>

                            <li role="seperator" class="divider"></li>

                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>

                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>

                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>

                            <li role="seperator" class="divider"></li>-->

                            <li><a href="index.php"><i class="material-icons">input</i>Sign Out</a></li>

                        </ul>	

                    </div>

                </div>

            </div>

            <!-- #User Info -->

            <!-- Menu -->

            <div class="menu">

		

                <ul class="list" id="sidebar_ul">

				<?php if($_SESSION['user_role'] == 'dispatcher') {?>

					<li class="header">WORK</li>

					<li id="sidebar_workermaster" class="active">

                        <a href="workermaster.php">

                            <i class="material-icons">settings</i>

                            <span>Pending Work</span>

                        </a>

                    </li>

				<?php }?>

				

			<?php if($_SESSION['user_role'] == 'admin') {?>

				

				

                  <!--     <li id="sidebar_consignment_master" class="active">

                        <a href="consignmentmaster.php">

                            <i class="material-icons">directions_boat</i>

                            <span>Consignments</span>

                        </a>

                    </li>

					

					 <li id="sidebar_pine_master">

                        <a href="pinemaster.php">

                            <i class="material-icons">clear_all</i>

                            <span>PineWood Master</span>

                        </a>

                    </li>

					

					<li id="sidebar_hardwood_master">

                        <a href="hardwoodmaster.php">

                            <i class="material-icons">list</i>

                            <span>HardWood Master</span>

                        </a>

                    </li>

					

					<li id="sidebar_log_search">

                        <a href="logsearch.php">

                            <i class="material-icons">list</i>

                            <span>Log Search</span>

                        </a>

                    </li>

				<!--

					 <li id="sidebar_log_check">

                        <a href="logcheck.php">

                            <i class="material-icons">playlist_add_check</i>

                            <span>Check Logs</span>

                        </a>

                    </li>-->

				<?php } ?>



				

				<?php if($_SESSION['user_role'] == 'admin') {?>

		<!--			<li id="sidebar_issued_log_summary">

                        <a href="issuedlogsummary.php">

                            <i class="material-icons">assignment_turned_in</i>

                            <span>Issued Log Summary</span>

                        </a>

                    </li>

					

					<li id="sidebar_round_logs">

                        <a href="roundlogs.php">

                            <i class="material-icons">launch</i>

                            <span>Direct Log Sale</span>

                        </a>

                    </li>

					

					<li id="sidebar_nontally_logs">

                        <a href="nontally.php">

                            <i class="material-icons">more</i>

                            <span>Non Tally</span>

                        </a>

                    </li> -->

				<?php } ?>

					<!--

					<li id="sidebar_log_measurement">

                        <a href="logmeasurement.php">

                            <i class="material-icons">assignment</i>

                            <span>log Measurement Summary</span>

                        </a>

                    </li>

					-->

				<?php if($_SESSION['user_role'] == 'admin' or $_SESSION['user_role'] == 'supervisor' ) {?>

			<!--	<li class="header">STOCK</li>

					 <li id="sidebar_stock_master">

                        <a href="stockmaster.php">

                            <i class="material-icons">archive</i>

                            <span>Stock Master</span>

                        </a>

                    </li>-->

					<?php } ?>

					

				<?php if($_SESSION['user_role'] == 'admin') {?>

			<!--		<li id="sidebar_stock_summary">

                        <a href="goodssummary.php">

                            <i class="material-icons">storage</i>

                            <span>Stock Summary</span>

                        </a>

                    </li>

					-->

				<?php } ?>

					

			<?php if($_SESSION['user_role'] == 'admin' or $_SESSION['user_role'] == 'supervisor') {?>

		<!--	<li class="header">LOADING</li>

			<!-- <li id="sidebar_dispatch_details">

                        <a href="dispatchdetails.php">

                            <i class="material-icons">local_shipping</i>

                            <span>Dispatch Details</span>

                        </a>

                    </li> -->

					

			<!--	<li id="sidebar_loading_master">

                        <a href="loadingmaster.php">

                            <i class="material-icons">local_shipping</i>

                            <span>Loading Master</span>

                        </a>

                    </li>

					

					<li id="sidebar_loading_report">

                        <a href="loadingreport.php">

                            <i class="material-icons">pie_chart</i>

                            <span>Loading Report</span>

                        </a>

                    </li>-->

				<?php } ?>

				<?php if($_SESSION['user_role'] == 'admin' or $_SESSION['user_role'] == 'supervisor') {?>

				<!--	<li id="sidebar_dispatch_summary">

                        <a href="dispatchsummary.php">

                            <i class="material-icons">near_me</i>

                            <span>Dispatch Summary</span>

                        </a>

                    </li>-->

				<!--	

				<li id="sidebar_dispatch_summary">

					<a href="dispatchsummary.php">

						<i class="material-icons">local_shipping</i>

						<span>Dispatch Summary</span>

					</a>

				</li>

				-->	

			<!--	 <li class="header">MASTERS</li>

					<li id="sidebar_sawmill_master">

                        <a href="sawmillmaster.php">

                            <i class="material-icons">brightness_high</i>

                            <span>Sawmill Master</span>

                        </a>

                    </li> -->

				<!--	<li id="sidebar_lot_master">

                        <a href="lotmaster.php">

                            <i class="material-icons">reorder</i>

                            <span>Lot Master</span>

                        </a>

                    </li>

					-->

<!--					

					<li id="sidebar_category_master">

                        <a href="categorymaster.php">

                            <i class="material-icons">add_box</i>

                            <span>Category Master</span>

                        </a>

                    </li> 

				-->

				<?php } ?>

				<?php if($_SESSION['user_role'] == 'admin') {?>

			<!--	  <li class="header">ACCOUNTS</li>

					<li id="sidebar_supervisor_master">

                        <a href="supervisormaster.php">

                            <i class="material-icons">supervisor_account</i>

                            <span>Supervisor Master</span>

                        </a>

                    </li>	

					<!--

					<li id="sidebar_party_master">

                        <a href="partymaster.php">

                            <i class="material-icons">person_add</i>

                            <span>Party Master</span>

                        </a>

                    </li>

					-->

					<!--

					<li class="header">REPORTS</li>

					<li id="sidebar_stock_report">

                        <a href="stockreport.php">

                            <i class="material-icons">insert_chart</i>

                            <span>Stock Report</span>

                        </a>

                    </li>

					

					<li id="sidebar_sawmill_report">

                        <a href="sawmillreport.php">

                            <i class="material-icons">pie_chart</i>

                            <span>Sawmill Wise Report</span>

                        </a>

                    </li>

					

					<li id="sidebar_supervisor_report">

                        <a href="supervisorreport.php">

                            <i class="material-icons">assignment_ind</i>

                            <span>Supervisor Report</span>

                        </a>

                    </li>

				-->

				<?php } ?>

				

				<?php if($_SESSION['user_role'] == 'admin' or $_SESSION['user_role'] == 'supervisor') {?>

					<li class="header">UTILITY</li>

					<li id="sidebar_change_pass">

                        <a href="changepass.php">

                            <i class="material-icons">lock_outline</i>

                            <span>Change Password</span>

                        </a>

                    </li>

				<?php } ?>

				

					<?php if($_SESSION['user_role'] == 'admin' or $_SESSION['user_role'] == 'supervisor') {?>

					<li class="header">MASTERS</li>

					<li id="sidebar_category_master">

                        <a href="categorymaster.php">

                            <i class="material-icons">collections_bookmark</i>

                            <span>Category Master</span>

                        </a>

                    </li>	

					<!--	<li id="sidebar_categoryimage_master">

                        <a href="categoryimagemaster.php">

                            <i class="material-icons">add_a_photo</i>

                            <span>Categoryimage Master</span>

                        </a>

                    </li> 	-->
	
			
					
					
					 <li id="sidebar_sub_master">

                        <a href="subcategorymaster.php">

                            <i class="material-icons">add_to_photos</i>

                            <span>Subcategory Master</span>

                        </a>

                    </li> 
					
					<li id="sidebar_subimg_master">

                        <a href="subcategoryimagemaster.php">

                            <i class="material-icons">photo_library</i>

                            <span>Subcategoryimage Master</span>

                        </a>

                    </li>
				
					
							<li id="sidebar_job_master">

                        <a href="magazinemaster.php">

                            <i class="material-icons">work</i>

                            <span>Magazine Master</span>

                        </a>

                    </li>	
					
					
					<li id="sidebar_product_ofthe_day">

                        <a href="productoftheday.php">

                            <i class="material-icons">build</i>

                            <span>Product Of the Day</span>

                        </a>

                    </li>
				
					
					<li id="sidebar_future_product">

                        <a href="futureproduct.php">

                            <i class="material-icons">build</i>

                            <span>Future Products</span>

                        </a>

                    </li>
					
					
					<li id="sidebar_notification">

                        <a href="notificationmaster.php">

                            <i class="material-icons">build</i>

                            <span>Notifications</span>

                        </a>

                    </li>
					
					
					<li id="sidebar_spc_master">

                        <a href="serviceprovidercategorymaster.php">

                            <i class="material-icons">build</i>

                            <span>Service Provider Category</span>

                        </a>

                    </li>

					

					<li id="sidebar_sp_master">

                        <a href="serviceprovidermaster.php">

                            <i class="material-icons">face</i>

                            <span>Service Provider</span>

                        </a>

                    </li>

					<li id="sidebar_poll_master">

                        <a href="pollmaster.php">

                            <i class="material-icons">equalizer</i>

                            <span>Poll Master</span>

                        </a>

                    </li>
                    	<li id="sidebar_history_master">

                        <a href="historymaster.php">

                            <i class="material-icons">history</i>

                            <span>History Master</span>

                        </a>

                    </li>
					
					<li id="sidebar_advertise_master_full">

                        <a href="advertisemaster.php">

                            <i class="material-icons">pages</i>

                            <span>Advertise Full</span>

                        </a>

                    </li>
					
						<li id="sidebar_advertise_slide_master_slider">

                        <a href="advertisemasterslider.php">

                            <i class="material-icons">pages</i>

                            <span>Advertise Slider</span>

                        </a>

                    </li>
					<li id="sidebar_advertise_sp_master_slider">

                        <a href="advertisemastersp.php">

                            <i class="material-icons">pages</i>

                            <span>Advertise Sp</span>

                        </a>

                    </li>
					<li id="sidebar_Contacts_master">

                        <a href="contactsmaster.php">

                            <i class="material-icons">email</i>

                            <span>Contact Us </span>

                        </a>

                    </li>
					
					<li id="sidebar_aboutus_master">

                        <a href="aboutusmaster.php">

                            <i class="material-icons">contacts</i>

                            <span>About Us </span>

                        </a>

                    </li>
					<!--<li id="sidebar_user_master">

                        <a href="usermaster.php">

                            <i class="material-icons">people</i>

                            <span>User Master</span>

                        </a>

                    </li>-->

					

					

					

					<li id="sidebar_admin_master">

                        <a href="adminmaster.php">

                            <i class="material-icons">perm_identity</i>

                            <span>Admin Master</span>

                        </a>

                    </li>

					<!-- 

					<li id="sidebar_transporter_master">

                        <a href="transportermaster.php">

                            <i class="material-icons">category</i>

                            <span>Transporter Master</span>

                        </a>

                    </li>

				<?php } ?>

					

                 <!-- <li>

                        <a href="pages/typography.html">

                            <i class="material-icons">text_fields</i>

                            <span>Typography</span>

                        </a>

                    </li>

                    <li>

                        <a href="pages/helper-classes.html">

                            <i class="material-icons">layers</i>

                            <span>Helper Classes</span>

                        </a>

                    </li>

                    <li>

                        <a href="javascript:void(0);" class="menu-toggle">

                            <i class="material-icons">widgets</i>

                            <span>Widgets</span>

                        </a>

                        <ul class="ml-menu">

                            <li>

                                <a href="javascript:void(0);" class="menu-toggle">

                                    <span>Cards</span>

                                </a>

                                <ul class="ml-menu">

                                    <li>

                                        <a href="pages/widgets/cards/basic.html">Basic</a>

                                    </li>

                                    <li>

                                        <a href="pages/widgets/cards/colored.html">Colored</a>

                                    </li>

                                    <li>

                                        <a href="pages/widgets/cards/no-header.html">No Header</a>

                                    </li>

                                </ul>

                            </li>

                            <li>

                                <a href="javascript:void(0);" class="menu-toggle">

                                    <span>Infobox</span>

                                </a>

                                <ul class="ml-menu">

                                    <li>

                                        <a href="pages/widgets/infobox/infobox-1.html">Infobox-1</a>

                                    </li>

                                    <li>

                                        <a href="pages/widgets/infobox/infobox-2.html">Infobox-2</a>

                                    </li>

                                    <li>

                                        <a href="pages/widgets/infobox/infobox-3.html">Infobox-3</a>

                                    </li>

                                    <li>

                                        <a href="pages/widgets/infobox/infobox-4.html">Infobox-4</a>

                                    </li>

                                    <li>

                                        <a href="pages/widgets/infobox/infobox-5.html">Infobox-5</a>

                                    </li>

                                </ul>

                            </li>

                        </ul>

                    </li>

                    <li>

                        <a href="javascript:void(0);" class="menu-toggle">

                            <i class="material-icons">swap_calls</i>

                            <span>User Interface (UI)</span>

                        </a>

                        <ul class="ml-menu">

                            <li>

                                <a href="pages/ui/alerts.html">Alerts</a>

                            </li>

                            <li>

                                <a href="pages/ui/animations.html">Animations</a>

                            </li>

                            <li>

                                <a href="pages/ui/badges.html">Badges</a>

                            </li>



                            <li>

                                <a href="pages/ui/breadcrumbs.html">Breadcrumbs</a>

                            </li>

                            <li>

                                <a href="pages/ui/buttons.html">Buttons</a>

                            </li>

                            <li>

                                <a href="pages/ui/collapse.html">Collapse</a>

                            </li>

                            <li>

                                <a href="pages/ui/colors.html">Colors</a>

                            </li>

                            <li>

                                <a href="pages/ui/dialogs.html">Dialogs</a>

                            </li>

                            <li>

                                <a href="pages/ui/icons.html">Icons</a>

                            </li>

                            <li>

                                <a href="pages/ui/labels.html">Labels</a>

                            </li>

                            <li>

                                <a href="pages/ui/list-group.html">List Group</a>

                            </li>

                            <li>

                                <a href="pages/ui/media-object.html">Media Object</a>

                            </li>

                            <li>

                                <a href="pages/ui/modals.html">Modals</a>

                            </li>

                            <li>

                                <a href="pages/ui/notifications.html">Notifications</a>

                            </li>

                            <li>

                                <a href="pages/ui/pagination.html">Pagination</a>

                            </li>

                            <li>

                                <a href="pages/ui/preloaders.html">Preloaders</a>

                            </li>

                            <li>

                                <a href="pages/ui/progressbars.html">Progress Bars</a>

                            </li>

                            <li>

                                <a href="pages/ui/range-sliders.html">Range Sliders</a>

                            </li>

                            <li>

                                <a href="pages/ui/sortable-nestable.html">Sortable & Nestable</a>

                            </li>

                            <li>

                                <a href="pages/ui/tabs.html">Tabs</a>

                            </li>

                            <li>

                                <a href="pages/ui/thumbnails.html">Thumbnails</a>

                            </li>

                            <li>

                                <a href="pages/ui/tooltips-popovers.html">Tooltips & Popovers</a>

                            </li>

                            <li>

                                <a href="pages/ui/waves.html">Waves</a>

                            </li>

                        </ul>

                    </li>

                    <li>

                        <a href="javascript:void(0);" class="menu-toggle">

                            <i class="material-icons">assignment</i>

                            <span>Forms</span>

                        </a>

                        <ul class="ml-menu">

                            <li>

                                <a href="pages/forms/basic-form-elements.html">Basic Form Elements</a>

                            </li>

                            <li>

                                <a href="pages/forms/advanced-form-elements.html">Advanced Form Elements</a>

                            </li>

                            <li>

                                <a href="pages/forms/form-examples.html">Form Examples</a>

                            </li>

                            <li>

                                <a href="pages/forms/form-validation.html">Form Validation</a>

                            </li>

                            <li>

                                <a href="pages/forms/form-wizard.html">Form Wizard</a>

                            </li>

                            <li>

                                <a href="pages/forms/editors.html">Editors</a>

                            </li>

                        </ul>

                    </li>

                    <li>

                        <a href="javascript:void(0);" class="menu-toggle">

                            <i class="material-icons">view_list</i>

                            <span>Tables</span>

                        </a>

                        <ul class="ml-menu">

                            <li>

                                <a href="pages/tables/normal-tables.html">Normal Tables</a>

                            </li>

                            <li>

                                <a href="pages/tables/jquery-datatable.html">Jquery Datatables</a>

                            </li>

                            <li>

                                <a href="pages/tables/editable-table.html">Editable Tables</a>

                            </li>

                        </ul>

                    </li>

                    <li>

                        <a href="javascript:void(0);" class="menu-toggle">

                            <i class="material-icons">perm_media</i>

                            <span>Medias</span>

                        </a>

                        <ul class="ml-menu">

                            <li>

                                <a href="pages/medias/image-gallery.html">Image Gallery</a>

                            </li>

                            <li>

                                <a href="pages/medias/carousel.html">Carousel</a>

                            </li>

                        </ul>

                    </li>

                    <li>

                        <a href="javascript:void(0);" class="menu-toggle">

                            <i class="material-icons">pie_chart</i>

                            <span>Charts</span>

                        </a>

                        <ul class="ml-menu">

                            <li>

                                <a href="pages/charts/morris.html">Morris</a>

                            </li>

                            <li>

                                <a href="pages/charts/flot.html">Flot</a>

                            </li>

                            <li>

                                <a href="pages/charts/chartjs.html">ChartJS</a>

                            </li>

                            <li>

                                <a href="pages/charts/sparkline.html">Sparkline</a>

                            </li>

                            <li>

                                <a href="pages/charts/jquery-knob.html">Jquery Knob</a>

                            </li>

                        </ul>

                    </li>

                    <li>

                        <a href="javascript:void(0);" class="menu-toggle">

                            <i class="material-icons">content_copy</i>

                            <span>Example Pages</span>

                        </a>

                        <ul class="ml-menu">

                            <li>

                                <a href="pages/examples/sign-in.html">Sign In</a>

                            </li>

                            <li>

                                <a href="pages/examples/sign-up.html">Sign Up</a>

                            </li>

                            <li>

                                <a href="pages/examples/forgot-password.html">Forgot Password</a>

                            </li>

                            <li>

                                <a href="pages/examples/blank.html">Blank Page</a>

                            </li>

                            <li>

                                <a href="pages/examples/404.html">404 - Not Found</a>

                            </li>

                            <li>

                                <a href="pages/examples/500.html">500 - Server Error</a>

                            </li>

                        </ul>

                    </li>

                    <li>

                        <a href="javascript:void(0);" class="menu-toggle">

                            <i class="material-icons">map</i>

                            <span>Maps</span>

                        </a>

                        <ul class="ml-menu">

                            <li>

                                <a href="pages/maps/google.html">Google Map</a>

                            </li>

                            <li>

                                <a href="pages/maps/yandex.html">YandexMap</a>

                            </li>

                            <li>

                                <a href="pages/maps/jvectormap.html">jVectorMap</a>

                            </li>

                        </ul>

                    </li>

                    <li>

                        <a href="javascript:void(0);" class="menu-toggle">

                            <i class="material-icons">trending_down</i>

                            <span>Multi Level Menu</span>

                        </a>

                        <ul class="ml-menu">

                            <li>

                                <a href="javascript:void(0);">

                                    <span>Menu Item</span>

                                </a>

                            </li>

                            <li>

                                <a href="javascript:void(0);">

                                    <span>Menu Item - 2</span>

                                </a>

                            </li>

                            <li>

                                <a href="javascript:void(0);" class="menu-toggle">

                                    <span>Level - 2</span>

                                </a>

                                <ul class="ml-menu">

                                    <li>

                                        <a href="javascript:void(0);">

                                            <span>Menu Item</span>

                                        </a>

                                    </li>

                                    <li>

                                        <a href="javascript:void(0);" class="menu-toggle">

                                            <span>Level - 3</span>

                                        </a>

                                        <ul class="ml-menu">

                                            <li>

                                                <a href="javascript:void(0);">

                                                    <span>Level - 4</span>

                                                </a>

                                            </li>

                                        </ul>

                                    </li>

                                </ul>

                            </li>

                        </ul>

                    </li>

                    <li>

                        <a href="pages/changelogs.html">

                            <i class="material-icons">update</i>

                            <span>Changelogs</span>

                        </a>

                    </li>

                    <li class="header">LABELS</li>

                    <li>

                        <a href="javascript:void(0);">

                            <i class="material-icons col-red">donut_large</i>

                            <span>Important</span>

                        </a>

                    </li>

                    <li>

                        <a href="javascript:void(0);">

                            <i class="material-icons col-amber">donut_large</i>

                            <span>Warning</span>

                        </a>

                    </li>

                    <li>

                        <a href="javascript:void(0);">

                            <i class="material-icons col-light-blue">donut_large</i>

                            <span>Information</span>

                        </a>

                    </li>-->

                </ul>

            </div>

            <!-- #Menu -->

            <!-- Footer -->

            <div class="legal">

                <div class="copyright">

                    &copy; 2017 - 2018 <a href="javascript:void(0);">VIGNYAPAN</a>

					</div>

                <div class="version">

                    <b>Powered By: </b> <a href="www.yeshasoftware.com" target="_blank">Yesha Software</a>

                </div>

            </div>

            <!-- #Footer -->

        </aside>

	</section>