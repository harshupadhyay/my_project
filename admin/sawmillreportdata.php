<?php

 include 'Config/config.php';
 include 'Config/DB.php';
 $db = new DB();
		 
/* $resultset = mysqli_query($conn,"SELECT i.stock_id,sc.category_name,i.stock_name,sum(g.pcs) as inward_pcs, sum(g.cft) as inward_cft,sum(o.pcs) as outward_pcs,sum(o.cft) as outward_cft,(sum(g.pcs)-sum(o.pcs))as closing_pcs,(sum(g.cft)-sum(o.cft)) as closing_cft FROM (SELECT stock_id,stock_name from item_master group by stock_id) i Left JOIN (Select stock_id,stock_cat_id,cft,pcs from goods_master group by stock_id) g ON i.stock_id=g.stock_id  LEFT JOIN (Select stock_id,pcs,cft from outward group by stock_id) o ON i.stock_id=o.stock_id LEFT JOIN (SELECT * from stock_category) sc on g.stock_cat_id=sc.scid GROUP BY i.stock_id"); */

$resultset = $db->selectQuery('SELECT i.stock_id,s.sawmill_name,sc.category_name, i.stock_name, g.pcs AS inward_pcs,g.cft AS inward_cft, o.pcs AS outward_pcs, o.cft AS outward_cft, (g.pcs - o.pcs) AS closing_pcs, (g.cft- o.cft) AS closing_cft
 FROM
(SELECT
      item_master.stock_id,
      item_master.stock_name
    FROM
      item_master
    GROUP BY
      item_master.stock_id) i
  INNER JOIN (SELECT
	sawmill_id,
      goods_master.stock_id,
      goods_master.stock_cat_id,
      SUM(goods_master.cft) AS cft,
      SUM(goods_master.pcs) AS pcs
    FROM
      goods_master
    GROUP BY
      goods_master.stock_id) g ON i.stock_id = g.stock_id
  LEFT JOIN (SELECT
      outward.stock_id,
      SUM(outward.pcs) AS pcs,
      SUM(outward.cft)AS cft
    FROM
      outward
    GROUP BY outward.stock_id) o ON i.stock_id = o.stock_id
	LEFT JOIN (SELECT * from stock_category) sc on g.stock_cat_id= sc.scid 
	LEFT JOIN (SELECT * from sawmill_master) s on g.sawmill_id= s.sid GROUP BY i.stock_id');
	//echo json_encode($resultset);

/* for($i=0;$i<$resultset.length();$i++){
	echo $resultset[$i]["inward_pcs"];
} */
/* $resultset = mysqli_query($conn,"Select sum(pcs) as inward_pcs, sum(cft) as inward_cft from goods_master");
foreach($resultset as $usere)
{
	echo $usere["inward_pcs"];
	echo $usere["outward_cft"];
} */

/* $resultset = $db->selectQuery("Select sum(pcs) as inward_pcs, sum(cft) as inward_cft from goods_master group by stock_id");

$second = $db->selectQuery("Select sum(pcs) as outward_pcs, sum(cft) as outward_cft from outward group by stock_id");

array_push($resultset,$second[0]['outward_pcs']);

echo $second[1]['outward_pcs'];
echo $resultset[0]['outward_pcs']; */

//array_push($resultset,mysqli_query($conn,"Select sum(pcs) as outward_pcs, sum(cft) as outward_cft group by stock_id");


$data = array();
$cdt='';
	/* 	while( $rows = mysqli_fetch_assoc($resultset) ) {
			if(!empty($rows["outward_cft"]) or !empty($rows["closing_cft"])){
				$outward_cft=round($rows['outward_cft'],2);
				$closing_cft=round($rows['closing_cft'],2);
				$rows["outward_cft"] = $outward_cft;
				$rows["closing_cft"] = $closing_cft;
			}
			$data[] = $rows;
			} */
			
 for($i=0;$i<sizeOf($resultset);$i++){
	if(!empty($resultset[$i]["outward_cft"]) or !empty($resultset[$i]["closing_cft"])){
				$outward_cft=round($resultset[$i]['outward_cft'],2);
				$closing_cft=round($resultset[$i]['closing_cft'],2);
				$resultset[$i]["outward_cft"] = $outward_cft;
				$resultset[$i]["closing_cft"] = $closing_cft;
			}
	$data[$i] = $resultset[$i];
} 			
$results = array(
"sEcho" => 1,
"iTotalRecords" => count($data),
"iTotalDisplayRecords" => count($data),
"data" => $data
);
echo json_encode($results);
?>




