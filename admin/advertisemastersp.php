<?php include'header.php';

if($_SESSION['user_role'] == 'admin')

	{}

	else

	{

	header('Location: index.php');

	exit(0);	

	}

include 'Config/DB.php';

$db = new DB();

?>

<link href="css/dropzone.css" type="text/css" rel="stylesheet" />

<script src="dropzone.min.js"></script>
<script>

//for validation

function validation(method)

{	   


	var vTitle = false;
	var vCoverImage = false;
	var vDetailsImage = false;

	if(!$('#vTitle').val())

	{
		$('#vTitle').parent().addClass('has-error');
		$('#vTitle_help').show();
	}			

	else{$('#vTitle').parent().removeClass('has-error');$('#vTitle_help').hide();vTitle=true;}


	if(!$('#vCoverImage').val())

	{
		$('#vCoverImage').parent().addClass('has-error');
		$('#vCoverImage_help').show();
	}			

	else{$('#vCoverImage').parent().removeClass('has-error');$('#vCoverImage_help').hide();vCoverImage=true;}


	if(!$('#vDetailsImage').val())

	{
		$('#vDetailsImage').parent().addClass('has-error');
		$('#vDetailsImage_help').show();
	}			

	else{$('#vDetailsImage').parent().removeClass('has-error');$('#vDetailsImage_help').hide();vDetailsImage=true;}


	if(vTitle && vCoverImage && vDetailsImage) 

	{
			vaction(method);
	} 

	else{alert("There is some problem occured. Please try again.")}

		

}

//for table data 

function editEntry(id)

{   



	$('.help-block').hide();

	$('.has-error').removeClass('has-error');

	

	$.ajax({

	type: 'POST',

	dataType:'JSON',

	url: 'advertisemasterspaction.php',

	data: 'action_type=data&iSubCategoryID='+id,

	success:function(data){

	$('#iSubCategoryID').val(data.iSubCategoryID);	

	$('#iCategoryNameID').val(data.iCategoryMasterID);
	
	$('#isStatus').val(data.isStatus);

	$('#vText').val(data.vText);	

	var src = 'images/subcategory/cover_image/'+data.vCoverImage;
	$('#blah').attr('src', src);

	

	}

});

	}
//for rack master

</script>
<script>

function vaction(type,id){

    id = (typeof id == "undefined")?'':id;

    var userData = '';

 

 if (type == 'add') 

 {

     var userData = new FormData($(".Category_entry")[0]);

	userData.append('action_type', type);

    $.ajax({

        url: 'advertisemasterspaction.php',

        type: 'POST',

        data: userData,

        async: false,

        success: function (data) {

			if(data == 'ok'){

			$('#example').DataTable().ajax.reload(null, false);

			showSuccesMessage();

			resetdata();

			$('#cover_image_blah').attr('src', 'images/image_upload.jpg');
			$('#details_image_blah').attr('src', 'images/image_upload.jpg');

           }

		},

        cache: false,

        contentType: false,

        processData: false,

    });



    return false;

    }

	else if (type == 'edit'){

        var userData = $(".Category_entry").serialize()+'&action_type='+type;

		

		$.ajax({

			url: 'advertisemasterspaction.php',

			type: 'POST',

			data: userData,

			async: false,

			success: function (data) {

			if(data == 'ok'){

				$('#example').DataTable().ajax.reload(null, false);

				showEditSuccesMessage();

				resetdata();

				$(btnUpdate).hide();

				$(btnSave).show();

			}

        }

    });

     }

	else if (type == 'delete'){

     

		userData ='action_type=delete&iAdvertiseID='+id; 

		$.ajax({	

			url: 'advertisemasterspaction.php',

			type: 'POST',

			data: userData,

			async: false,

			success: function (data) {

				

				$('#example').DataTable().ajax.reload(null, false);

        },

       

    });

				

    }

    

    }

function resetdata(){

	

	  $('.Category_entry')[0].reset();

	  $('.help-block').hide();

	  $('.has-error').removeClass('has-error');

}



function prevent(e) {

   e.preventDefault();

}

</script>

<?php include'navbar.php' ?>

<?php include'sidebar.php' ?>

<section class="content">

<div class="container-fluid">

           

        </div>

				<div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">

                        <div class="header">

                            <h2><i class="fa fa-gear" aria-hidden="true"></i>Service Provider Advertisement</h2>

                         

                        </div>

                        <div class="body">
						<form id="form_validation" enctype="multipart/form-data" method="POST" class="Category_entry">
						
							<div class="row">
							
								<div class="col-sm-6">
									<label for="iSubCategoryID">Subcategory Name</label>
									<div class="form-line">
                                    <select class="form-control show-tick" name="iSubCategoryID" id="iSubCategoryID">
								
										<option value="">Select Subcategory Name.</option>	
										
											<?php
										$condition['where'] = array("iCategoryMasterID" => 4);
										$users = $db->getRows('sub_category_master',$condition);

										if(!empty($users)): $count = 0; foreach($users as $user): $count++;

										?>
										<option value="<?php echo $user['iSubCategoryID']; ?>">

										<?php echo $user['vText']; ?></option>

										<?php endforeach; else: ?> <?php endif; ?>											
                                    </select>
									<span class="help-block" id="iSubCategoryID_help" style="display:none;font-size:12px;color:red;">Please Select Subcategory Name.</span>
                                </div>
							</div>
							
								<div class="col-md-6">
									 <label>Title</label>
									 
								     <input type="text" class="form-control" name="vTitle" id="vTitle"  tabindex="0">
							
								</div>
							</div>
						
									<div class="row">

									<div class="col-sm-6">

										 <div class="form-line">

										 

										<div class="form-line">

										  <label><i class="fa fa-image"></i> Upload Cover Image</label>

										  <input type="file" class="btn btn-md" name="vCoverImage" id="vCoverImage" accept=".jpg, .jpeg, .png" tabindex="0" onchange="document.getElementById('cover_image_blah').src = window.URL.createObjectURL(this.files[0])">

										<!--<input type="file" name="image" id="image">-->

										</div>

										<p class="help-block">Max size 1 Mb</p>

									  </div>

								  </div>
								  
								
				
								  <!--  DETAILS IMAGE -->
								  <div class="col-sm-6">

									 <div class="form-line">

									 

									<div class="form-line">

									  <label><i class="fa fa-image"></i> Upload Details Image</label>

									  <input type="file" class="btn btn-md" name="vDetailsImage" id="vDetailsImage" accept=".jpg, .jpeg, .png" tabindex="0" onchange="document.getElementById('details_image_blah').src = window.URL.createObjectURL(this.files[0])">

									<!--<input type="file" name="image" id="image">-->

									</div>

									<p class="help-block">Max size 1 Mb</p>

								  </div>

								  </div>

									

			

				

				<!--IMAGE PREVIEW CODE-->
								
								</div>
								<div class="row">
								
									<div class="col-sm-6">

								<label class="margin-left">&nbsp;</label>

								<img id="cover_image_blah" src="images/image_upload.jpg" class="img-thumbnail" width="180px" height="150px" />

								</div>
				
								
								<div class="col-sm-6">

									<label class="margin-left">&nbsp;</label>

									<img id="details_image_blah" src="images/image_upload.jpg" class="img-thumbnail" width="180px" height="150px" />

									</div>
								</div>
								
								 <div class="row clearfix js-sweetalert">

								

                                <button id="btnSave" class="btn bg-blue btn-sm waves-effect" type="button" style="margin-left:20px;" onclick="validation('add');"><i class="material-icons">save</i><span class="icon-name">Save</span></button>

								

                                <button type="button" id="btnUpdate" class="btn bg-orange btn-sm waves-effect"  style="display: none;margin-left:20px;" onclick="vaction('edit');">

								<i class="material-icons">edit</i><span class="icon-name">Update</span></button>

								

                                 <button type="button" id="btnReset" class="btn bg-red btn-sm waves-effect" style="margin-left:20px;"  onclick="resetdata();$('#btnUpdate').hide();$('#btnSave').show();"><i class="material-icons">repeat</i><span class="icon-name">Reset</span></button>

							

                        </div>
						</div>
						</div>
                    </div>
                </div>
           
			 <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">

                        <div class="header">

                            <h2>

                               <i class="fa fa-table" aria-hidden="true"></i>  Advertise List

                            </h2> 
							</div>

                        <div class="body">

                            <div class="table-responsive">

                                <table id="example" class="table table-bordered table-striped table-hover dataTable">

                                    <thead>

                                        <tr>


                                            <th>SR NO</th>
                                            <th>Sub Category</th>
                                            <th>Title</th>
                                            <th>Cover Image</th>  
                                            <th>Details Image</th>  
                                            <th>Date</th>  
											<th>Action</th>
                                        </tr>
                                    </thead>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div> 

    </section>







<?php include'footer.php' ?>

<script>

 	  function toChangeDateFormat(dateStr)

{

    var parts = dateStr.split("-");

    //return new Date(parts[2], parts[1], parts[0]);

	var dt=  parts[2]+"/"+parts[1]+"/"+parts[0];

	return dt;

}



//ADD FOCUS ON SIDEBAR

	$("#sidebar_ul").find(".active").removeClass("active");

	$("#sidebar_advertise_sp_master_slider").addClass("active").focus();

$( document ).ready(function() {



	var otable = $('#example').DataTable( {

	dom: 'Bfrtip',

	buttons: [

            'copy', 'csv', 'excel', 'pdf', 'print'

        ],

"ajax": "advertisemasterspdata.php",

"bPaginate":true,

"bProcessing": true,





"dataSrc":"data",

 "columns": [

{ mData: 'count' },

{ mData: 'vText' } ,

{ mData: 'vTitle' } ,

{ mData: 'vCoverImage' } ,

{ mData: 'vDetailsImage' } ,

{ mData: 'created' } ,

{ mData: 'action' }

],

"columnDefs": [

 {targets: [3],
        "mData": "vCoverImage",
        "sClass": "center",
        "mRender": function (data, type, row) {
		return '<img src="'+row.vCoverImage+'" width="42" height="42">'
        }
    },
	
 {targets: [4],
        "mData": "vDetailsImage",
        "sClass": "center",
        "mRender": function (data, type, row) {
		return '<img src="'+row.vDetailsImage+'" width="42" height="42">'
        }
    },
	
{targets: [6],
        "mData": "action",
        "sClass": "center",
        "mRender": function (data, type, row) {
		return '<button class="btn bg-red waves-effect pull-left btn-sm" onclick="showConfirmMessage('+row.iAdvertiseID+');"><i class="material-icons">delete_forever</i></button>'

        }

    },

]		



});

});



</script>

<script>



function showStopMessage() {

    swal({

        title: "Stop!",

        text: "Dont cut this log.!",

        type: "error",



        closeOnConfirm: false

    });

}



function showSuccesMessage() {

    swal({

        title: "Success!",

        text: "Category Details has been Added succesfully.!",

        type: "success",



        closeOnConfirm: false

    });

}



function showEditSuccesMessage() {

    swal({

        title: "Success!",

        text: "Category Details has been Updated succesfully.!",

        type: "success",



        closeOnConfirm: false

    });

}



function showWarningMessage() {

    swal({

        title: "oops!",

        text: "Something went wrong, please check the values and try again.!",

        type: "warning",



        closeOnConfirm: false

    });

}



function showConfirmMessage(iAdvertiseID) {

    swal({

        title: "Are you sure?",

        text: "You will not be able to recover this data!",

        type: "warning",

        showCancelButton: true,

        confirmButtonColor: "#DD6B55",

        confirmButtonText: "Yes, delete it!",

        closeOnConfirm: false

    }, function () {

		vaction('delete',iAdvertiseID);

        swal("Deleted!", "Category data has been deleted successfully.", "success");

    });

}



</script>

<!--  <script type="text/javascript">

	$(document).ready(function() {

		$("#loading").hide();

		var options = {

			beforeSubmit:  showRequest,

			success:       showResponse,

			url:       'advertisemasterspaction.php',  // your upload script

			dataType:  'json'

		};

		$('#Form1').submit(function() {

			$('#message').html('');

			$(this).ajaxSubmit(options);

			return false;

		});

	}); 

	function showRequest(formData, jqForm, options) { 

		var fileToUploadValue = $('#fileToUpload').fieldValue();

		if (!fileToUploadValue[0]) { 

			$('#message').html('You need to select a file!'); 

			return false; 

		}

		$("#loading").show();

		return true; 

	} 

	function showResponse(data, statusText, xhr, $form)  {

		$("#loading").hide();

		if (statusText == 'success') {

			var msg = data.error.replace(/##/g, "<br />");

			if (data.img != '') {

				$('#result').html('<br /><img src="files/photo/' + data.img + '" />');

				$('#message').html('<br />' + msg + '<a href="index.html">Click here</a> to upload another file.'); 

				$('#formcont').html('');

			} else {

				$('#message').html(msg); 

			}

		} else {

			$('#message').html('Unknown error!'); 

		}

	} 

	</script> */

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="http://malsup.github.com/min/jquery.form.min.js"></script>

</body>

</html>