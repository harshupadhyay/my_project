<?php

include 'Config/DB.php';

$db = new DB();

$tblName = 'contacts_master';

if(isset($_POST['action_type']) && !empty($_POST['action_type'])){
    if($_POST['action_type'] == 'add'){
		if(!empty($_POST['vCategoryName'])){
            $userData = array(
				'vCategoryName' => $_POST['vCategoryName'],
				'isStatus' => $_POST['isStatus'],
				'isSubCategory' => isset($_POST['isSubCategory'])?'Yes':'No'
            );

            $insert = $db->insert($tblName,$userData);
            // echo 'ok';
            
			if(isset($_FILES["vImageName"]["name"])){
				$image_id=$insert;
				$image_name = time().'_'.$_FILES["vImageName"]["name"];
				$folder = "images/category/".$image_name;
				move_uploaded_file($_FILES["vImageName"]["tmp_name"], "$folder");
				$imgData = array(
					'vImageName' => $image_name,
				);

				$condition = array('iCategoryMasterID' => $image_id);
				$update = $db->update($tblName,$imgData,$condition);
				echo $update?"ok":"err";
			}
		}
    }elseif($_POST['action_type'] == 'edit'){
        if(!empty($_POST['iCategoryMasterID'])){
            $userData = array(
				'vCategoryName' => $_POST['vCategoryName'],
				'isStatus' => $_POST['isStatus'],
				'isSubCategory' => isset($_POST['isSubCategory'])?'Yes':'No',
            );

            $condition = array('iCategoryMasterID' => $_POST['iCategoryMasterID']);
            $update = $db->update($tblName,$userData,$condition);
			
			
			// print_r($_FILES);
			// exit;
			
			if($_FILES["vImageName"]["error"]== 0){
				$image_name = time().'_'.$_FILES["vImageName"]["name"];

				$folder = "images/category/".$image_name;

				move_uploaded_file($_FILES["vImageName"]["tmp_name"], "$folder");
				$imgData = array(
						'vImageName' => $image_name,
					);
				$condition = array('iCategoryMasterID' => $_POST['iCategoryMasterID']);
				$update = $db->update($tblName,$imgData,$condition);
			}
			echo $update?"ok":"err";
		}
		
    }elseif($_POST['action_type'] == 'delete'){
        if(!empty($_POST['iContactsMasterID'])){
            $condition = array('iContactsMasterID' => $_POST['iContactsMasterID']);
            $delete = $db->delete($tblName,$condition);
            echo $delete?'ok':'err';
        }
	}elseif($_POST['action_type'] == 'data'){
        $conditions['where'] = array('iCategoryMasterID'=>$_POST['iCategoryMasterID']);
        $conditions['return_type'] = 'single';
        $user = $db->getRows($tblName,$conditions);
        echo json_encode($user);
 	}
}

    exit;

 

?>







