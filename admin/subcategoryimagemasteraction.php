<?php

include 'Config/DB.php';

$db = new DB();

$tblName = 'sub_category_image';

if(isset($_POST['action_type']) && !empty($_POST['action_type'])){
    if($_POST['action_type'] == 'add'){
		if(!empty($_POST['iCategoryMasterID'])){
    		
		// Count total files
		$count = count($_POST['viText']);
		// echo $count;

		for($i=0;$i<$count;$i++){

	            $userData = array(
						'iCategoryMasterID' => $_POST['iCategoryMasterID'],
						'isStatus' => $_POST['isStatus'],
						'iSubCategoryID' => $_POST['iSubCategoryID'],
						'viText' => $_POST['viText'][$i],
	            );

	        	$insert = $db->insert($tblName,$userData);
				// echo $insert?"ok":"err"."<br>";
				if($insert){
					$image_id=$insert;
					$image_name = time().'_'.$_FILES["vImageName"]["name"][$i];
					$folder = "images/subcategory/".$image_name;
					move_uploaded_file($_FILES["vImageName"]["tmp_name"][$i], "$folder");
					$imgData = array(
									'vImageName' => $image_name,
								);
					$condition = array('iSubCategoryImageID' => $image_id);
					$update = $db->update($tblName,$imgData,$condition);
					// echo $update?"ok":"err";
				}
			}
		}
			echo "ok";
    }elseif($_POST['action_type'] == 'edit'){
        if(!empty($_POST['iSubCategoryImageID'])){
    		
			$count = count($_POST['viText']);
			
			for($i=0;$i<$count;$i++)
			{
			
		  $userData = array(
				'iCategoryMasterID' => $_POST['iCategoryMasterID'],
				'iSubCategoryID' => $_POST['iSubCategoryID'],
				'isStatus' => 'active',
				'viText' => $_POST['viText'][$i],
            );
			$msg = "";
            $condition = array('iSubCategoryImageID' => $_POST['iSubCategoryImageID']);
            $update = $db->update($tblName,$userData,$condition);
			$msg = $update?"ok":"err";
			
			if($_FILES["vImageName"]["error"][$i]== 0){
			$image_name = time().'_'.$_FILES["vImageName"]["name"][$i];
			$folder = "images/subcategory/".$image_name;

			move_uploaded_file($_FILES["vImageName"]["tmp_name"][$i], "$folder");
				$imgData = array(
						'vImageName' => $image_name,
					);
				$condition = array('iSubCategoryImageID' => $_POST['iSubCategoryImageID']);
				$update = $db->update($tblName,$imgData,$condition);
				$msg = $update?"ok":"err";
			}
			}
        echo $msg;
		}
    }elseif($_POST['action_type'] == 'delete'){

        if(!empty($_POST['iSubCategoryImageID'])){

            $condition = array('iSubCategoryImageID' => $_POST['iSubCategoryImageID']);

            $delete = $db->delete($tblName,$condition);

            echo $delete?'ok':'err';

        }

	}elseif($_POST['action_type'] == 'data'){

        $conditions['where'] = array('iSubCategoryImageID'=>$_POST['iSubCategoryImageID']);

        $conditions['return_type'] = 'single';

        $user = $db->getRows($tblName,$conditions);

        echo json_encode($user);

	 }

	 

	 // elseif($_POST['action_type'] == 'getSubCat')
	 // {
		
        // $conditions['select'] = "iSubCategoryID,viText";
        // $conditions['where'] = array('iCategoryMasterID'=>$_POST['iCategoryMasterID']);
        // $lots = $db->getRows("sub_category_master",$conditions);
		
		
		
		// echo json_encode($lots);
	 // }
	 
	 
	  elseif($_POST['action_type'] == 'getSubCat')
	 {
		
        $iCategoryMasterID = $_POST['iCategoryMasterID'];
        
		$rate = $db->selectQuery("select iSubCategoryID,vText from sub_category_master where iCategoryMasterID='$iCategoryMasterID' ");	
		
		echo json_encode($rate);
	 }
	 
	  elseif($_POST['action_type'] == 'getSubCat1')
	 {
		
        $iCategoryMasterID = $_POST['category_id_search'];
        
		$rate = $db->selectQuery("select iSubCategoryID,vText from sub_category_master where iCategoryMasterID='$iCategoryMasterID' ");	
		
		echo json_encode($rate);
	 }
	 
	
	 
    }

    exit;

 

?>







