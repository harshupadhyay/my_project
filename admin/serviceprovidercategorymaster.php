<?php
	include'header.php';
	if($_SESSION['user_role'] == 'admin'){

	}else{
		header('Location: index.php');
		exit(0);	
	}
	include 'Config/DB.php';
	$db = new DB();
?>

<link href="css/dropzone.css" type="text/css" rel="stylesheet" />
<script src="dropzone.min.js"></script>
<script>
	function validation(method){	   
		var iCategoryNameID = false;
		var vText = false;
		var isStatus = false;
		if(!$('#isStatus').val()){
			$('#isStatus').parent().addClass('has-error');
			$('#isStatus_help').show();
		}else{
			$('#isStatus').parent().removeClass('has-error');$('#isStatus_help').
			hide();isStatus=true;
		}
		if(!$('#iCategoryNameID').val()){
			$('#iCategoryNameID').parent().addClass('has-error');
			$('#iCategoryNameID_help').show();
		}else{
			$('#iCategoryNameID').parent().removeClass('has-error');$('#iCategoryNameID_help').hide();iCategoryNameID=true;
		}

		if(!$('#vText').val()){
			$('#vText').parent().addClass('has-error');
			$('#vText_help').show();
		}else{
			$('#vText').parent().removeClass('has-error');$('#vText_help').hide();vText=true;
		}

		if(iCategoryNameID && vText && isStatus ){
			vaction(method);

		}else{
			alert("There is some problem occured. Please try again.")
		}

	}

	function editEntry(id){   
		$('.help-block').hide();
		$('.has-error').removeClass('has-error');

		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url: 'serviceprovidercategoryaction.php',
			data: 'action_type=data&iSubCategoryID='+id,
			success:function(data){
				$('#iSubCategoryID').val(data.iSubCategoryID);	
				$('#iCategoryMasterID').val(data.iCategoryMasterID);
				$('#isStatus').val(data.isStatus);
				$('#vText').val(data.vText);	
				var src = 'images/subcategory/cover_image/'+data.vCoverImage;
					$('#blah').attr('src', src);
			}
		});
	}
</script>

<script>
function vaction(type,id){

    id = (typeof id == "undefined")?'':id;

    var userData = '';

 if (type == 'add') 

 {

     var userData = new FormData($(".Category_entry")[0]);

	userData.append('action_type', type);

    $.ajax({

        url: 'serviceprovidercategoryaction.php',

        type: 'POST',

        data: userData,

        async: false,
        success: function (data) {
			if(data == 'ok'){
			$('#example').DataTable().ajax.reload(null, false);
			showSuccesMessage();
			resetdata();

			$('#blah').attr('src', 'images/image_upload.jpg');
			
			
			

           }

		},
        cache: false,
        contentType: false,
        processData: false,
    });

    return false;

    }else if (type == 'edit'){

         var userData = new FormData($(".Category_entry")[0]);

		userData.append('action_type', type);

		
		$.ajax({

			url: 'serviceprovidercategoryaction.php',

			type: 'POST',

			data: userData,

			async: false,

			success: function (data) {

			if(data == 'ok'){

				$('#example').DataTable().ajax.reload(null, false);

				showEditSuccesMessage();

				resetdata();

				$(btnUpdate).hide();

				$(btnSave).show();

			}

        },
		cache: false,

        contentType: false,

        processData: false,

    });
	  return false;

     }
else if (type == 'delete'){
		userData ='action_type=delete&iSubCategoryID='+id; 
		$.ajax({	
			url: 'serviceprovidercategoryaction.php',
			type: 'POST',
			data: userData,
			async: false,
			success: function (data) {
				$('#example').DataTable().ajax.reload(null, false);
        	},
    	});
	}
}



function resetdata(){



	



	  $('.Category_entry')[0].reset();



	  $('.help-block').hide();



	  $('.has-error').removeClass('has-error');



}







function prevent(e) {



   e.preventDefault();



}



</script>







<?php include'navbar.php' ?>



<?php include'sidebar.php' ?>
	<section class="content">
	    <div class="container-fluid">
        </div>
			<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2><i class="fa fa-gear" aria-hidden="true"></i>  Service Provider Category </h2>
                        </div>
                        <div class="body">
							<form id="form_validation" enctype="multipart/form-data" method="POST" class="Category_entry">
							<div class="row">
								<div class="hidden">
									<div class="col-md-6">
										<label class="form-label">Category Name</label><span style="color:red"> </span>
                                        <select class="form-control" name="iCategoryNameID" id="iCategoryNameID">
											<option value="4">Service Provider</option>	
										</select>
	                                    <span class="help-block" id="iCategoryNameID_help" style="display:none;font-size:12px;color:red;">Please Select Category Name.</span>
                                    </div>
								</div>
								
								<div class="col-sm-12">
									<label class="form-label">Sub-category Name</label>
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Enter Subcategory  Name" name="vText" id="vText" />
                                    </div>
									<span class="help-block" id="vText_help" style="display:none;font-size:12px;color:red;">Please Enter Category Name.</span>
                                </div>   
								<div class="col-sm-4">
  						 			<div class="form-line">
										<div class="form-line">
										  	<i class="fa fa-image"></i> Upload Image
											<input type="file" name="vCoverImage" id="vCoverImage" accept=".jpg, .jpeg, .png" tabindex="0" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
										</div>
										<p class="help-block">Max size 1 Mb</p>
								    </div>
								</div>
								<div class="col-sm-4">
									<label class="margin-left">&nbsp;</label>
									<img id="blah" src="images/image_upload.jpg" class="img-thumbnail" width="200px" height="150px" />
								</div>
								<div class="col-sm-4">
									<label class="form-label">Status</label><span style="color:red"> * </span>
                                    <select class="form-control input-md" name="isStatus" id="isStatus">
										<option>active</option>
										<option>deactive</option>
									</select>
									<span class="help-block" id="isStatus_help" style="display:none;">Please Select Status.</span>
                                </div>
							</div>
                           	<div class="row clearfix js-sweetalert">
  							  	<input type="hidden" id="iSubCategoryID" name="iSubCategoryID"/>
                                <button id="btnSave" class="btn bg-blue btn-sm waves-effect" type="button" style="margin-left:20px;" onclick="validation('add');"><i class="material-icons">save</i><span class="icon-name">Save</span></button>
                                <button type="button" id="btnUpdate" class="btn bg-orange btn-sm waves-effect"  style="display: none;margin-left:20px;" onclick="vaction('edit');">
								<i class="material-icons">edit</i><span class="icon-name">Update</span></button>
                                <button type="button" id="btnReset" class="btn bg-red btn-sm waves-effect" style="margin-left:20px;"  onclick="resetdata();$('#btnUpdate').hide();$('#btnSave').show();"><i class="material-icons">repeat</i><span class="icon-name">Reset</span></button>
                        	</div>
					    </form>
                    </div>
				</div>
            </div>
        </div>



				



           <div class="row clearfix">



                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">



                    <div class="card">



                        <div class="header">
                            <h2>
								<i class="fa fa-table" aria-hidden="true"></i>  Service Provider List
							</h2>
                        </div>



                        <div class="body">



                            <div class="table-responsive">



                                <table id="example" class="table table-bordered table-striped table-hover dataTable">



                                    <thead>



                                        <tr>



                                            <th>&nbsp;</th>



                                            <th>SR NO</th>



                                            <th>Category Name</th> 



											<th>Subcategory Name</th>



											<th>Cover Image</th>



											<th>Status</th>



                                            <th>Date</th>  



											<th>Action</th>



                                        </tr>



                                    </thead>



                       



                                </table>



                            </div>



                        </div>



                    </div>



                </div>



            </div> 

    </section>

<?php include'footer.php' ?>
<script>

	$("#sidebar_ul").find(".active").removeClass("active");
	$("#sidebar_spc_master").addClass("active").focus();
	function toChangeDateFormat(dateStr){
	    var parts = dateStr.split("-");
		var dt=  parts[2]+"/"+parts[1]+"/"+parts[0];
		return dt;
	}

	$( document ).ready(function() {
		var otable = $('#example').DataTable( {
		dom: 'Bfrtip',
		buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
		"ajax": "serviceprovidercategorymasterdata.php",
		"bPaginate":true,
		"bProcessing": true,
		"dataSrc":"data",
		"columns": [
			{ mData: 'iSubCategoryID' },
			{ mData: 'count' },
			{ mData: 'vCategoryName' } ,
			{ mData: 'vText' } ,
			{ mData: 'vCoverImage' } ,
			{ mData: 'isStatus' } ,
			{ mData: 'created'  },
			{ mData: 'action' }
		],



"columnDefs": [







{ targets: [0],



        "mData": "iSubCategoryID",



		"sClass": "center",



		"width": "1%",



		"visible":false



 },







 



 



 



 {targets: [4],







        "mData": "vCoverImage",







        "sClass": "center",







        "mRender": function (data, type, row) {







		return '<img src="images/subcategory/cover_image/'+row.vCoverImage+'" width="42" height="42">'







        }







    }, 



	







 {targets: [5],

				"mData": "isStatus",

					"sClass": "center",

				"mRender": function (data, type, row) {

				if(row.isStatus == "active"){$color = '<b><span style="color:#42ce42">'}else{$color = '<b><span style="color:red">'}

					return $color+row.isStatus+'</span></b>'

					}

				},



 



 



  {targets: [7],



        "mData": "action",



        "sClass": "center",



        "mRender": function (data, type, row) {



		return '<button type="button" class="btn bg-blue waves-effect pull-left btn-sm" onclick="$(\'#btnSave\').hide();$(\'#btnUpdate\').show();editEntry('+row.iSubCategoryID	+')"><i class="material-icons">edit</i></button>' +



		'<button class="btn bg-red waves-effect pull-left btn-sm" onclick="showConfirmMessage('+row.iSubCategoryID	+');"><i class="material-icons">delete_forever</i></button>'



        }



    },



]		







});



});







</script>



<script>







function showStopMessage() {



    swal({



        title: "Stop!",



        text: "Dont cut this log.!",



        type: "error",







        closeOnConfirm: false



    });



}







function showSuccesMessage() {



    swal({



        title: "Success!",



        text: "Category Details has been Added succesfully.!",



        type: "success",







        closeOnConfirm: false



    });



}







function showEditSuccesMessage() {



    swal({



        title: "Success!",



        text: "Category Details has been Updated succesfully.!",



        type: "success",







        closeOnConfirm: false



    });



}







function showWarningMessage() {



    swal({



        title: "oops!",



        text: "Something went wrong, please check the values and try again.!",



        type: "warning",







        closeOnConfirm: false



    });



}







function showConfirmMessage(iSubCategoryID) {



    swal({



        title: "Are you sure?",



        text: "You will not be able to recover this data!",



        type: "warning",



        showCancelButton: true,



        confirmButtonColor: "#DD6B55",



        confirmButtonText: "Yes, delete it!",



        closeOnConfirm: false



    }, function () {



		vaction('delete',iSubCategoryID);



        swal("Deleted!", "Category data has been deleted successfully.", "success");



    });



}







</script>



<!--  <script type="text/javascript">



	$(document).ready(function() {



		$("#loading").hide();



		var options = {



			beforeSubmit:  showRequest,



			success:       showResponse,



			url:       'subcategorymasteraction.php',  // your upload script



			dataType:  'json'



		};



		$('#Form1').submit(function() {



			$('#message').html('');



			$(this).ajaxSubmit(options);



			return false;



		});



	}); 



	function showRequest(formData, jqForm, options) { 



		var fileToUploadValue = $('#fileToUpload').fieldValue();



		if (!fileToUploadValue[0]) { 



			$('#message').html('You need to select a file!'); 



			return false; 



		}



		$("#loading").show();



		return true; 



	} 



	function showResponse(data, statusText, xhr, $form)  {



		$("#loading").hide();



		if (statusText == 'success') {



			var msg = data.error.replace(/##/g, "<br />");



			if (data.img != '') {



				$('#result').html('<br /><img src="files/photo/' + data.img + '" />');



				$('#message').html('<br />' + msg + '<a href="index.html">Click here</a> to upload another file.'); 



				$('#formcont').html('');



			} else {



				$('#message').html(msg); 



			}



		} else {



			$('#message').html('Unknown error!'); 



		}



	} 



	</script> */



	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->



    <script src="http://malsup.github.com/min/jquery.form.min.js"></script>



</body>



</html>