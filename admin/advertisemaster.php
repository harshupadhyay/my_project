<?php include'header.php';

if($_SESSION['user_role'] == 'admin')

	{}

	else

	{

	header('Location: index.php');

	exit(0);	

	}

include 'Config/DB.php';

$db = new DB();

?>

<link href="css/dropzone.css" type="text/css" rel="stylesheet" />

<script src="dropzone.min.js"></script>
<script>

//for validation

function validation(method)

{	   


	var vCoverImage = false;

	if(!$('#vCoverImage').val())

	{
		$('#vCoverImage').parent().addClass('has-error');
		$('#vCoverImage_help').show();
	}			

	else{$('#vCoverImage').parent().removeClass('has-error');$('#vCoverImage_help').hide();vCoverImage=true;}


	if(vCoverImage) 

	{
			vaction(method);
	} 

	else{alert("There is some problem occured. Please try again.")}

		

}

//for table data 

function editEntry(id)

{   



	$('.help-block').hide();

	$('.has-error').removeClass('has-error');

	

	$.ajax({

	type: 'POST',

	dataType:'JSON',

	url: 'advertisemasteraction.php',

	data: 'action_type=data&iAdvertiseID='+id,

	success:function(data){

	$('#iAdvertiseID').val(data.iAdvertiseID);	

	$('#iCategoryNameID').val(data.iCategoryMasterID);
	
	$('#isStatus').val(data.isStatus);

	$('#vText').val(data.vText);	

	var src = data.vCoverImage;
	$('#blah').attr('src', src);

	

	}

});

	}
//for rack master

</script>
<script>

function vaction(type,id){
	$("#loading").fadeIn();
    id = (typeof id == "undefined")?'':id;

    var userData = '';

 

 if (type == 'add') 

 {

     var userData = new FormData($(".Category_entry")[0]);

	userData.append('action_type', type);

    $.ajax({

        url: 'advertisemasteraction.php',

        type: 'POST',

        data: userData,

        async: false,

        success: function (data) {
			

			if(data == 'ok'){

			$('#example').DataTable().ajax.reload(null, false);

			showSuccesMessage();

			resetdata();

			$('#blah').attr('src', 'images/image_upload.jpg');

           }

		},

        cache: false,

        contentType: false,

        processData: false,

    });



    return false;

    }

	else if (type == 'edit'){

        var userData = new FormData($(".Category_entry")[0]);
	
		userData.append('action_type', type);
			

		$.ajax({

			url: 'advertisemasteraction.php',

			type: 'POST',

			data: userData,

			async: false,

			success: function (data) {
				$("#loading").fadeOut();
				
				if(data == 'ok')
			{

				$('#example').DataTable().ajax.reload(null, false);

				showEditSuccesMessage();

		
				$("#updateSection").hide();
				
			}

        },
		   cache: false,

        contentType: false,

        processData: false,

    });


    return false;
     }

	else if (type == 'delete'){

     

		userData ='action_type=delete&iAdvertiseID='+id; 

		$.ajax({	

			url: 'advertisemasteraction.php',

			type: 'POST',

			data: userData,

			async: false,

			success: function (data) {

				

				$('#example').DataTable().ajax.reload(null, false);

        },

       

    });

				

    }

    

    }

function resetdata(){

	

	  $('.Category_entry')[0].reset();

	  $('.help-block').hide();

	  $('.has-error').removeClass('has-error');

}



function prevent(e) {

   e.preventDefault();

}

</script>

<?php include'navbar.php' ?>

<?php include'sidebar.php' ?>

<section class="content">

<div class="container-fluid">

           

        </div>

				<div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card" style="display:none" id="updateSection">

                        <div class="header">

                            <h2><i class="fa fa-gear" aria-hidden="true"></i>  Advertise Details</h2>

                         

                        </div>

                        <div class="body">
						<form id="form_validation" enctype="multipart/form-data" method="POST" class="Category_entry">
						
									<div class="row">

									<div class="col-sm-4">

									 <div class="form-line">

									 

									<div class="form-line">

									  <label><i class="fa fa-image"></i> Upload Image</label>

									  <input type="file" class="btn btn-md" name="vCoverImage" id="vCoverImage" accept=".jpg, .jpeg, .png" tabindex="0" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">

									<!--<input type="file" name="image" id="image">-->

									</div>

									<p class="help-block">Max size 1 Mb</p>

								  </div>

								  </div>				

								<div class="col-sm-4">

								<label class="margin-left">&nbsp;</label>

								<img id="blah" src="images/image_upload.jpg" class="img-thumbnail" width="180px" height="150px" />

								</div>																	  								  <div class="col-sm-4">									 <div class="form-line">									  <label>Type</label>									  <select class="form-control" name="isStatus" id="isStatus">

											<option value="">Select Category Name</option>
											<option value="active">Active</option>
											<option value="deactive">Deactive</option>

                                    </select>									<!--<input type="file" name="image" id="image">-->									</div>									<p class="help-block">Max size 1 Mb</p>								  </div>
				
	

				

				<!--IMAGE PREVIEW CODE-->
								
								</div>
								
								  <input type="hidden" id="iAdvertiseID" name="iAdvertiseID"/>

								 <div class="row clearfix js-sweetalert">

								
							<!--
                                <button id="btnSave" class="btn bg-blue btn-sm waves-effect" type="button" style="margin-left:20px;" onclick="validation('add');"><i class="material-icons">save</i><span class="icon-name">Save</span></button>
-->
								

                                <button type="button" id="btnUpdate" class="btn bg-orange btn-sm waves-effect"  style="margin-left:20px;" onclick="vaction('edit');">

								<i class="material-icons">edit</i><span class="icon-name">Update</span></button>
									
							<div class="col-md-12" id="loading" style="text-align:-webkit-center;display:none;">		
								<div class="loader" ></div>
								<span>Uploading..</span><br/>
								
							</div>
								
<!--
                                 <button type="button" id="btnReset" class="btn bg-red btn-sm waves-effect" style="margin-left:20px;"  onclick="resetdata();$('#btnUpdate').hide();$('#btnSave').show();"><i class="material-icons">repeat</i><span class="icon-name">Reset</span></button>
-->
							

                        </div>
						</div>
						</div>
                    </div>
                </div>
           
			 <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">

                        <div class="header">

                            <h2>

                               <i class="fa fa-table" aria-hidden="true"></i>  Advertise List

                            </h2> 
							</div>

                        <div class="body">

                            <div class="table-responsive">

                                <table id="example" class="table table-bordered table-striped table-hover dataTable">

                                    <thead>

                                        <tr>


                                            <th>SR NO</th>
                                            <th>Cover Image</th>  
                                            <th>Date</th>  
                                            <th>Status</th>  
											<th>Action</th>
                                        </tr>
                                    </thead>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div> 

    </section>







<?php include'footer.php' ?>

<script>

 	  function toChangeDateFormat(dateStr)

{

    var parts = dateStr.split("-");

    //return new Date(parts[2], parts[1], parts[0]);

	var dt=  parts[2]+"/"+parts[1]+"/"+parts[0];

	return dt;

}



//ADD FOCUS ON SIDEBAR

	$("#sidebar_ul").find(".active").removeClass("active");

	$("#sidebar_advertise_master_full").addClass("active").focus();

$( document ).ready(function() {



	var otable = $('#example').DataTable( {

	dom: 'Bfrtip',

	buttons: [

            'copy', 'csv', 'excel', 'pdf', 'print'

        ],

"ajax": "advertisemasterdata.php",

"bPaginate":true,

"bProcessing": true,





"dataSrc":"data",

 "columns": [

{ mData: 'count' },

{ mData: 'vCoverImage' } ,

{ mData: 'created' } ,

{ mData: 'isStatus' } ,

{ mData: 'action' }

],

"columnDefs": [

 {targets: [1],
        "mData": "vCoverImage",
        "sClass": "center",
        "mRender": function (data, type, row) {
		return '<img src="'+row.vCoverImage+'" width="42" height="42">'
        }
    },
	
{targets: [4],
        "mData": "action",
        "sClass": "center",
        "mRender": function (data, type, row) {
		return '<button type="button" class="btn bg-blue waves-effect pull-left btn-sm" onclick="resetdata();$(\'#addBTN\').hide();$(\'#updateSection\').show();$(\'#options_\').hide();$(\'#options__\').hide();$(\'#btnUpdate\').show();editEntry('+row.iAdvertiseID+')"><i class="material-icons">edit</i></button>'

        }

    },

]		



});

});



</script>

<script>



function showStopMessage() {

    swal({

        title: "Stop!",

        text: "Dont cut this log.!",

        type: "error",



        closeOnConfirm: false

    });

}



function showSuccesMessage() {

    swal({

        title: "Success!",

        text: "Category Details has been Added succesfully.!",

        type: "success",



        closeOnConfirm: false

    });

}



function showEditSuccesMessage() {

    swal({

        title: "Success!",

        text: "Category Details has been Updated succesfully.!",

        type: "success",



        closeOnConfirm: false

    });

}



function showWarningMessage() {

    swal({

        title: "oops!",

        text: "Something went wrong, please check the values and try again.!",

        type: "warning",



        closeOnConfirm: false

    });

}



function showConfirmMessage(iAdvertiseID) {

    swal({

        title: "Are you sure?",

        text: "You will not be able to recover this data!",

        type: "warning",

        showCancelButton: true,

        confirmButtonColor: "#DD6B55",

        confirmButtonText: "Yes, delete it!",

        closeOnConfirm: false

    }, function () {

		vaction('delete',iAdvertiseID);

        swal("Deleted!", "Category data has been deleted successfully.", "success");

    });

}



</script>

<!--  <script type="text/javascript">

	$(document).ready(function() {

		$("#loading").hide();

		var options = {

			beforeSubmit:  showRequest,

			success:       showResponse,

			url:       'advertisemasteraction.php',  // your upload script

			dataType:  'json'

		};

		$('#Form1').submit(function() {

			$('#message').html('');

			$(this).ajaxSubmit(options);

			return false;

		});

	}); 

	function showRequest(formData, jqForm, options) { 

		var fileToUploadValue = $('#fileToUpload').fieldValue();

		if (!fileToUploadValue[0]) { 

			$('#message').html('You need to select a file!'); 

			return false; 

		}

		$("#loading").show();

		return true; 

	} 

	function showResponse(data, statusText, xhr, $form)  {

		$("#loading").hide();

		if (statusText == 'success') {

			var msg = data.error.replace(/##/g, "<br />");

			if (data.img != '') {

				$('#result').html('<br /><img src="files/photo/' + data.img + '" />');

				$('#message').html('<br />' + msg + '<a href="index.html">Click here</a> to upload another file.'); 

				$('#formcont').html('');

			} else {

				$('#message').html(msg); 

			}

		} else {

			$('#message').html('Unknown error!'); 

		}

	} 

	</script> */

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="http://malsup.github.com/min/jquery.form.min.js"></script>

</body>

</html>