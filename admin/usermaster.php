<?php include'header.php';
if($_SESSION['user_role'] != 'admin')
	{
	header('Location: index.php');
	exit(0);	
}
include 'Config/DB.php';
$db = new DB();
?>

<script>


//for validation
 
function validateEmail(sEmail) {
var filter =/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {return true;}
	else {return false;}
}



function randomPass(){
		$.ajax({
		type: 'POST',
		url: 'usermasteraction.php',
		data: 'action_type=getPass',
		async: false,
		success:function(pass)
		{
			$("#password").val(pass);
		}
		});
}
	
function validation(method)
{	   


	var username = false;
	var password = false;
	var email = false;
	var contact_no = false;
	var city = false;
	var role = false;
	var status = false;

	
	
	if($('#username').val() == '')
	{
			 $('#username').parent().addClass('has-error');
			 $('#username_help').show();
	}
	
	if($('#username').val())
	{
		if(method == 'edit'){
			$.ajax({
			type: 'POST',
			url: 'usermasteraction.php',
			data: 'action_type=validateusernameEdit&username='+$('#username').val()+'&uid='+$('#uid').val(),
			async: false,
			success:function(msg)
			{
			if(msg == "err")
			{
			alert("This Supervisor Name is already registered");
			}
			else if(msg == "ok"){
				$('#username').parent().removeClass('has-error');$('#username_help').hide();
				username=true;
				}
			}
			});
			
		}
		else{
			$.ajax({
			type: 'POST',
			url: 'usermasteraction.php',
			data: 'action_type=validateusername&username='+$('#username').val(),
			async: false,
			success:function(msg)
			{
			if(msg == "err")
			{
			alert("This Supervisor Name is already registered on our database.");
			}
			else if(msg == "ok"){
				$('#username').parent().removeClass('has-error');$('#username_help').hide();
				username=true;
				}
			}
			});
		}
	}
	
	
	if(!$('#email').val())
	{
			 $('#email').parent().addClass('has-error');
			 $('#email_help').show();
	}	
	
	if($('#email').val())
	{
	if(validateEmail($('#email').val())){
		
	if(method == 'edit'){
		$customer = $('#email').val();	
		
		$.ajax({
		type: 'POST',
		url: 'usermasteraction.php',
		data: 'action_type=validateemailEdit&email='+$customer+'&uid='+$('#uid').val(),
		async: false,
		success:function(msg)
		{
		if(msg == "err")
		{
		alert("This email is already registered, please enter another email address.");
		}
		if(msg == "ok"){
			$('#email').parent().removeClass('has-error');$('#email_help').hide();
			email=true;
			}
		}
		});
		}
		
	
		else{
		$customer = $('#email').val();	
	
		$.ajax({
		type: 'POST',
		url: 'usermasteraction.php',
		data: 'action_type=validateemail&email='+$customer,
		async: false,
		success:function(msg)
		{
		if(msg == "err")
		{
		alert("This email is already registered, please enter another email address.");
		}
		if(msg == "ok"){
			$('#email').parent().removeClass('has-error');$('#email_help').hide();
			email=true;
			}
		}
		});
		}
		}
	}
	else{
				alert("Please enter valid email address.");
	}
	

	if(!$('#password').val())
	{
			$('#password').parent().addClass('has-error');
			$('#password_help').show();
	}			
	else{$('#password').parent().removeClass('has-error');$('#password_help').hide();password=true;}
	
	if(!$('#contact_no').val())
	{
		$('#contact_no').parent().addClass('has-error');
		$('#contact_no_help').show();
	}
	else{$('#contact_no').parent().removeClass('has-error');$('#contact_no_help').hide();contact_no=true;}

	if(!$('#city').val())
	{
			$('#city').parent().addClass('has-error');
			$('#city_help').show();
	}
	else{$('#city').parent().removeClass('has-error');$('#city_help').hide();city=true;}

	
	if(!$('#role').val())
	{
			$('#role').parent().addClass('has-error');
			$('#role_help').show();
	}
	else{$('#role').parent().removeClass('has-error');$('#role_help').hide();role=true;}
	
	if(!$('#status').val())
	{
			$('#status').parent().addClass('has-error');
			$('#status_help').show();
	}
	else{$('#status').parent().removeClass('has-error');$('#status_help').hide();status=true;}
	

	
	
	

	
	
	
	if(username &&  password && email &&  contact_no && city && address && role && status  ) 
	{
			vaction(method);
	} 
	else{alert("There is some problem occured. Please try again.")}
		
}

//for table data 
function editEntry(id)
{   
	$.ajax({
	type: 'POST',
	dataType:'JSON',
	url: 'usermasteraction.php',
	data: 'action_type=data&uid='+id,
	success:function(data){
	$('#uid').val(data.uid);	
	$('#username').val(data.username);	
	$('#email').val(data.email);	
	$('#password').val(data.password);	
	$('#contact_no').val(data.contact_no);	
	$('#city').val(data.city);
	$('#address').val(data.address);
	$('#status').val(data.status);	
	$('#role').val(data.role);	
	
	
	
	}
});
}
	

	
//for rack master
</script>
<script>
function vaction(type,id){
    id = (typeof id == "undefined")?'':id;
    var userData = '';
 
 if (type == 'add') 
 {
    var userData = 	$(".user_entry").serialize()+'&action_type='+type;
    $.ajax({
        url: 'usermasteraction.php',
        type: 'POST',
        data: userData,
        async: false,
        success: function (data){
			if(data == 'ok'){
			$('#example').DataTable().ajax.reload(null, false);
			resetdata();
			 randomPass();
			showSuccessMessage();	
        }
	}
});

	
    }
	else if (type == 'edit'){
        var userData = $(".user_entry").serialize()+'&action_type='+type;
		
		$.ajax({
			url: 'usermasteraction.php',
			type: 'POST',
			data: userData,
			async: false,
			success: function (data) {
				
				$('#example').DataTable().ajax.reload(null, false);
				resetdata();
				 randomPass();
				$(btnUpdate).hide();
				$(btnSave).show();
				randomPass();
				showSuccessMessageEdit();
			
        }
    });
     }
	 
	 
	else if (type == 'delete'){
     
		userData ='action_type=delete&uid='+id; 
		$.ajax({	
			url: 'usermasteraction.php',
			type: 'POST',
			data: userData,
			async: false,
			success: function (data) {
				
				$('#example').DataTable().ajax.reload(null, false);
        },
       
    });
				
    }
    
    }
function resetdata(){
	
	  $('.user_entry')[0].reset();
}

function prevent(e) {
   e.preventDefault();
}
</script>

<?php include'navbar.php'?>
    <?php include'sidebar.php'?>
  <section class="content">
  <!--      <div class="container-fluid">
            <div class="block-header">
                <h2>Consignment Details</h2>
            </div>
        </div>
		 -->
		 <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2><i class="fa fa-fw fa-users"></i><span>  User Master</span></h2>
                         
                        </div>
                        <div class="body">
                            <form id="form_validation" method="POST" class="user_entry">
							
								<div class="row">
									<div class="col-md-3">
									<label class="form-label">User  Name</label><span style="color:red"> * </span>
                                        <div class="form-line">
                                           <input type="text"  id="username" class="form-control input-md" tabindex="0" name="username"  placeholder="Enter User Name..." autocomplete="off">
											<span class="help-block" id="username_help" style="display:none;">Please Enter User Name.</span>
                                        </div>
                                    </div>
								
								
								<div class="col-md-3">
									<label class="form-label">E-mail</label><span style="color:red"> * </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control input-md" id="email"  name="email" placeholder="Enter E-mail..." tabindex="0">
										<span class="help-block" id="email_help" style="display:none;">Please Enter E-mail.</span>
                                        </div>
                                    </div>
									<div class="col-md-3">
									<label class="form-label">Password</label><span style="color:red"> * </span>
                                        <div class="form-line">
                                             <input type="text" class="form-control input-md" id="password"  name="password" placeholder="Enter Password...">
										<span class="help-block" id="password_help" style="display:none;">Please Enter Password.</span>
                                        </div>
                                    </div>
                                   
									
									<div class="col-md-3">
									<label class="form-label">Role</label><span style="color:red"> * </span>
                                      
                                            <select class="form-control input-md" name="role" id="role">
										<option value="">Select Role..</option>	
										<option  value="Role1">Role1</option>
										<option  value="Role2">Role2</option>
										
										</select>
                                      <span class="help-block" id="role_help" style="display:none;">Please Select Role.</span>
                                    </div>
									 </div>
								<div class="row">
									<div class="col-md-4">
									<label class="form-label">Contact Number</label><span style="color:red"> * </span>
                                        <div class="form-line">
                                             <input type="number" class="form-control input-md" id="contact_no"  name="contact_no" placeholder="Enter Contact No..." tabindex="0" autocomplete="off">
											<span class="help-block" id="contact_no_help" style="display:none;">Please Enter Contact No.</span>
                                        </div>
                                    </div>
                                
								
								<div class="col-md-4">
									<label class="form-label">City</label><span style="color:red"> * </span>
                                        <div class="form-line">
                                           <input type="text"  id="city" class="form-control input-md" tabindex="0" name="city"  placeholder="Enter City..." autocomplete="off">
											<span class="help-block" id="city_help" style="display:none;">Please Enter City.</span>
                                        </div>
                                    </div>
									
									<div class="col-md-4">
									<label class="form-label">Status</label><span style="color:red"> * </span>
                                      
                                            <select class="form-control input-md" name="status" id="status">
										<option>Active</option>
										<option>Deactive</option>
                  
										</select>
										<span class="help-block" id="status_help" style="display:none;">Please Select Status.</span>
                                      
                                    </div>
									
								
                                </div>
									
								<div class="row">
									<div class="col-md-12">
									<label class="form-label">Address</label>
                                        <div class="form-line">
                                           <textarea class="form-control" rows="4" id="address" name="address" placeholder="Enter Address..." tabindex="0"></textarea>
										<span class="help-block" id="address_help" style="display:none;">Please Enter Address.</span>
                                        </div>
                                    </div>
								</div>
									
								<input type="hidden" id="uid" name="uid"/>
								 <div class="row clearfix js-sweetalert">
								
								<div class="row">
								<div class="col-md-12">
                                <button id="btnSave" class="btn bg-blue waves-effect"  type="button" style="margin-left:20px;" onclick="validation('add');"><i class="material-icons">save</i><span class="icon-name">Save</span></button>
								
                                <button type="button" id="btnUpdate" class="btn bg-orange waves-effect"  style="display: none;margin-left:20px;" onclick="vaction('edit');">
								<i class="material-icons">edit</i><span class="icon-name">Update</span></button>
								
                                 <button type="button" id="btnReset" class="btn bg-red" style="margin-left:20px;"  onclick="resetdata();$('#btnUpdate').hide();$('#btnSave').show();randomPass();"><i class="material-icons">repeat</i><span class="icon-name">Reset</span></button>
								 </div>
								 </div>
                            </form>
                        </div>
						</div>
                    </div>
                </div>
				
            </div>
			 <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                            <i class="fa fa-fw fa-users"></i>   User List
                            </h2>
                          
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="example" class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
											<th>&nbsp;</th>
                                            <th>SR NO</th>
											<th>User Name  </th>
											<th>Email</th>
											<th>Contact No</th>
											<th>City</th>
											<th>Address</th>
											<th>Status</th>
											<th>Role</th>
											<th>Date</th>
											<th>action</th>
                                        </tr>
                                    </thead>
                       
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </section>



 <?php include'footer.php'?>
 <script>
 	  function toChangeDateFormat(dateStr)
{
    var parts = dateStr.split("-");
    //return new Date(parts[2], parts[1], parts[0]);
	var dt=  parts[2]+"/"+parts[1]+"/"+parts[0];
	return dt;
}

//ADD FOCUS ON SIDEBAR
	$("#sidebar_ul").find(".active").removeClass("active");
	$("#sidebar_user_master").addClass("active").focus();
$( document ).ready(function() {

	var otable = $('#example').DataTable( {
	dom: 'Bfrtip',
	buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
"ajax": "usermasterdata.php",
"bPaginate":true,
"bProcessing": true,


"dataSrc":"data",
 "columns": [
{ mData: 'uid' },
{ mData: 'count' },
{ mData: 'username' } ,
{ mData: 'email' } ,
{ mData: 'contact_no' } ,
{ mData: 'city' } ,
{ mData: 'address' } ,
{ mData: 'status' } ,
{ mData: 'role' } ,
{ mData: 'created'  },
{ mData: 'action' }
],
"columnDefs": [

{ targets: [0],
        "mData": "uid",
		"sClass": "center",
		"width": "1%",
		"visible":false
 },
 
 {targets: [7],
				"mData": "status",
					"sClass": "center",
					"mRender": function (data, type, row) {
						return '<button type="button" class="btn btn-flat btn-green pull-left bg-green btn-sm" >'+row.status+'</button>'
					}
				},
				{targets: [8],
					"mData": "role",
					"sClass": "center",
					"mRender": function (data, type, row) {
						return '<button type="button" class="btn btn-flat btn- pull-left bg-indigo btn-sm" >'+row.role+'</button>'
					}
				},

  {targets: [10],
        "mData": "action",
        "sClass": "center",
        "mRender": function (data, type, row) {
		return '<button type="button" class="btn bg-blue waves-effect pull-left btn-sm" onclick="$(\'#btnSave\').hide();$(\'#btnUpdate\').show();editEntry('+row.uid	+')"><i class="material-icons">edit</i></button>' +
		'<button class="btn bg-red waves-effect pull-left btn-sm" onclick="showConfirmMessage('+row.uid	+');"><i class="material-icons">delete_forever</i></button>'
        }
    },
]		

});
});

</script>
<script>

function showStopMessage() {
    swal({
        title: "Stop!",
        text: "Dont cut this log.!",
        type: "error",

        closeOnConfirm: false
    });
}

function showSuccesMessage() {
    swal({
        title: "Success!",
        text: "User Details has been Added succesfully.!",
        type: "success",

        closeOnConfirm: false
    });
}

function showEditSuccesMessage() {
    swal({
        title: "Success!",
        text: "User Details has been Updated succesfully.!",
        type: "success",

        closeOnConfirm: false
    });
}

function showWarningMessage() {
    swal({
        title: "oops!",
        text: "Something went wrong, please check the values and try again.!",
        type: "warning",

        closeOnConfirm: false
    });
}

function showConfirmMessage(uid) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
		vaction('delete',uid);
        swal("Deleted!", "User data has been deleted successfully.", "success");
    });
}

</script>
<script src="js/pages/forms/basic-form-elements.js"></script>
</body>

</html>