<?php include'header.php';

if($_SESSION['user_role'] != 'admin'  && $_SESSION['user_role'] != 'supervisor')

	{

	header('Location: index.php');

	exit(0);	

}

?>

<script>
//for validation

function validation(method)

{	   

	var vPollQuestion = false;

	var expiry_date = false;

	

	

	//$length=$('#form').parent().find('length');



	if(!$('#vPollQuestion').val())

	{

			$('#vPollQuestion').parent().addClass('has-error');

			$('#vPollQuestion_help').show();

	}			

	else{$('#vPollQuestion').parent().removeClass('has-error');$('#vPollQuestion_help').hide();vPollQuestion=true;}

		

		if(!$('#expiry_date').val())

	{

			$('#expiry_date').parent().addClass('has-error');

			$('#expiry_date_help').show();

	}			

	else{$('#expiry_date').parent().removeClass('has-error');$('#expiry_date_help').hide();expiry_date=true;}

	
	if(vPollQuestion && expiry_date ) 

	{

			vaction(method);

	} 

	else{alert("There is some problem occured. Please try again.")}

		

}

//for table data 

	function editEntry(id){   
		// $('.help-block').hide();
		// $('.has-error').removeClass('has-error');
		$(".multi-fields").empty();
		
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url: 'pollmasteraction.php',
			data: 'action_type=data&iPollQuestionID='+id,
			success:function(data){
		
		var html = '';
			for($i=0;$i<data.poll_question.length;$i++)
			{
			
			$vPollOption = data.poll_question[$i]['vPollOption'];
		
		// console.log($vPollOption);
		html+= '<div class="multi-field"><div class="row" style="border-top: 1px solid rgba(204, 204, 204, 0.35);padding-top: 10px;"><div class="col-md-6 col-sm-6 col-xs-6"><label class="form-label">Poll Option</label><span style="color:red"> * </span><div class="form-line"><input type="text" class="form-control input-md" id="vPollOption_'+$i+'"  name="vPollOption[]" value="'+$vPollOption+'" placeholder="Poll Option" tabindex="0"><span class="help-block" id="answer_help" style="display:none;font-size:12px;color:red;">Please Enter vPollOption.</span></div></div><div class="col-md-1 col-sm-1 col-xs-1"><label class="form-label"></label><div class="form-line"><button class="btn btn-block bg-blue waves-effect add-field"  type="button" ><i class="material-icons">add_circle_outline</i> <span class="icon-name"></span></button></div></div>';
		
		if($i == 0){
			html += '</div></div>';
		}else{
			html += '<div class="col-md-1 col-sm-1 col-xs-1"><label class="form-label"></label><div class="form-line"><button class="btn btn-block bg-red waves-effect remove_field"  type="button" ><i class="material-icons">remove_circle_outline</i> <span class="icon-name"></span></button></div></div></div></div>';
		}
	
		
	
		$expiry_date = toChangeDateFormat(data.poll_question[$i]['expiry_date']);
		$vPollQuestion = data.poll_question[$i]['vPollQuestion'];
		$iPollQuestionID = data.poll_question[$i]['iPollQuestionID'];
		
		$('#expiry_date').val($expiry_date);
		$('#vPollQuestion').val($vPollQuestion);
		$('#iPollQuestionID').val($iPollQuestionID);
		
		html+='</div>';
		
		}
		
		$(".multi-fields").append(html);
		
		console.log(html);
		
			}
		});
	}

	

	
	function getOptions(id)
{   
	$.ajax({
	type: 'POST',
	dataType:'JSON',
	url: 'pollmasteraction.php',
	data: 'action_type=getOptions&iPollQuestionID='+id,
	success:function(data){	
	$count=1;
		for($i=0;$i<data.length;$i++){
			$('#transaction_list').append('<tr><td>'+$count+'</td><td>'+data[$i]['vPollOption']+'</td><td>'+data[$i]['answ']+'</td><td>'+data[$i]['perc']+'</td></tr>');
			$count++;
		}
	
	//SCROLL TO TOP
	$('html, body').animate({ scrollTop: 0 }, 'slow');
	}});
	$('#transaction_list').empty();
}


	

//for rack master

</script>

<script>
function vaction(type,id){

    id = (typeof id == "undefined")?'':id;

    var userData = '';

 

if (type == 'add') 

 {

     var userData = new FormData($(".Category_entry")[0]);

	userData.append('action_type', type);

    $.ajax({

        url: 'pollmasteraction.php',

        type: 'POST',

        data: userData,

        async: false,

        success: function (data) {

			if(data == 'ok'){

			$('#example').DataTable().ajax.reload(null, false);

			showSuccesMessage();

			resetdata();

			$('#blah').attr('src', 'images/image_upload.jpg');

           }
		   else if(data == 'expiry_date'){
				showExpiryDateMessage();
           }

		},

        cache: false,

        contentType: false,

        processData: false,

    });



    return false;



	

    }

	else if (type == 'edit'){

        var userData = $(".Category_entry").serialize()+'&action_type='+type;

		

		$.ajax({

			url: 'pollmasteraction.php',

			type: 'POST',

			data: userData,

			async: false,

			success: function (data) {

			if(data == 'ok'){

				$('#example').DataTable().ajax.reload(null, false);

				showEditSuccesMessage();

				resetdata();

				$(btnUpdate).hide();

				$(btnSave).show();

			}
			else if(data == 'expiry_date'){
				showExpiryDateMessage();
           }

        }

    });

     }

	 

	 

	else if (type == 'delete'){

     

		userData ='action_type=delete&iPollQuestionID='+id; 

		$.ajax({	

			url: 'pollmasteraction.php',

			type: 'POST',

			data: userData,

			async: false,

			success: function (data) {

				

				$('#example').DataTable().ajax.reload(null, false);

        },

       

    });

				

    }

    

    }

function resetdata(){

	  $('.Category_entry')[0].reset();
	  $('#iPollQuestionID').val("");
	  
		$('#insertBtns').show();
		$('#updateBtns').hide();
		$('.help-block').hide();
		$('.has-error').removeClass('has-error');
		$('.multi-fields').empty();
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$html = "";
		$html += '<div class="multi-field"><div class="row" style="border-top: 1px solid rgba(204, 204, 204, 0.35);padding-top: 10px;"><div class="col-md-6 col-sm-6 col-xs-6"><label class="form-label">Poll Option</label><span style="color:red"> * </span><div class="form-line"><input type="text" class="form-control input-md" id="vPollOption_1"  name="vPollOption[]" value="" placeholder="Poll Option" tabindex="0"><span class="help-block" id="answer_help" style="display:none;font-size:12px;color:red;">Please Enter vPollOption.</span></div></div><div class="col-md-1 col-sm-1 col-xs-1"><label class="form-label"></label><div class="form-line"><button class="btn btn-block bg-blue waves-effect add-field"  type="button" ><i class="material-icons">add_circle_outline</i> <span class="icon-name"></span></button></div></div></div></div>';
		
		$('.multi-fields').append($html);
	  
  }



function prevent(e) {

   e.preventDefault();

}

</script>



<?php include'navbar.php' ?>

<?php include'sidebar.php' ?>

  <section class="content">

        <div class="container-fluid">

           

        </div>

		 

		 <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">

                        <div class="header">

                            <h2><i class="fa fa-gear" aria-hidden="true"></i>  Poll Details</h2>

                         

                        </div>

                        <div class="body">

                            <form id="form_validation" method="POST" class="Category_entry">

							<div class="row">
								
								
								
							

									<div class="col-md-6 col-sm-6 col-xs-6">
									<label class="form-label">Poll Question</label>
                                    <div class="form-line">
                                     <textarea class="form-control" rows="4" id="vPollQuestion" name="vPollQuestion" placeholder="Enter Poll Question..." tabindex="0"></textarea>
									<span class="help-block" id="vPollQuestion_help" style="display:none;">Please Enter Poll Question.</span>
                                    </div>
                                    </div>

									<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="form-line">
									<label class="form-label">Expiry Date</label>
									<input type="text" class="datepicker form-control" placeholder="Please choose a date..." name="expiry_date" id="expiry_date">
									<span class="help-block" id="expiry_date_help" style="display:none;font-size:12px;color:red;">Please Enter Expiry Date.</span>
										
									</div> 
									</div>

									

				

								</div>

							<!-- Multiple Add -->							
								
									<div class="multi-field-wrapper">
										<div class="multi-fields">
											<div class="row" style="border-top: 1px solid rgba(204, 204, 204, 0.35);padding-top: 10px;">
											
													<div class="col-md-6 col-sm-6 col-xs-6" id="poll_options">
														<label class="form-label" id="options__">Poll Option</label><span style="color:red"> </span>
														<div class="form-line" id="options_">
															<input type="text" class="form-control input-md" id="vPollOption_1"  name="vPollOption[]" placeholder="Poll Option" tabindex="0">
																
															<span class="help-block" id="answer_help" style="display:none;font-size:12px;color:red;">Please Enter Poll Option.</span>
														</div>
													</div>
													
													<div class="col-md-1 col-sm-6 col-xs-6">
														<label class="form-label"></label>
														<div class="form-line">
															<button class="btn btn-block bg-blue waves-effect add-field" id="addBTN" type="button" ><i class="material-icons">add_circle_outline</i> <span class="icon-name"></span></button>
														</div>
													</div>
													
												</div>
												</div>
												</div>
										
<!-- Multiple Add-->

								<input type="hidden" id="iPollQuestionID" name="iPollQuestionID"/>

								 <div class="row clearfix js-sweetalert">

								 <button id="btnSave" class="btn bg-blue btn-sm waves-effect" type="button" style="margin-left:20px;" onclick="validation('add');"><i class="material-icons">save</i><span class="icon-name">Save</span></button>

								

                                <button type="button" id="btnUpdate" class="btn bg-orange btn-sm waves-effect"  style="display: none;margin-left:20px;" onclick="vaction('edit');">

								<i class="material-icons">edit</i><span class="icon-name">Update</span></button>

								

                                 <button type="button" id="btnReset" class="btn bg-red btn-sm waves-effect" style="margin-left:20px;"  onclick="resetdata();$('#btnUpdate').hide();$('#btnSave').show();"><i class="material-icons">repeat</i><span class="icon-name">Reset</span></button>

                            </form>

                        </div>

						</div>

                    </div>

                </div>

				

            </div>

			 <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">

                        <div class="header">

                            <h2>

                               <i class="fa fa-table" aria-hidden="true"></i>   Poll List

                            </h2>

                         

                        </div>

                        <div class="body">

                            <div class="table-responsive">

                                <table id="example" class="table table-bordered table-striped table-hover dataTable">

                                    <thead>

                                        <tr>

                                            <th>&nbsp;</th>

                                            <th>SR NO</th>

                                            <th>Poll Question</th>  

											<th>Expiry Date</th>

                                            <th>Poll Option</th>  

											<th>Action</th>

                                        </tr>

                                    </thead>

                       

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

			
			<!-- M A T E R I A L   L I S T   M O D A L -->
				<div class="modal" id="material_modal">
					<div class="modal-dialog" style="width:842px">
						<div class="modal-content">
							<div class="modal-header" style="background-color:#00BCD4">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" style="color:#fff">P O L L  &nbsp;&nbsp; O P T I O N S</h4>
							</div>
							<div class="modal-body" id="invoice_body" style="background-color:#ffffff">
								
								<!-- MODAL DATA -->
								
								
								<!-- Content Header (Page header) -->
								
								
								<!-- Main content -->
								<section class="invoice" id="section-to-print">
									
										<!-- /.row -->
										<!--C O U R I E R D E T A I L S-->
										
										<div class="row">
											<div class="col-xs-12 col-md-12">
												<p class="lead">Options List</p>
												
												<div class="table-responsive">
													<table class="table" id="doc_list">
														<thead>
															<th>SR No</th>
															<th>Option Name</th>
															<th>Answers</th>
															<th>Answer %</th>
															
														</thead>
														<tbody id ="transaction_list">
														
														</tbody>
														
													</table>
												</div>
											</div>
										</div>
										
									</section>
									<!-- /.content -->
									<div class="clearfix"></div>
									
									
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-flat btn-warning pull-right" onlick="resetdata();" id="close_modal_btn" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
					
		<!-- T R A N S A C T I O N   L I S T   M O D A L -->


    </section>







 <?php include'footer.php' ?>

 <script>

function toChangeDateFormat(dateStr)
{
    var parts = dateStr.split("-");
    //return new Date(parts[2], parts[1], parts[0]);
	var dt=  parts[2]+"/"+parts[1]+"/"+parts[0];
	return dt;
}


//ADD FOCUS ON SIDEBAR

	$("#sidebar_ul").find(".active").removeClass("active");

	$("#sidebar_poll_master").addClass("active").focus();

$( document ).ready(function() {



	var otable = $('#example').DataTable( {

	dom: 'Bfrtip',

	buttons: [

            'copy', 'csv', 'excel', 'pdf', 'print'

        ],

"ajax": "pollmasterdata.php",

"bPaginate":true,

"bProcessing": true,





"dataSrc":"data",

 "columns": [

 { mData: 'iPollQuestionID' },

 { mData: 'count' },

{ mData: 'vPollQuestion' } ,

{ mData: 'expiry_date' } ,

{ mData: 'options' } ,


{ mData: 'action' }

],

"columnDefs": [



{ targets: [0],

        "mData": "iPollQuestionID",

		"sClass": "center",

		"width": "1%",

		"visible":false

 },


{targets: [5],

        "mData": "action",

        "sClass": "center",

        "mRender": function (data, type, row) {

		return '<button type="button" class="btn bg-blue waves-effect pull-left btn-sm" onclick="resetdata();$(\'#btnSave\').hide();$(\'#addBTN\').hide();$(\'#options_\').hide();$(\'#options__\').hide();$(\'#btnUpdate\').show();editEntry('+row.iPollQuestionID+')"><i class="material-icons">edit</i></button>' +

		'<button class="btn bg-red waves-effect pull-left btn-sm" onclick="showConfirmMessage('+row.iPollQuestionID+');"><i class="material-icons">delete_forever</i></button>'

        }

    },
 

	{targets: [4],

        "mData": "action",

        "sClass": "center",

        "mRender": function (data, type, row) {

		return '<button type="button" id="status_color" class="btn bg-blue btn-flat btn-sm" data-toggle="modal" data-target="#material_modal" onclick="getOptions('+row.iPollQuestionID+');"><i class="fa fa-file-text-o"></i>&nbsp;Options</button>'

        }

    },

]		



});

});



</script>

<script>



function showStopMessage() {

    swal({

        title: "Stop!",

        text: "Dont cut this log.!",

        type: "error",



        closeOnConfirm: false

    });

}



function showSuccesMessage() {

    swal({

        title: "Success!",

        text: "Poll Details has been Added succesfully.!",

        type: "success",



        closeOnConfirm: false

    });

}



function showEditSuccesMessage() {

    swal({

        title: "Success!",

        text: "Poll Details has been Updated succesfully.!",

        type: "success",



        closeOnConfirm: false

    });

}



function showWarningMessage() {

    swal({

        title: "oops!",

        text: "Something went wrong, please check the values and try again.!",

        type: "warning",



        closeOnConfirm: false

    });

}

function showExpiryDateMessage() {

    swal({

        title: "oops!",

        text: "Expiry Date cant be set to Todays date or previous Date.!",

        type: "warning",
        closeOnConfirm: false

    });

}



function showConfirmMessage(iPollQuestionID) {

    swal({

        title: "Are you sure?",

        text: "You will not be able to recover this data!",

        type: "warning",

        showCancelButton: true,

        confirmButtonColor: "#DD6B55",

        confirmButtonText: "Yes, delete it!",

        closeOnConfirm: false

    }, function () {

		vaction('delete',iPollQuestionID);

        swal("Deleted!", "Poll data has been deleted successfully.", "success");

    });

}
$( document ).ready(function() {

//MULTI ADD SECTION





    /* ADD DESTINATION */
    $('.multi-field-wrapper').each(function () {
        var $wrapper = $('.multi-fields', this);
        var x = 1;
        /*$(".add-field", $(this)).click(function (e) {*/
        $($wrapper).on("click", ".add-field", function (e) { //user click on remove text
            x++;
            $($wrapper).append('<div class="multi-field"><div class="row" style="border-top: 1px solid rgba(204, 204, 204, 0.35);padding-top: 10px;"><div class="col-md-6 col-sm-6 col-xs-6"><label class="form-label">Poll Option</label><span style="color:red"> * </span><div class="form-line"><input type="text" class="form-control input-md" id="vPollOption_'+x+'"  name="vPollOption[]" placeholder="Poll Option" tabindex="0"><span class="help-block" id="answer_help" style="display:none;font-size:12px;color:red;">Please Enter vPollOption.</span></div></div><div class="col-md-1 col-sm-6 col-xs-6"><label class="form-label"></label><div class="form-line"><button class="btn btn-block bg-blue waves-effect add-field"  type="button" ><i class="material-icons">add_circle_outline</i> <span class="icon-name"></span></button></div></div><div class="col-md-1 col-sm-6 col-xs-6"><label class="form-label"></label><div class="form-line"><button class="btn btn-block bg-red waves-effect remove_field"  type="button" ><i class="material-icons">remove_circle_outline</i> <span class="icon-name"></span></button></div></div></div></div>');
        });
		
		

        $($wrapper).on("click", ".remove_field", function (e) { //TO REMOVE FIELD
            e.preventDefault(); $(this).parent().parent().parent().parent('div').remove(); x--;
        });


    });
	})





//MULTI ADD SECTION


</script>

</body>
<script src="js/pages/forms/advanced-form-elements.js"></script>
<script src="js/pages/forms/basic-form-elements.js"></script>


</html>