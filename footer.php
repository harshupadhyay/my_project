   <div class="footer-container-for-sidebar-menu">
            <footer class="web-footer footer footer-top-border py-50px">
                <div class="container">
                    <div class="row large-gutters">
                        <div class="col-md-6">
                            <div class="footer-widget">
                                <p class="m-0">© 2018 Vignyapan</p>
                            </div>
                        </div> 
                        <div class="col-md-6">
                            <div class="footer-widget">
                                <ul class="list-inline m-0 text-right footer-nav">
                                    <li class="list-inline-item">
                                        <a href="index.php">
                                            Home
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="page-about-us.html">
                                            About us
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="page-contact.html">
                                            Contact
                                        </a>
                                    </li>
                                  <!--   <li class="list-inline-item">
                                        <a href="https://twitter.com/energeticthemes">
                                            Store location
                                        </a>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <!-- End main 
        <div class="footer-container-for-sidebar-menu">
            <footer class="web-footer footer footer-top-border py-50px">
                <div class="container">
                    <div class="row large-gutters">
                        <div class="col-md-6">
                            <div class="footer-widget">
                                <p class="m-0">© 2018 Vignyapan</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="footer-widget">
                                <ul class="list-inline m-0 text-right footer-nav">
                                    <!--<li class="list-inline-item">
                                        <a href="https://www.facebook.com/energeticthemes">
                                            Home
                                        </a>
                                    </li>
                                </ul>
                                    <li class="list-inline-item">
                                        <a href="https://instagram.com/energeticthemes">
                                            About us
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="https://behance.com/energeticthemes">
                                            Contact
                                        </a>
                                    </li>
                                   <!-- <li class="list-inline-item">
                                        <a href="https://twitter.com/energeticthemes">
                                            Store location
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!-- ================================================== -->
    <!-- Placed js files at the end of the document so the pages load faster -->
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/plugins/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="js/plugins/aos.js"></script>
    <script type="text/javascript" src="js/plugins/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.countTo.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.easing.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/plugins/onepage.min.js"></script>
    <script type="text/javascript" src="js/plugins/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/plugins/instafeed.min.js"></script>
    <script type="text/javascript" src="js/plugins/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="js/plugins/contact-us.min.js"></script>
    <script type="text/javascript" src="js/plugins/twitterFetcher_min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAE_JprYsi2sHzUcl8u1DbcUgQnDveJWs4"></script>
    <script type="text/javascript" src="js/main.js"></script>
	<script>
var myIndex = 0;
carousel();

function carousel() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  myIndex++;
  if (myIndex > x.length) {myIndex = 1}    
  x[myIndex-1].style.display = "block";  
  setTimeout(carousel, 2000); // Change image every 2 seconds
}
</script>
</body>
</html>